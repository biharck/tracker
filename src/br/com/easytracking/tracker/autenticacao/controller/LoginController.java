package br.com.easytracking.tracker.autenticacao.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.authorization.User;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MessageType;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.menu.MenuTag;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.dao.NextAuthorizationDAO;
import br.com.easytracking.tracker.dao.UsuarioDAO;
import br.com.easytracking.tracker.service.UsuarioService;
import br.com.easytracking.tracker.util.PathUtil;
import br.com.easytracking.tracker.util.TrackerUtil;

/**
 * Controller que far� o login do usu�rio na aplica��o.
 * Se o login for efetuado com sucesso ir� redirecionar para /secured/Index
 */
@Controller(path="/autenticacao/Login")
public class LoginController extends MultiActionController {

	NextAuthorizationDAO authorizationDAO;
	
	public void setAuthorizationDAO(NextAuthorizationDAO authorizationDAO) {
		this.authorizationDAO = authorizationDAO;
	}

	
	/**
	 * Action que envia para a p�gina de login
	 */
	@DefaultAction
	public ModelAndView doPage(WebRequestContext request, Usuario usuario){
		return new ModelAndView("login", "usuario", usuario);
	}
	
	/**
	 * Efetua o login do usu�rio
	 */
	public ModelAndView doLogin(WebRequestContext request, Usuario usuario){
		String login = usuario.getLogin();
		//se foi passado o login na requisi��o, iremos verificar se o usu�rio existe e a senha est� correta
		if(login != null){
			//buscamos o usu�rio do banco pelo login
			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
			
			User userByLogin = authorizationDAO.findUserByLogin(login);
			
			// se o usu�rio existe e a senha est� correta
			if(userByLogin != null && encryptor.checkPassword(usuario.getPassword(), userByLogin.getPassword())){
				Usuario usuarioLogado = UsuarioService.getInstance().loadForEntrada((Usuario) userByLogin);
				
				if(!usuarioLogado.isAtivo()){
					request.addMessage("Usu�rio com acesso bloqueado. ", MessageType.ERROR);
					//limpar o campo senha, e enviar para a tela de login j� que o processo falhou
					usuario.setSenha(null);
					return doPage(request, usuario);
				}
				
				//Setando o atributo de se��o USER fazemos o login do usu�rio no sistema. 
				request.setUserAttribute("USER", userByLogin);

				//Limpamos o cache de permiss�es o menu.
				//O menu ser� refeito levando em considera��o as permiss�es do usu�rio
				request.setUserAttribute(MenuTag.MENU_CACHE_MAP, null);
				
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:mm");	
				if(usuarioLogado.getDtUltimoLogin()!=null){
					request.getSession().setAttribute("dtUltimoLogin", format.format(usuarioLogado.getDtUltimoLogin()));
				}
				request.getSession().setAttribute("nomeExibicao", usuarioLogado.getNome());
				
				//verifica se usu�rio deve alterar senha
				if(usuarioLogado.isSenhaProvisoria()){
					request.getSession().setAttribute("isSenhaProvisoria", true);
				}else
					request.getSession().setAttribute("isSenhaProvisoria", false);

				
				//altera ultimo login realizado
				usuarioLogado.setDtUltimoLogin(new Timestamp(System.currentTimeMillis()));
				UsuarioDAO.getInstance().alteraDtUltimoLogin(usuarioLogado);
				
				TrackerUtil.addLog("Autentica��o no Sistema");
				
				return new ModelAndView("redirect:"+PathUtil.AFTER_LOGIN_GO_TO);				
			}
			
			request.addMessage("Login e/ou senha inv�lidos", MessageType.ERROR);
		}
		
		//limpar o campo senha, e enviar para a tela de login j� que o processo falhou
		usuario.setSenha(null);
		return doPage(request, usuario);
	}

}