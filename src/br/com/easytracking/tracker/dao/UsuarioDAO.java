package br.com.easytracking.tracker.dao;


import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.standard.Next;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.util.CollectionsUtil;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.easytracking.tracker.adm.filtro.UsuarioFiltro;
import br.com.easytracking.tracker.adm.relatorio.filtro.UsuarioReportFiltro;
import br.com.easytracking.tracker.bean.Papel;
import br.com.easytracking.tracker.bean.PapelUsuario;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.service.UsuarioService;
import br.com.easytracking.tracker.util.SCRException;
import br.com.easytracking.tracker.util.TrackerUtil;


@DefaultOrderBy("usuario.nome")
public class UsuarioDAO extends GenericDAOTracker<Usuario> {
	
	PapelDAO papelDAO;
	PapelUsuarioDAO papelUsuarioDAO;	private static UsuarioDAO instance;
	public static UsuarioDAO getInstance() {
		if(instance == null){
			instance = Next.getObject(UsuarioDAO.class);
		}
		return instance;
	}
	
	public void setPapelUsuarioDAO(PapelUsuarioDAO papelUsuarioDAO) {
		this.papelUsuarioDAO = papelUsuarioDAO;
	}
	
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	public Usuario findByLogin(String login) {
		Usuario bean = query()
			.where("UPPER(usuario.login) = ?", login.toUpperCase())			
			.unique();
		if(bean !=null)
			bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	
	
	@Override
	public Usuario loadForEntrada(Usuario bean) {
		bean = super.loadForEntrada(bean);
		bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	
	
	public PapelDAO getPapelDAO() {
		return papelDAO;
	}
	
	
	public Usuario getUserCliente(Usuario usuario){
		return query().select("usuario.id, c.id")
				.leftOuterJoin("usuario.cliente c")
				.where("usuario=?",usuario)
				.unique();
	}
	
	@Override
	public void saveOrUpdate(final Usuario bean)  {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				
				UsuarioService usuarioService = new UsuarioService();
				Usuario load;
				
				/* Insert */
				if(bean.getId() == null){
					load = new Usuario();
					bean.setSenha(usuarioService.encryptPassword(bean));
					load.setSenha(bean.getSenha());
					bean.setAtivo(true);
				}else
					load = load(bean);
				
				//atualiza somente campos do formulario
				load.setNome(bean.getNome());
				load.setLogin(bean.getLogin());
				load.setAtivo(bean.isAtivo());
				load.setSenhaProvisoria(bean.isSenhaProvisoria());
				load.setEmail(bean.getEmail());
				load.setCliente(bean.getCliente());
				
				//valida uniques
				UsuarioDAO.super.saveOrUpdate(load);
				
				papelUsuarioDAO.deleteByUsuario(load);
				if(bean.getPapeis() != null){
					for (Papel papel : bean.getPapeis()) {
						PapelUsuario papelUsuario = new PapelUsuario();
						papelUsuario.setPapel(papel);
						papelUsuario.setUsuario(load);
						papelUsuarioDAO.saveOrUpdate(papelUsuario);
					}
				}
				return null;
			}
			
		});
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Usuario> query,FiltroListagem _filtro) {
		UsuarioFiltro filtro = (UsuarioFiltro) _filtro;

		
		if(!TrackerUtil.isPessoaLogadaAdministrador() && !TrackerUtil.isPessoaLogadaAdministrativo()){
			query.where("usuario.cliente=?",getUserCliente(TrackerUtil.getUsuarioLogado()).getCliente());
		}
		
		query.select("usuario.id, usuario.nome, usuario.login,usuario.ativo,usuario.dtUltimoLogin, "+
				"papel.nome, papel.id")
		.leftOuterJoin("usuario.papeisUsuario pu")
		.leftOuterJoin("pu.papel papel")
		.whereLikeIgnoreAll("usuario.nome", filtro.getNome());
		
		if(filtro.getPapeis()!=null){
			String idsPapel = CollectionsUtil.listAndConcatenate(filtro.getPapeis(), "id", ",");
			query.whereIn("pu.papel", idsPapel);
		}
		
		if(filtro.getAtivo()!=null)			
			query.where("usuario.ativo is "+filtro.getAtivo().isOptBool());
		
	}

	public void alteraSenhaAndDataRequisicao(Usuario bean) {
		getJdbcTemplate().update("UPDATE usuario SET senha = ?, senhaAnterior = ?, senhaAnterior2 = ?, senhaProvisoria = ?, dataRequisicao = ? " +
				" where id = ?",bean.getSenha(),bean.getSenhaAnterior(), bean.getSenhaAnterior2(), bean.isSenhaProvisoria(), bean.getDataRequisicao() ,bean.getId());
	}
	
	public void alteraSenha(Usuario bean) {
		getJdbcTemplate().update("UPDATE usuario SET senha = ?, senhaAnterior = ?, senhaAnterior2 = ?, senhaProvisoria = ?" +
				" where id = ?",bean.getSenha(),bean.getSenhaAnterior(), bean.getSenhaAnterior2(), bean.isSenhaProvisoria(), bean.getId());
	}
	
	public void alteraDtUltimoLogin(Usuario bean) {
		getJdbcTemplate().update("UPDATE usuario SET dtUltimoLogin = ?" +
				" where id = ?", bean.getDtUltimoLogin(), bean.getId());
	}
	
	public List<Usuario> findByFiltro(UsuarioReportFiltro filtro) {
		QueryBuilder<Usuario> query = query();
		
		query.select("usuario.id, usuario.nome, usuario.login,usuario.ativo,usuario.dtUltimoLogin,usuario.email, "+
					"papel.nome, papel.id")
			.leftOuterJoin("usuario.papeisUsuario pu")
			.leftOuterJoin("pu.papel papel");
		
		if(filtro.getPapeis()!=null){
			String idsPapel = CollectionsUtil.listAndConcatenate(filtro.getPapeis(), "id", ",");
			query.whereIn("pu.papel", idsPapel);
		}
		
		if(filtro.getAtivo()!=null)			
			query.where("usuario.ativo is "+filtro.getAtivo().isOptBool());
		
		return query.list();
	}
	
	/**
	 * <p>Encontra uma usu�rio que possua o respectivo email</p>
	 * @param email {@link String}
	 * @return Um usu�rio com base no email
	 */
	public Usuario findUserEmail(String email) {
		if (email == null || "".equals(email)) {
			throw new SCRException("Email n�o deve ser NULL e/ou vazio");
		} else {
			return query()
					.select("usuario")
					.where("usuario.email = ?",email)
					.unique();
		}
	}
	
	
	@Override
	public void delete(Usuario bean) {
		papelUsuarioDAO.deleteByUsuario(bean);
		super.delete(bean);
	}
	
	public List<Usuario> getUsuariosInstaladores(){
		return query()
				.leftOuterJoin("usuario.papeisUsuario pu")
				.leftOuterJoin("pu.papel papel")
				.where("papel.id=?",6).list();
	}
	
	
	
}
