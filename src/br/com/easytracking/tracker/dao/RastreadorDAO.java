package br.com.easytracking.tracker.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.easytracking.tracker.adm.filtro.RastreadorFiltro;
import br.com.easytracking.tracker.bean.Rastreador;
import br.com.easytracking.tracker.bean.SituacaoRastreador;

@DefaultOrderBy("rastreador.idRastreador")
public class RastreadorDAO extends GenericDAOTracker<Rastreador>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Rastreador> query,FiltroListagem _filtro) {
		RastreadorFiltro filtro = (RastreadorFiltro) _filtro;
		query
			.where("rastreador.chip = ?", filtro.getChip() )
			.whereLikeIgnoreAll("rastreador.imei", filtro.getImei())
			.where("rastreador.idRastreador=?",filtro.getIdRastreador());
		
		if(filtro.getSituacaoRastreador()!=null)
			query.where("rastreador.situacaoRastreador=?",filtro.getSituacaoRastreador());
	}
	
	public Rastreador getRastreador(Rastreador rastreador){
		return query().select("rastreador.id, rastreador.idRastreador, rastreador.fabricante, rastreador.situacaoRastreador," +
				"m.id, c.id")
				.leftOuterJoin("rastreador.modeloRastreador m")
				.leftOuterJoin("rastreador.chip c").where("rastreador=?",rastreador).unique();
	}
	
	public void updateSituacaoRastreador(Rastreador rastreador, SituacaoRastreador situacaoRastreador){
		String sql = "update Rastreador set situacaoRastreador_id = "+ situacaoRastreador.getId() + " where id = "+rastreador.getId();
		getJdbcTemplate().update(sql);
	}

}
