package br.com.easytracking.tracker.dao;

import br.com.easytracking.tracker.bean.Fabricante;
import br.com.easytracking.tracker.bean.ModeloRastreador;

public class FabricanteDAO extends GenericDAOTracker<Fabricante>{

	public Fabricante getFabricanteByModelo(ModeloRastreador modeloRastreador){
		return query().join("fabricante.modelos m").where("m=?",modeloRastreador).unique();
	}
}
