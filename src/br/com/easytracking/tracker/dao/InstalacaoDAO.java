package br.com.easytracking.tracker.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.easytracking.tracker.bean.Instalacao;
import br.com.easytracking.tracker.bean.Rastreador;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.util.TrackerUtil;

@DefaultOrderBy("instalacao.id")
public class InstalacaoDAO extends GenericDAOTracker<Instalacao>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<Instalacao> query,FiltroListagem _filtro) {
		if(!TrackerUtil.isPessoaLogadaAdministrador() && !TrackerUtil.isPessoaLogadaAdministrativo())
			query.where("instalacao.instalador=?",TrackerUtil.getUsuarioLogado());
	}
	
	public List<Instalacao> getByInstalador(Usuario user){
		return query()
				.select("r.idRastreador,r.id,  mr.id, f.id")
				.leftOuterJoin("instalacao.rastreador r")
				.leftOuterJoin("instalacao.situacaoRastreador sr")
				.leftOuterJoin("r.modeloRastreador mr")
				.leftOuterJoin("mr.fabricante f")
				.openParentheses().where("sr.id=5").or().where("sr.id=6").closeParentheses()
				.where("instalacao.instalador=?",user).list();
	}
	
	public List<Instalacao> getNaoInstalados(){
		return query()
				.select("r.idRastreador,r.id,  mr.id, f.id")
				.leftOuterJoin("instalacao.rastreador r")
				.leftOuterJoin("instalacao.situacaoRastreador sr")
				.leftOuterJoin("r.modeloRastreador mr")
				.leftOuterJoin("mr.fabricante f")
				.where("sr.id=5")
				.list();
	}
	
	public Instalacao getInstalacaoByRastreador(Rastreador rastreador){
		return query()
				.leftOuterJoin("instalacao.rastreador r")
				.where("r =?",rastreador)
				.unique();
	}
}
