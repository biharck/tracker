package br.com.easytracking.tracker.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.easytracking.tracker.adm.filtro.ChipFiltro;
import br.com.easytracking.tracker.bean.Chip;

@DefaultOrderBy("chip.numero")
public class ChipDAO extends GenericDAOTracker<Chip>{

	@Override
	public void updateListagemQuery(QueryBuilder<Chip> query,FiltroListagem _filtro) {
		
		ChipFiltro filtro = (ChipFiltro) _filtro;
		query	
			.where("chip.operadora=?",filtro.getOperadora());
			if(filtro.getNumero()!=null)
				query.whereLikeIgnoreAll("chip.numero", filtro.getNumero().getValue());
	}
}
