package br.com.easytracking.tracker.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Grupo;
import br.com.easytracking.tracker.util.TrackerUtil;

@DefaultOrderBy("grupo.nome")
public class GrupoDAO extends GenericDAOTracker<Grupo>{

	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("grupoVeiculos");
	}
	
	public Grupo getGrupoCompleto(Integer id){
		return query().select("grupo.id, grupo.nome, v.placa")
				.leftOuterJoin("grupo.grupoVeiculos gv")
				.leftOuterJoin("gv.veiculo v")
				.where("grupo.id=?",id)
				.unique();
	}
	public List<Grupo> getGrupoCompletoByCliente(Cliente c){
		return query().select("grupo.id, grupo.nome, v.placa")
				.leftOuterJoin("grupo.grupoVeiculos gv")
				.leftOuterJoin("gv.veiculo v")
				.leftOuterJoin("v.cliente c")
				.where("c=?",c)
				.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Grupo> query,FiltroListagem _filtro) {
		
		query
			.leftOuterJoin("grupo.grupoVeiculos gv")
			.leftOuterJoin("gv.veiculo v");
		
		if(!TrackerUtil.isPessoaLogadaAdministrador())
			query
			.leftOuterJoin("v.cliente c")
			.where("c=?",TrackerUtil.getUsuarioLogado().getCliente());
	}
	
	@Override
	public List<Grupo> findAll() {
		QueryBuilder<Grupo> query = query();
		
		query
		.leftOuterJoin("grupo.grupoVeiculos gv")
		.leftOuterJoin("gv.veiculo v")
		.leftOuterJoin("v.cliente c");
	
	if(!TrackerUtil.isPessoaLogadaAdministrador())
		query.where("c=?",TrackerUtil.getUsuarioLogado().getCliente())
		;
	return query.list();
	}
}
