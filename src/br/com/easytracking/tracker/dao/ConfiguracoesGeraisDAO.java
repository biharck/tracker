package br.com.easytracking.tracker.dao;

import br.com.easytracking.tracker.bean.ConfiguracoesGerais;

public class ConfiguracoesGeraisDAO extends GenericDAOTracker<ConfiguracoesGerais> {
	
	/**
	 * <p>Método responsável em retornar as Configuracoes Gerais
	 * @return {@link ConfiguracoesGerais}
	 * @author biharck
	 */
	public ConfiguracoesGerais getConfiguracoesGerais(){
		return
			query().select("configuracoesGerais").unique();
	}

}
