package br.com.easytracking.tracker.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.easytracking.tracker.adm.filtro.VeiculoFiltro;
import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.util.TrackerUtil;

@DefaultOrderBy("veiculo.placa")
public class VeiculoDAO extends GenericDAOTracker<Veiculo>{

	public List<Veiculo> getVeiculosByCliente(Cliente cliente){
		return query()
				.select("veiculo.id, f.id, veiculo.placa, veiculo.cliente, veiculo.chassi," +
						"veiculo.cor, veiculo.ano")
				.where("c=?",cliente)
				.leftOuterJoin("veiculo.rastreador r")
				.leftOuterJoin("r.modeloRastreador m")
				.join("veiculo.cliente c")
				.leftOuterJoin("m.fabricante f")
				.list();
	}
	public List<Veiculo> getVeiculosAdministrador(){
		return query()
				.select("veiculo.id, f.id, veiculo.placa, veiculo.chassi," +
						"veiculo.cor, veiculo.ano")
						.leftOuterJoin("veiculo.rastreador r")
						.leftOuterJoin("r.modeloRastreador m")
						.leftOuterJoin("m.fabricante f")
						.list();
	}
	public Veiculo getVeiculoByPlaca(String placa){
		return query()
				.select("r.idRastreador")
				.leftOuterJoin("veiculo.rastreador r")
				.where("veiculo.placa=?",placa).unique();
	}
	
	public Veiculo getVeiculo(Veiculo veiculo){
		return query()
				.select("veiculo.id, f.id, veiculo.placa, veiculo.cliente, veiculo.chassi," +
						"veiculo.cor, veiculo.ano, m.id, m.nome,r.id,f.id, f.nome")
				.where("veiculo=?",veiculo)
				.leftOuterJoin("veiculo.rastreador r")
				.leftOuterJoin("r.modeloRastreador m")
				.leftOuterJoin("m.fabricante f")
				.unique();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Veiculo> query,FiltroListagem _filtro) {
		VeiculoFiltro filtro = (VeiculoFiltro) _filtro;
		
		
			if(!TrackerUtil.isPessoaLogadaAdministrador() && !TrackerUtil.isPessoaLogadaAdministrativo()){
				query
				.leftOuterJoin("veiculo.cliente c")
				.where("c=?",TrackerUtil.getUsuarioLogado().getCliente());
			}
		
			if(filtro.getPlaca()!=null)
				query.where("veiculo.placa=?",filtro.getPlaca().toUpperCase());
			
			query.where("veiculo.chassi=?",filtro.getChassi())
				.leftOuterJoin("veiculo.rastreador r")
				.leftOuterJoin("r.modeloRastreador m")
				.leftOuterJoin("m.fabricante f")
				.where("r=?",filtro.getRastreador())
				.where("f=?",filtro.getFabricante())
				.where("m=?",filtro.getModeloRastreador());
			
	}
}
