package br.com.easytracking.tracker.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.easytracking.tracker.adm.filtro.CoordenadasFiltro;
import br.com.easytracking.tracker.bean.Coordenadas;

@DefaultOrderBy("coordenadas.date desc")
public class CoordenadasDAO extends GenericDAOTracker<Coordenadas>{

	public List<Coordenadas> getCoordenadasBySerial(String serial, int qtd, String dtInicio, String dtFim){
		QueryBuilder<Coordenadas> query = query();
		if(dtInicio!=null)
			query.where("coordenadas.date between '"+dtInicio+"' and '"+ dtFim +"'");
		
		query
				.where("coordenadas.serial=?",serial)
				.orderBy("coordenadas.date desc")
				.setMaxResults(qtd);
		
		return query.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Coordenadas> query,FiltroListagem _filtro) {
		
		CoordenadasFiltro filtro = (CoordenadasFiltro) _filtro;
		
		query
			.where("coordenadas.serial=?",filtro.getSerial())
			.where("coordenadas.date=?",filtro.getDate())
			.where("coordenadas.moving=?",filtro.getMoving())
			.where("coordenadas.input1=?",filtro.getInput1())
			.where("coordenadas.output1=?",filtro.getOutput1());
	}
}
