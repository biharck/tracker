package br.com.easytracking.tracker.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.easytracking.tracker.adm.filtro.LogFiltro;
import br.com.easytracking.tracker.bean.Log;
import br.com.easytracking.tracker.service.UsuarioService;

@DefaultOrderBy("log.timeInc")
public class LogDAO extends GenericDAOTracker<Log>{

	private UsuarioService usuarioService;
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Log> query,FiltroListagem _filtro) {
		LogFiltro filtro = (LogFiltro) _filtro;
		if(filtro!=null && filtro.getUsuario()!=null)
			query.where("log.userInc=?",usuarioService.load(filtro.getUsuario()).getLogin());
		
		query.whereLikeIgnoreAll("log.evento", filtro.getEvento());
		
	}
}
