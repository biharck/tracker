package br.com.easytracking.tracker.dao;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import br.com.easytracking.tracker.bean.PapelUsuario;
import br.com.easytracking.tracker.bean.Usuario;


@DefaultOrderBy("usuario")
public class PapelUsuarioDAO extends GenericDAO<PapelUsuario> {

	public void deleteByUsuario(Usuario usuario) {
		getHibernateTemplate().bulkUpdate(
				"delete from PapelUsuario where usuario = ?", usuario);
	}

}
