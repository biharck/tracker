package br.com.easytracking.tracker.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.easytracking.tracker.adm.filtro.ClienteFiltro;
import br.com.easytracking.tracker.bean.Cliente;

@DefaultOrderBy("cliente.razaoSocial")
public class ClienteDAO extends GenericDAOTracker<Cliente>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cliente> query,FiltroListagem _filtro) {
		ClienteFiltro filtro = (ClienteFiltro) _filtro;
		
		query
			.where("cliente.tipoCliente=?",filtro.getTipoCliente())
			.whereLikeIgnoreAll("cliente.razaoSocial", filtro.getRazaoSocial())
			.whereLikeIgnoreAll("cliente.cpfCnpj", filtro.getCpfCnpj())
			.where("cliente.municipio=?",filtro.getMunicipio())
			.where("cliente.telefone1=?",filtro.getTelefone1())
			.whereLikeIgnoreAll("cliente.email1", filtro.getEmail1())
			;
		
	}

}
