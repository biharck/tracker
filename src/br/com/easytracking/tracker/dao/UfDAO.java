package br.com.easytracking.tracker.dao;
import org.nextframework.persistence.DefaultOrderBy;

import br.com.easytracking.tracker.bean.Municipio;
import br.com.easytracking.tracker.bean.Uf;


@DefaultOrderBy("uf.nome")
public class UfDAO extends GenericDAOTracker<Uf> {
	
	public Uf getUfByMunicipio(Municipio m){
		return query().join("uf.municipios m").where("m=?",m).unique();
	}
}
