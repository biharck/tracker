package br.com.easytracking.tracker.dao;
import org.nextframework.persistence.DefaultOrderBy;

import br.com.easytracking.tracker.bean.Municipio;

@DefaultOrderBy("nome")
public class MunicipioDAO extends GenericDAOTracker<Municipio> {
	
	public Municipio loadByNome(String  nome) {
		Municipio bean = query()
				.select("municipio")
				.where("UPPER(municipio.nome) = ?", nome.toUpperCase())			
				.unique();
		
		return bean;
	}
	
	public Municipio findNomeById(Municipio bean){
		return query()
				.select("municipio.nome")
				.where("municipio = ?", bean)
				.unique();
		
	}
	
	
}
