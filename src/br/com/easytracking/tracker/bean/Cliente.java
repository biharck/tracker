package br.com.easytracking.tracker.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="cliente")
public class Cliente extends BeanAuditoria{
	
	private EnumTipoCliente tipoCliente;
	private String razaoSocial;
	private String nomeFantasia;
	private String cpfCnpj;
	private String inscricaoMunicipal;
	private String inscricaoEstadual;
	private String cep;
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private Uf uf;
	private Municipio municipio;
	private Telefone telefone1;
	private Telefone telefone2;
	private Telefone celular1;
	private Telefone celular2;
	private String email1;
	private String email2;
	private String website;
	private String facebook;
	private List<Usuario> usuarios;
	
	@Required
	public EnumTipoCliente getTipoCliente() {
		return tipoCliente;
	}
	@Required
	@MaxLength(200)
	@DescriptionProperty
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@MaxLength(200)
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	@Required
	@MaxLength(18)
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	@MaxLength(45)
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	@MaxLength(45)
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	@MaxLength(9)
	public String getCep() {
		return cep;
	}
	@Required
	@MaxLength(200)
	public String getEndereco() {
		return endereco;
	}
	@MaxLength(10)
	public String getNumero() {
		return numero;
	}
	@MaxLength(45)
	public String getComplemento() {
		return complemento;
	}
	@Required
	@MaxLength(60)
	public String getBairro() {
		return bairro;
	}
	@Required
	@Transient
	public Uf getUf() {
		return uf;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_cliente_municipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@Required
	@MaxLength(14)
	public Telefone getTelefone1() {
		return telefone1;
	}
	@MaxLength(14)
	public Telefone getTelefone2() {
		return telefone2;
	}
	@Required
	@MaxLength(14)
	public Telefone getCelular1() {
		return celular1;
	}
	@MaxLength(14)
	public Telefone getCelular2() {
		return celular2;
	}
	@Required
	@MaxLength(100)
	@Email
	public String getEmail1() {
		return email1;
	}
	@Email
	@MaxLength(100)
	public String getEmail2() {
		return email2;
	}
	@MaxLength(100)
	public String getWebsite() {
		return website;
	}
	@MaxLength(50)
	public String getFacebook() {
		return facebook;
	}
	@OneToMany(mappedBy="cliente")
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setTipoCliente(EnumTipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(Telefone telefone2) {
		this.telefone2 = telefone2;
	}
	public void setCelular1(Telefone celular1) {
		this.celular1 = celular1;
	}
	public void setCelular2(Telefone celular2) {
		this.celular2 = celular2;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
}
