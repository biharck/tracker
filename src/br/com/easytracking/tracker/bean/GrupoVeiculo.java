package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@Table(name="grupoveiculo")
public class GrupoVeiculo {

	private Integer id;
	private Veiculo veiculo;
	private Grupo grupo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_grupoveiculo_veiculo")
	@DescriptionProperty
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_grupoveiculo_grupo")
	public Grupo getGrupo() {
		return grupo;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	
	
}
