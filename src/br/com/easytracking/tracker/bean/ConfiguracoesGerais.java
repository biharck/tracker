package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="configuracoesgerais")
public class ConfiguracoesGerais extends BeanAuditoria{

	private String usuarioEmail;
	private String senhaEmail;
	private String smtp;
	private String porta;
	
	@Required
	public String getUsuarioEmail() {
		return usuarioEmail;
	}
	@Required
	public String getSenhaEmail() {
		return senhaEmail;
	}
	@Required
	public String getSmtp() {
		return smtp;
	}
	@Required
	public String getPorta() {
		return porta;
	}
	
	public void setUsuarioEmail(String usuarioEmail) {
		this.usuarioEmail = usuarioEmail;
	}
	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}
	public void setPorta(String porta) {
		this.porta = porta;
	}
	public void setSenhaEmail(String senhaEmail) {
		this.senhaEmail = senhaEmail;
	}
}

