package br.com.easytracking.tracker.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="veiculo")
public class Veiculo extends BeanAuditoria{
	
	private Fabricante fabricante;
	private ModeloRastreador modeloRastreador;
	private Rastreador rastreador;
	private Cliente cliente;
	private String placa;
	private int ano;
	private String chassi;
	private String cor;
	private boolean tk;
	private boolean instalador;
	private String observacoes;
	
	
	@Transient
	@Required
	public Fabricante getFabricante() {
		return fabricante;
	}
	@Transient
	public ModeloRastreador getModeloRastreador() {
		return modeloRastreador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_veiculo_rastreador")
	@Required
	public Rastreador getRastreador() {
		return rastreador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_veiculo_cliente")
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	@DescriptionProperty
	@Required
	@MaxLength(8)
	@Column(unique=true)
	public String getPlaca() {
		return placa;
	}

	@Required
	@MinLength(4)
	public int getAno() {
		return ano;
	}
	@Required
	@Column(unique=true)
	public String getChassi() {
		return chassi;
	}
	@MaxLength(45)
	@Required
	public String getCor() {
		return cor;
	}
	@Transient
	public boolean isTk() {
		return tk;
	}
	@Transient
	public boolean isInstalador() {
		return instalador;
	}
	@MaxLength(255)
	public String getObservacoes() {
		return observacoes;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public void setModeloRastreador(ModeloRastreador modeloRastreador) {
		this.modeloRastreador = modeloRastreador;
	}
	public void setRastreador(Rastreador rastreador) {
		this.rastreador = rastreador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public void setChassi(String chassi) {
		this.chassi = chassi;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public void setTk(boolean tk) {
		this.tk = tk;
	}
	public void setInstalador(boolean instalador) {
		this.instalador = instalador;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
}
