package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="municipio")
@DisplayName("Município")
public class Municipio extends BeanAuditoria  {
    
	private String nome; 
	private Uf uf;
	
	//GET
    @Required
    @MaxLength(100)
    @DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_municipio_uf")
	public Uf getUf() {
		return uf;
	}	
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
}