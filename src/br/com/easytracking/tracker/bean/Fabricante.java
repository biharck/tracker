package br.com.easytracking.tracker.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="fabricante")
public class Fabricante extends BeanAuditoria{

	private String nome;
	private String contatoComercial;
	private Telefone telefone1;
	private Telefone telefone2;
	private String email1;
	private String email2;
	private List<ModeloRastreador> modelos;
	
	
	@Required
	@DescriptionProperty
	@MaxLength(45)
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(50)
	public String getContatoComercial() {
		return contatoComercial;
	}
	@Required
	@MaxLength(14)
	public Telefone getTelefone1() {
		return telefone1;
	}
	@MaxLength(14)
	public Telefone getTelefone2() {
		return telefone2;
	}
	@Required
	@MaxLength(100)
	@Email
	public String getEmail1() {
		return email1;
	}
	@MaxLength(100)
	@Email
	public String getEmail2() {
		return email2;
	}
	@OneToMany(mappedBy="fabricante")
	public List<ModeloRastreador> getModelos() {
		return modelos;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setContatoComercial(String contatoComercial) {
		this.contatoComercial = contatoComercial;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(Telefone telefone2) {
		this.telefone2 = telefone2;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	public void setModelos(List<ModeloRastreador> modelos) {
		this.modelos = modelos;
	}
}
