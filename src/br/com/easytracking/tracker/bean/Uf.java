package br.com.easytracking.tracker.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="uf")
public class Uf extends BeanAuditoria  {
    
	private String nome;
	private String sigla;
	private List<Municipio> municipios;
	
	//GET
    @Required
    @MaxLength(value=100)
	public String getNome() {
		return nome;
	}
    @DescriptionProperty
	@Required
	@MaxLength(value=2)
	public String getSigla() {
		return sigla;
	}
	@OneToMany(mappedBy="uf")
	public List<Municipio> getMunicipios() {
		return municipios;
	}
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}
	
}