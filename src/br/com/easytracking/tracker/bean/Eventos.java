package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="eventos")
public class Eventos extends BeanAuditoria{

	private Veiculo veiculo;
	private String menssagem;
	private String dataEvento;
	private boolean visto;
	
	@ForeignKey(name="fk_eventos_veiculo")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@Required
	@MaxLength(100)
	public String getMenssagem() {
		return menssagem;
	}
	@Required
	public String getDataEvento() {
		return dataEvento;
	}
	
	public boolean isVisto() {
		return visto;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setMenssagem(String menssagem) {
		this.menssagem = menssagem;
	}
	public void setDataEvento(String dataEvento) {
		this.dataEvento = dataEvento;
	}
	public void setVisto(boolean visto) {
		this.visto = visto;
	}
}
