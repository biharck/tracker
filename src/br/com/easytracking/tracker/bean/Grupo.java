package br.com.easytracking.tracker.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="grupo")
public class Grupo extends BeanAuditoria{
	
	private String nome;
	private List<GrupoVeiculo> grupoVeiculos;
	private String veiculos;
	
	@Required
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy="grupo")
	public List<GrupoVeiculo> getGrupoVeiculos() {
		return grupoVeiculos;
	}
	@Transient
	public String getVeiculos() {
		return veiculos;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setGrupoVeiculos(List<GrupoVeiculo> grupoVeiculos) {
		this.grupoVeiculos = grupoVeiculos;
	}
	public void setVeiculos(String veiculos) {
		this.veiculos = veiculos;
	}
}
