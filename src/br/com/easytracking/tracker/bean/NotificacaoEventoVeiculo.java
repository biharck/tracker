package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="notificacaoeventoveiculo")
public class NotificacaoEventoVeiculo extends BeanAuditoria{

	private Veiculo veiculo;
	private Boolean emailVelocidade;
	private Boolean emailCorte;
	private Boolean emailAlertaMovimento;
	private Boolean enviarUnicaVez;

	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_veiculo_notificacoes")
	@Required
	public Veiculo getVeiculo() {
		return veiculo;
	}

	public Boolean getEmailVelocidade() {
		return emailVelocidade;
	}

	public Boolean getEmailCorte() {
		return emailCorte;
	}

	public Boolean getEmailAlertaMovimento() {
		return emailAlertaMovimento;
	}

	public Boolean getEnviarUnicaVez() {
		return enviarUnicaVez;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public void setEmailVelocidade(Boolean emailVelocidade) {
		this.emailVelocidade = emailVelocidade;
	}

	public void setEmailCorte(Boolean emailCorte) {
		this.emailCorte = emailCorte;
	}

	public void setEmailAlertaMovimento(Boolean emailAlertaMovimento) {
		this.emailAlertaMovimento = emailAlertaMovimento;
	}
	public void setEnviarUnicaVez(Boolean enviarUnicaVez) {
		this.enviarUnicaVez = enviarUnicaVez;
	}
	
}
