package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="modelorastreador")
public class ModeloRastreador extends BeanAuditoria{
	
	private Fabricante fabricante;
	private String nome;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_modelo_fabricante")
	@Required
	public Fabricante getFabricante() {
		return fabricante;
	}
	@Required
	@MaxLength(45)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
