package br.com.easytracking.tracker.bean;

public enum EnumTipoCliente {
	
	FISICA(0,"Pessoa F�sica"),
	JURIDICA(1,"Pessoa Jur�dica");
	
	private EnumTipoCliente(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}
	
	private Integer id;
	private String descricao;
	
	public Integer getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
