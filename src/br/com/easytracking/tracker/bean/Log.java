package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="log")
public class Log extends BeanAuditoria{

	
	private String evento;
	
	public String getEvento() {
		return evento;
	}
	
	public void setEvento(String evento) {
		this.evento = evento;
	}
}
