package br.com.easytracking.tracker.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="instalacao")
public class Instalacao extends BeanAuditoria {
	
	private Usuario instalador;
	private Rastreador rastreador;
	private String observacoes;
	private Boolean testeCorte;
	private Boolean testeLiberacao;
	private SituacaoRastreador situacaoRastreador;
	//transient
	private Veiculo veiculo;
	private Date dataEntregueInstalacao;
	private Timestamp dataInstalacao;
	private boolean teste_corte = false;
	private boolean teste_liberar = false;
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_instalacao_instalador")
	public Usuario getInstalador() {
		return instalador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_instalacao_rastreador")
	@Required
	public Rastreador getRastreador() {
		return rastreador;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public Boolean getTesteCorte() {
		return testeCorte;
	}
	public Boolean getTesteLiberacao() {
		return testeLiberacao;
	}
	public void setTesteCorte(Boolean testeCorte) {
		this.testeCorte = testeCorte;
	}
	public void setTesteLiberacao(Boolean testeLiberacao) {
		this.testeLiberacao = testeLiberacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_instalacao_situacao")
	@Required
	public SituacaoRastreador getSituacaoRastreador() {
		return situacaoRastreador;
	}
	@Transient
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@Required
	public Date getDataEntregueInstalacao() {
		return dataEntregueInstalacao;
	}
	public Timestamp getDataInstalacao() {
		return dataInstalacao;
	}
	@Transient
	public boolean isTeste_corte() {
		return teste_corte;
	}
	@Transient
	public boolean isTeste_liberar() {
		return teste_liberar;
	}
	public void setInstalador(Usuario instalador) {
		this.instalador = instalador;
	}
	public void setRastreador(Rastreador rastreador) {
		this.rastreador = rastreador;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setSituacaoRastreador(SituacaoRastreador situacaoRastreador) {
		this.situacaoRastreador = situacaoRastreador;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setDataEntregueInstalacao(Date dataEntregueInstalacao) {
		this.dataEntregueInstalacao = dataEntregueInstalacao;
	}
	public void setDataInstalacao(Timestamp dataInstalacao) {
		this.dataInstalacao = dataInstalacao;
	}
	public void setTeste_corte(boolean teste_corte) {
		this.teste_corte = teste_corte;
	}
	public void setTeste_liberar(boolean teste_liberar) {
		this.teste_liberar = teste_liberar;
	}
}
