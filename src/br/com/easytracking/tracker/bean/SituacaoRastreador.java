package br.com.easytracking.tracker.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="situacaorastreador")
@DisplayName("Situa��o dos Rastreadores")
public class SituacaoRastreador extends BeanAuditoria{
	
	private String descricao;

	@Required
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
