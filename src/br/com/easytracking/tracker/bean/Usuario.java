package br.com.easytracking.tracker.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.authorization.User;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa um usu�rio no sistema
 */
@Entity
@Table(name="usuario")
public class Usuario extends BeanAuditoria implements User {
    
	private String nome;
	private String login;
	private String senha;
	private String reSenha;
	private String email;
	private String reEmail;
	private boolean ativo;
	private boolean senhaProvisoria;
	private Timestamp dtUltimoLogin;
	private String senhaAnterior;
	private String senhaAnterior2;
	private Timestamp dataRequisicao;
	private List<Papel> papeis;
	private List<PapelUsuario> papeisUsuario;
	private Cliente cliente;
	
	
	//TRANSIENT
	@Transient
	public String getPassword() {
		return getSenha();
	}
	@Required
	@Transient
	public List<Papel> getPapeis() {
		return papeis;
	}
	//GET
	@DescriptionProperty
    @Required
    @MaxLength(50)
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(30)
	public String getLogin() {
		return login;
	}
	@Password
	@Required
	@MaxLength(20)
	public String getSenha() {
		return senha;
	}
	@Password
	@Required
	@Transient
	public String getReSenha() {
		return reSenha;
	}
	@Email
	@Required
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	@Transient
	public String getReEmail() {
		return reEmail;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public boolean isSenhaProvisoria() {
		return senhaProvisoria;
	}
	public Timestamp getDtUltimoLogin() {
		return dtUltimoLogin;
	}
	@Password
	public String getSenhaAnterior() {
		return senhaAnterior;
	}
	@Password
	public String getSenhaAnterior2() {
		return senhaAnterior2;
	}
	public Timestamp getDataRequisicao() {
		return dataRequisicao;
	}
	@OneToMany(mappedBy="usuario")
	public List<PapelUsuario> getPapeisUsuario() {
		return papeisUsuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_usuario_cliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setReSenha(String reSenha) {
		this.reSenha = reSenha;
	}
	public void setSenhaProvisoria(boolean senhaProvisoria) {
		this.senhaProvisoria = senhaProvisoria;
	}
	public void setDtUltimoLogin(Timestamp dtUltimoLogin) {
		this.dtUltimoLogin = dtUltimoLogin;
	}
	public void setSenhaAnterior(String senhaAnterior) {
		this.senhaAnterior = senhaAnterior;
	}
	public void setSenhaAnterior2(String senhaAnterior2) {
		this.senhaAnterior2 = senhaAnterior2;
	}
	public void setPapeisUsuario(List<PapelUsuario> papeisUsuario) {
		this.papeisUsuario = papeisUsuario;
	}	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void addPapeis(Papel papel) {
		if(this.papeis == null)
			this.papeis = new ArrayList<Papel>();
		this.papeis.add(papel);
	}
	public void setDataRequisicao(Timestamp dataRequisicao) {
		this.dataRequisicao = dataRequisicao;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
}