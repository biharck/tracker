package br.com.easytracking.tracker.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="rastreador")
public class Rastreador extends BeanAuditoria{
	
	private String imei;
	private String idRastreador;
	private Fabricante fabricante;
	private ModeloRastreador modeloRastreador;
	private SituacaoRastreador situacaoRastreador;
	private Chip chip;
	
	@Required
	@DescriptionProperty
	@MaxLength(45)
	@Column(unique=true)
	public String getImei() {
		return imei;
	}
	@Required
	public String getIdRastreador() {
		return idRastreador;
	}
	
	@Transient
	@Required
	public Fabricante getFabricante() {
		return fabricante;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_rastreador_modelo")
	public ModeloRastreador getModeloRastreador() {
		return modeloRastreador;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_rastreador_situacao")
	public SituacaoRastreador getSituacaoRastreador() {
		return situacaoRastreador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_rastreador_chip")
	public Chip getChip() {
		return chip;
	}
	
	public void setImei(String imei) {
		this.imei = imei;
	}
	public void setIdRastreador(String idRastreador) {
		this.idRastreador = idRastreador;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public void setModeloRastreador(ModeloRastreador modeloRastreador) {
		this.modeloRastreador = modeloRastreador;
	}
	public void setSituacaoRastreador(SituacaoRastreador situacaoRastreador) {
		this.situacaoRastreador = situacaoRastreador;
	}
	public void setChip(Chip chip) {
		this.chip = chip;
	}
}
