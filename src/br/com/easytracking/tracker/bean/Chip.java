package br.com.easytracking.tracker.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="chip")
@DisplayName("Chip")
public class Chip extends BeanAuditoria{
	
	private Operadora operadora;
	private Telefone numero;
	private String pin;
	private String pin2;
	private String puk;
	private String puk2;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_chip_operadora")
	@Required
	public Operadora getOperadora() {
		return operadora;
	}
	@Required
	@MaxLength(14)
	@DescriptionProperty
	@Column(unique=true)
	public Telefone getNumero() {
		return numero;
	}
	@Required
	@MaxLength(45)
	public String getPin() {
		return pin;
	}
	@Required
	@MaxLength(45)
	public String getPin2() {
		return pin2;
	}
	@Required
	@MaxLength(45)
	public String getPuk() {
		return puk;
	}
	@MaxLength(45)
	@Required
	public String getPuk2() {
		return puk2;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	public void setNumero(Telefone numero) {
		this.numero = numero;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public void setPin2(String pin2) {
		this.pin2 = pin2;
	}
	public void setPuk(String puk) {
		this.puk = puk;
	}
	public void setPuk2(String puk2) {
		this.puk2 = puk2;
	}
}
