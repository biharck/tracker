package br.com.easytracking.tracker.publico.controller.crud;


import java.io.IOException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.bean.annotation.Bean;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;

import br.com.easytracking.tracker.bean.Municipio;
import br.com.easytracking.tracker.bean.Webservicecep;
import br.com.easytracking.tracker.service.MunicipioService;

import com.google.gson.Gson;

@Bean
@Controller(path="/publico/process/BuscaCep")
public class BuscaCepProcess extends MultiActionController {

	private MunicipioService municipioService;
	
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	
	/**
	 * <p>M�todo Ajax utilizado para buscar endre�o a partir de um cep
	 * @param request {@link WebRequestContext}
	 * @throws JSONException 
	 * @throws IOException 
	 */
	@DefaultAction
	public void ajaxBuscaCep(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			String cep = request.getParameter("cep");
			
			JAXBContext jc = JAXBContext.newInstance(Webservicecep.class);
			Unmarshaller u = jc.createUnmarshaller();
			URL url = new URL(
					"http://cep.republicavirtual.com.br/web_cep.php?cep="+cep+"&formato=xml");
			Webservicecep obj = (Webservicecep) u.unmarshal(url);
			
			//valida CEP
			if(!obj.getResultado().equals("0")){
				//busca id municipior e uf a patir dos nomes recebidos
				Municipio m = municipioService.loadByNome(obj.getCidade());
				obj.setIdMunicipio(m.getId().toString());
				obj.setIdUf(m.getUf().getId().toString());
				String objJSONString = gson.toJson(obj); //transforma obj para formato Json
				jsonObj.put("obj", objJSONString);
				jsonObj.put("erro", false);
			}else{
				jsonObj.put("erro", true);
				jsonObj.put("msg", "Ups! O CEP informado n�o foi localizado!");

			}
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
}
