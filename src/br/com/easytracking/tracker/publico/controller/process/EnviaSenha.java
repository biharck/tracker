package br.com.easytracking.tracker.publico.controller.process;

import java.sql.Timestamp;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MessageType;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.service.UsuarioService;
import br.com.easytracking.tracker.util.PathUtil;
import br.com.easytracking.tracker.util.SCRException;
import br.com.easytracking.tracker.util.TrackerUtil;


@Controller(path = "/publico/process/EnviaSenha")
public class EnviaSenha extends MultiActionController {

	private UsuarioService usuarioService;
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	/**
	 * Action que envia para a p�gina de login
	 */
	@DefaultAction
	public ModelAndView doPage(WebRequestContext request){
		return new ModelAndView("redirect:/autenticacao/Login");
	}
	
	@Action("pesquisarDados")
	public ModelAndView pesquisarDados(WebRequestContext context) {
		String email = String.valueOf(context.getParameter("email"));
		Usuario usuario = usuarioService.findUserEmail(email);
		if (usuario == null) {
			context.addMessage("Usu�rio n�o cadastrado no sistema.", MessageType.ERROR);
			context.setAttribute("MSGEmailInvalid", true);
			return doPage(context);
		} else {
			
			Timestamp dtHoraAtual = new Timestamp(System.currentTimeMillis());
			String senhaTemporaria = TrackerUtil.senhaTemporaria(); 
			usuario.setSenhaAnterior2(usuario.getSenhaAnterior());
			usuario.setSenhaAnterior(usuario.getSenha());
			usuario.setDataRequisicao(dtHoraAtual);
		    usuario.setReSenha(senhaTemporaria);
		    usuario.setSenha(senhaTemporaria);
		    usuario.setSenha(usuarioService.encryptPassword(usuario));
		    usuario.setSenhaProvisoria(true); //ser� solicitado altera��o de senha no pr�ximo login
		    
		    usuarioService.alteraSenhaAndDataRequisicao(usuario);
			
			try {
				TrackerUtil.enviaEmailRecuperaSenha(usuario,senhaTemporaria);
			} catch (Exception e) {
				e.printStackTrace();
				throw new SCRException("N�o foi poss�vel enviar a senha para o seu e-mail!");
			}
			context.addMessage("Os passos para altera��o de senha foram enviados para seu email.");
		}
		return doPage(context);
	}

}
