
package br.com.easytracking.tracker.util;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import org.nextframework.authorization.User;
import org.nextframework.core.standard.Next;
import org.nextframework.core.web.NextWeb;
import org.nextframework.report.Report;
import org.nextframework.util.NextImageResolver;

import br.com.easytracking.tracker.bean.Arquivo;
import br.com.easytracking.tracker.bean.ConfiguracoesGerais;
import br.com.easytracking.tracker.bean.Empresa;
import br.com.easytracking.tracker.bean.Log;
import br.com.easytracking.tracker.bean.Papel;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.dao.EmpresaDAO;
import br.com.easytracking.tracker.service.ConfiguracoesGeraisService;
import br.com.easytracking.tracker.service.LogService;
import br.com.easytracking.tracker.service.UsuarioService;

public class TrackerUtil {

	public  static final int QTD_COORDENADAS = 50;
	public  static final int QTD_COORDENADAS_GRUPO = 10;
	
	public static String getLoginUsuarioLogado(){
		return NextWeb.getUser().getLogin();
	}
	
	/**
	 * M�todo para retornar usu�rio logado no sistema Alterado para retornar
	 * 
	 * @return Usuario
	 * @author Biharck
	 */
	public static Usuario getUsuarioLogado() {
		User user = Next.getRequestContext().getUser();
		if (user instanceof Usuario) {
			Usuario pessoa = (Usuario) user;
			return pessoa;
		} else {
			return null;
		}
	}
	
	
	/**
	 * <p>M�todo que devolve somente o id enviado de uma requisi��o junto com o endere�o do pacote
	 * @param values {@link String} contendo o id completo
	 * @return {@link Integer} id
	 */
	public static Integer returnOnlyId(String... values) {
		Pattern pattern = Pattern.compile("\\d");
		StringBuilder result = new StringBuilder();
		Matcher matcher = pattern.matcher(values[0]);
		while (matcher.find())
			result.append(matcher.group());
		return Integer.parseInt(result.toString());
	}
	
	
	/**
	  * <p>M�todo que recebe uma classe, o nome do @id da classe e o valor do id, e retorna no formato de um value
	  * do combo. ex br.com.easytracking.tracker.bean.UF[id=10]
	  * @param o {@link Object}
	  * @param nomeId nome do campo com a annotation @id
	  * @param id
	  * @return formato do value do combo
	  * @author biharck
	  */
	 public static String getComboFormat(Object o,String nomeId, Integer id){
		 return o.getClass().getCanonicalName().toString()+"["+nomeId+"="+id+"]";
	 }
	 
	/**
	 * Verifica se o usu�rio logado � do tipo "Administrador Geral" 
	 * 
	 * @throws -
	 *             Se o usu�rio n�o estiver logado
	 * @return TRUE - Se o usu�rio conter em um de seus pap�is o perfil de
	 *         Administrador Geral. 
	 *         FALSE - Se o usu�rio n�o conter algum de seus
	 *         pap�is o perfil de Administrador Geral.
	 */
	public static Boolean isPessoaLogadaAdministrador() {
		Usuario pessoaLogada = UsuarioService.getInstance().findByLogin(NextWeb.getUser().getLogin());
		if (pessoaLogada == null)
			throw new SCRException("N�o foi encontrado usu�rio logado.");
		for (Papel papel : pessoaLogada.getPapeis()) {
			if(papel.getId().equals(1))
				return true;
		}
		return false;
	}
	/**
	 * Verifica se o usu�rio logado � do tipo "Administrador Geral" 
	 * 
	 * @throws -
	 *             Se o usu�rio n�o estiver logado
	 * @return TRUE - Se o usu�rio conter em um de seus pap�is o perfil de
	 *         Administrador Geral. 
	 *         FALSE - Se o usu�rio n�o conter algum de seus
	 *         pap�is o perfil de Administrador Geral.
	 */
	public static Boolean isPessoaLogadaAdministrativo() {
		Usuario pessoaLogada = UsuarioService.getInstance().findByLogin(NextWeb.getUser().getLogin());
		if (pessoaLogada == null)
			throw new SCRException("N�o foi encontrado usu�rio logado.");
		for (Papel papel : pessoaLogada.getPapeis()) {
			if(papel.getId().equals(5))
				return true;
		}
		return false;
	}
	/**
	 * Verifica se o usu�rio logado � do tipo "Administrador Geral" 
	 * 
	 * @throws -
	 *             Se o usu�rio n�o estiver logado
	 * @return TRUE - Se o usu�rio conter em um de seus pap�is o perfil de
	 *         Administrador Geral. 
	 *         FALSE - Se o usu�rio n�o conter algum de seus
	 *         pap�is o perfil de Administrador Geral.
	 */
	public static Boolean isPessoaLogadaInstalador() {
		Usuario pessoaLogada = UsuarioService.getInstance().findByLogin(NextWeb.getUser().getLogin());
		if (pessoaLogada == null)
			throw new SCRException("N�o foi encontrado usu�rio logado.");
		for (Papel papel : pessoaLogada.getPapeis()) {
			if(papel.getId().equals(6))
				return true;
		}
		return false;
	}

	public static Boolean isPessoaLogadaGerente() {
		Usuario pessoaLogada = UsuarioService.getInstance().findByLogin(NextWeb.getUser().getLogin());
		if (pessoaLogada == null)
			throw new SCRException("N�o foi encontrado usu�rio logado.");
		for (Papel papel : pessoaLogada.getPapeis()) {
			if(papel.getId().equals(7))
				return true;
		}
		return false;
	}
	
	public static String getAno(){
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy");
		return format.format(date);
	}
	
	public static String getDia(){
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("dd");
		return format.format(date);
	}
	
	
	public static String  getMes(){

		String meses[] = {"Janeiro", "Fevereiro", 
	            "Mar�o", "Abril", "Maio", "Junho", 
	            "Julho", "Agosto", "Setembro", "Outubro",
		      "Novembro", "Dezembro"};
		   
		   Calendar agora = Calendar.getInstance(); 
		   return meses[agora.get(Calendar.MONTH)];
		}
	
	/**
	 * <p>M�todo respons�vel em retornar uma string rand�mica
	 * @return {@link String} contendo o valor rand�mico
	 * 
	 */
	public static String randon() {
		String valor = "";
		for (int i = 0; i < 50; i++) {
			if (1 + (int) (Math.random() * 10) >= 5)
				valor = valor + (char) (Math.random() * 26 + 65);
			else
				valor = valor + (int) (Math.random() * 10);
		}
		return valor;
	}

	
	
	
	public static String getDataHora(String formato){
		DateFormat format = new SimpleDateFormat(formato);  
		String data = format.format(System.currentTimeMillis());	
		return data;
	}
	
	public static String getDataHora(String formato, Date date){
		DateFormat format = new SimpleDateFormat(formato);  
		String data = format.format(date);	
		return data;
	}

	
	public static File getFileByDados(String name, byte[] bytes) {
		File file = null;
		try {
			file = new File(name);
			FileOutputStream out = new FileOutputStream(file);
			out.write(bytes);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return file;
	}
	
	/**
	 * <b>M�todo respons�vel em incrementar datas</b>
	 * 
	 * @param data - Data a ser incrementada.
	 * @param num - N�mero de vezes que um campo ser� incrementado.
	 * @param field - Campo a ser incrementado. <B>Exemplo:</b> <code>Calendar.DAY_OF_MONTH</code>
	 * @return java.sql.Date
	 * 
	 * @author Biharck
	 */
	public static java.sql.Date incrementDate(java.sql.Date data, int num, int field){
		Calendar dt = Calendar.getInstance();
		dt.setTimeInMillis(data.getTime());
		dt.add(field, num);
		return new java.sql.Date(dt.getTimeInMillis());
	}
	
	private static void sendMail(String templateName,String assunto,String destinatarios, String... param) throws IOException, Exception{
		TemplateManager template = new TemplateManager("WEB-INF/template/"+templateName);
		// adiciona as informa��es do email ao template
		try{
			for (int i = 0; i < param.length; i++) {
				template.assign(param[i].split(":")[0], param[i].split(":")[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ConfiguracoesGerais config = ConfiguracoesGeraisService.getInstance().getConfiguracoesGerais();
		
		
		EmailUtil emailDepart = new EmailUtil(config.getUsuarioEmail(), config.getUsuarioEmail(), 
				config.getSenhaEmail(), config.getSmtp(), config.getPorta(), "true", destinatarios, assunto, template.getTemplate());
//		emailDepart.addHtmlText(template.getTemplate());
//		emailDepart.enviar();
		emailDepart.sendMailGoogle();
		
		
	}
	
	/**
	 * <p>M�todo respons�vel em retornar uma string contendo uma senha tempor�ria
	 * @return {@link String} contendo o valor rand�mico
	 * 
	 */
	public static String senhaTemporaria() {
		String valor = "";
		for (int i = 0; i < 8; i++) {
			if (1 + (int) (Math.random() * 10) >= 5)
				valor = valor + (char) (Math.random() * 26 + 65);
			else
				valor = valor + (int) (Math.random() * 10);
		}
		return valor;
	}
	
	public static void enviaEmailNovoUsuario(Usuario user, String senhaTemporaria) throws MessagingException, IOException{
		
		String assunto = "Ol� "+user.getNome()+" - Bem Vindo ao Sistema de Controle de Res�duos.";
		
		try {
			sendMail("templateNovoUsuario.tpl", assunto, "email",
							"usuario:"+user.getNome(),
							"login:"+user.getLogin(),
							"senha:"+senhaTemporaria
							);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void enviaEmailRecuperaSenha(Usuario usuario, String senhaTemporaria) throws Exception {
		String dataHora = getDataHora("dd/MM/yyyy HH:mm");
		String assunto = "Tracker - "+dataHora+" - Solicita��o de Recastramento de Nova Senha";
			sendMail("templateEnvioSenha.tpl", assunto, "email",
					"nome:"+usuario.getNome(),
					"senha:"+senhaTemporaria);
	}
		
	public static void addCabecarioReport(Report report){
		Empresa empresa = EmpresaDAO.getInstance().getEmpresa();
		Arquivo arquivo = empresa.getLogomarca();
		Image imageLogo;
		if(arquivo != null){
			try {
				imageLogo = NextImageResolver.getInstance().getImage(PathUtil.LOGO_RELATORIO);
			} catch (IOException e) {
				throw new SCRException("Falha ao carregar logo do relat�rio.");
			}
			report.addParameter("logo",imageLogo);
		}
		
		report.addParameter("empresa", empresa.getNomeFantasia());
		report.addParameter("data", new Date());
	}
	
	/**
	  * Transforma uma string do tipo 'dd/MM/yyyy' em um objeto java.sql.Date.
	  * 
	  * @param string
	  * @return
	  * 
	  * @author biharck
	  */
	 public static java.sql.Date stringToDate(String string){
	  
	  int dia = Integer.parseInt(string.substring(0, 2));
	  int mes = Integer.parseInt(string.substring(3, 5));
	  int ano = Integer.parseInt(string.substring(6, 10));
	  
	  Calendar calendar = Calendar.getInstance();
	  calendar.set(Calendar.DAY_OF_MONTH, dia);
	  calendar.set(Calendar.MONTH, mes-1);
	  calendar.set(Calendar.YEAR, ano);
	  
	  return new java.sql.Date(calendar.getTimeInMillis());
	 }
	 
	 //M�todo respons�vel por retirar os caracteres (,) , (.)  , (/)
	 
	public static String retiraMascara(String valor) {
		if (valor != null && valor!=null) {
			if (valor.contains("."))
				valor = valor.replaceAll("\\.", "");
			if (valor.contains(","))
				valor = valor.replaceAll("\\,", "");
			if (valor.contains(" "))
				valor = valor.replaceAll(" ", "");
			if (valor.contains("/"))
				valor = valor.replaceAll("/", "");
			if (valor.contains("-"))
				valor = valor.replaceAll("-", "");
			if (valor.contains(")"))
				valor = valor.replaceAll(")", "");
			if (valor.contains("("))
				valor = valor.replaceAll("(", "");
		}
		
		return valor;

	}
	
	
	public static boolean isCNPJValido(String CNPJ) {
		
		CNPJ = retiraMascara(CNPJ);
		// considera-se erro CNPJ's formados por uma sequencia de numeros iguais
		if (CNPJ.equals("00000000000000") || CNPJ.equals("11111111111111")
				|| CNPJ.equals("22222222222222")
				|| CNPJ.equals("33333333333333")
				|| CNPJ.equals("44444444444444")
				|| CNPJ.equals("55555555555555")
				|| CNPJ.equals("66666666666666")
				|| CNPJ.equals("77777777777777")
				|| CNPJ.equals("88888888888888")
				|| CNPJ.equals("99999999999999") || (CNPJ.length() != 14))
			return (false);

		char dig13, dig14;
		int sm, i, r, num, peso;

		// "try" - protege o código para eventuais erros de conversao de tipo
		// (int)
		try {
			// Calculo do 1o. Digito Verificador
			sm = 0;
			peso = 2;
			for (i = 11; i >= 0; i--) {
				// converte o i-ésimo caractere do CNPJ em um número:
				// por exemplo, transforma o caractere '0' no inteiro 0
				// (48 eh a posição de '0' na tabela ASCII)
				num = (int) (CNPJ.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso + 1;
				if (peso == 10)
					peso = 2;
			}

			r = sm % 11;
			if ((r == 0) || (r == 1))
				dig13 = '0';
			else
				dig13 = (char) ((11 - r) + 48);

			// Calculo do 2o. Digito Verificador
			sm = 0;
			peso = 2;
			for (i = 12; i >= 0; i--) {
				num = (int) (CNPJ.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso + 1;
				if (peso == 10)
					peso = 2;
			}

			r = sm % 11;
			if ((r == 0) || (r == 1))
				dig14 = '0';
			else
				dig14 = (char) ((11 - r) + 48);

			// Verifica se os dígitos calculados conferem com os dígitos
			// informados.
			if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13)))
				return (true);
			else
				return (false);
		} catch (InputMismatchException erro) {
			return (false);
		}
	}

	public static boolean isCPFValido(String CPF) {

		CPF = retiraMascara(CPF);
		// considera-se erro CPF's formados por uma sequencia de numeros iguais
		if (CPF.equals("00000000000") || CPF.equals("11111111111")
				|| CPF.equals("22222222222") || CPF.equals("33333333333")
				|| CPF.equals("44444444444") || CPF.equals("55555555555")
				|| CPF.equals("66666666666") || CPF.equals("77777777777")
				|| CPF.equals("88888888888") || CPF.equals("99999999999")
				|| (CPF.length() != 11))
			return (false);

		char dig10, dig11;
		int sm, i, r, num, peso;

		// "try" - protege o codigo para eventuais erros de conversao de tipo
		// (int)
		try {
			// Calculo do 1o. Digito Verificador
			sm = 0;
			peso = 10;
			for (i = 0; i < 9; i++) {
				// converte o i-esimo caractere do CPF em um numero:
				// por exemplo, transforma o caractere '0' no inteiro 0
				// (48 eh a posicao de '0' na tabela ASCII)
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig10 = '0';
			else
				dig10 = (char) (r + 48); // converte no respectivo caractere
											// numerico

			// Calculo do 2o. Digito Verificador
			sm = 0;
			peso = 11;
			for (i = 0; i < 10; i++) {
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig11 = '0';
			else
				dig11 = (char) (r + 48);

			// Verifica se os digitos calculados conferem com os digitos
			// informados.
			if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
				return (true);
			else
				return (false);
		} catch (InputMismatchException erro) {
			return (false);
		}
	}
	
	public static boolean isCPFouCNPJValido(String cpfcnpj){
		if(cpfcnpj.length()> 14)
			return isCNPJValido(cpfcnpj);
		else 
			return isCPFValido(cpfcnpj);
	}
	
	public static String getCommandType(int key){
		switch (key) {
			case 1:
				return "Cortar o Combust�vel";
			case 2:
				return "Liberar o Combust�vel";
			case 3:
				return "Ativar Limite de Velocidade";
			case 4:
				return "Desativar o Limite de Velocidade";
			case 5:
				return "Desativar o P�nico";
			case 6:
				return "Solicitar nova Coordenada";
			default:
				return "";

		}
	}
	
	public static void addLog(String evento){
		Log log = new Log();
		log.setEvento(evento);
		LogService.getInstance().saveOrUpdate(log);
	}
	
}
