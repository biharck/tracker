package br.com.easytracking.tracker.util;

public class SCRException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SCRException() {
		super();
	}

	public SCRException(String message, Throwable cause) {
		super(message, cause);
	}

	public SCRException(String message) {
		super(message);
	}

	public SCRException(Throwable cause) {
		super(cause);
	}

}
