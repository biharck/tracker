package br.com.easytracking.tracker.adm.controller;

import org.nextframework.authorization.DefaultAuthorizationProcess;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/adm/Permissao",authorizationModule=ProcessAuthorizationModule.class)
public class PermissaoController extends DefaultAuthorizationProcess {
	@Override
	protected ModelAndView getModelAndView() {
		return new ModelAndView("permissao");
	}
}
