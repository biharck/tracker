package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.bean.Operadora;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Operadora", authorizationModule = CrudAuthorizationModule.class)
public class OperadoraCrud extends CrudControllerTracker<FiltroListagem, Operadora, Operadora>{
	
	@Override
	protected void salvar(WebRequestContext request, Operadora bean) {
		TrackerUtil.addLog("Nova Operadora Criada no Sistema");
		super.salvar(request, bean);
	}

}
