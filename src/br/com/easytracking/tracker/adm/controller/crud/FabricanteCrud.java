package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.bean.Fabricante;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Fabricante", authorizationModule = CrudAuthorizationModule.class)
public class FabricanteCrud extends CrudControllerTracker<FiltroListagem, Fabricante, Fabricante>{
	
	@Override
	protected void salvar(WebRequestContext request, Fabricante bean) {
		TrackerUtil.addLog("Novo Fabricante Criado no Sistema");
		super.salvar(request, bean);
	}

}
