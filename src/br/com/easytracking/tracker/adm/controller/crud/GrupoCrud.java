package br.com.easytracking.tracker.adm.controller.crud;

import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.bean.Grupo;
import br.com.easytracking.tracker.bean.GrupoVeiculo;
import br.com.easytracking.tracker.service.GrupoService;
import br.com.easytracking.tracker.util.DatabaseError;

@Controller(path = "/adm/crud/Grupo", authorizationModule = CrudAuthorizationModule.class)
public class GrupoCrud extends CrudControllerTracker<FiltroListagem, Grupo, Grupo>{

	private GrupoService grupoService;
	public void setGrupoService(GrupoService grupoService) {
		this.grupoService = grupoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Grupo form)throws Exception {
		request.setAttribute("veiculos_cliente",null);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Grupo form)throws CrudException {
		try{
			grupoService.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_nome")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Grupo com este Nome cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new FiltroListagem());
	}
	
	@Override
	protected void listagem(WebRequestContext request, FiltroListagem filtro)throws Exception {
		
		List<Grupo> minhaLista = grupoService.findAll();
		for (Grupo grupo : minhaLista) {
			String veiculos = new String();
			Grupo tmp = grupoService.getGrupoCompletoById(grupo.getId());
			for (GrupoVeiculo gv : tmp.getGrupoVeiculos()) {
				veiculos += gv.getVeiculo().getPlaca()+ "  ";
			}
			grupo.setVeiculos(veiculos);
		}
		request.getSession().setAttribute("listaGrupos",minhaLista );
	}
	
	
}
