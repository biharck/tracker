package br.com.easytracking.tracker.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.bean.Municipio;

@Controller(path="/adm/crud/Municipio",authorizationModule=CrudAuthorizationModule.class)
public class MunicipioCrud extends CrudControllerTracker<FiltroListagem,Municipio,Municipio> {
	

}
