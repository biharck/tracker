package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.RastreadorFiltro;
import br.com.easytracking.tracker.bean.Rastreador;
import br.com.easytracking.tracker.service.FabricanteService;
import br.com.easytracking.tracker.service.RastreadorService;
import br.com.easytracking.tracker.util.DatabaseError;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Rastreador", authorizationModule = CrudAuthorizationModule.class)
public class RastreadorCrud extends CrudControllerTracker<RastreadorFiltro, Rastreador, Rastreador>{

	
	private FabricanteService fabricanteService;
	private RastreadorService rastreadorService;
	public void setFabricanteService(FabricanteService fabricanteService) {
		this.fabricanteService = fabricanteService;
	}
	public void setRastreadorService(RastreadorService rastreadorService) {
		this.rastreadorService = rastreadorService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Rastreador form)throws Exception {
		if(form.getId()!=null)
			form.setFabricante(fabricanteService.getFabricanteByModelo(form.getModeloRastreador()));
	}

	@Override
	public ModelAndView doSalvar(WebRequestContext request, Rastreador form)throws CrudException {
		try{
			rastreadorService.saveOrUpdate(form);
			TrackerUtil.addLog("Novo Rastreador Cadastrado no Sistema");
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_chip")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Rastreador com este Chip cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_serial")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Rastreador com este Serial cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new RastreadorFiltro());
	}
	
}
