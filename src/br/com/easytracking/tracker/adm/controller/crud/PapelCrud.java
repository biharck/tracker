package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.PapelFiltro;
import br.com.easytracking.tracker.bean.Papel;
import br.com.easytracking.tracker.util.DatabaseError;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Papel", authorizationModule = CrudAuthorizationModule.class)
public class PapelCrud extends CrudControllerTracker<PapelFiltro, Papel, Papel> {

	@Override
	protected void entrada(WebRequestContext request, Papel form)throws Exception {
		request.setAttribute("notBeanAuditoria", true);
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Papel form)	throws CrudException {
		TrackerUtil.addLog("Novo Papel Criado no Sistema");
		try{
			salvar(request, form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_nome")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Papel com este nome cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new PapelFiltro());
	}
	
}
