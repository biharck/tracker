package br.com.easytracking.tracker.adm.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.ClienteFiltro;
import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Papel;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.service.ClienteService;
import br.com.easytracking.tracker.service.UfService;
import br.com.easytracking.tracker.service.UsuarioService;
import br.com.easytracking.tracker.util.DatabaseError;
import br.com.easytracking.tracker.util.PapelUtil;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Cliente", authorizationModule = CrudAuthorizationModule.class)
public class ClienteCrud extends CrudControllerTracker<ClienteFiltro, Cliente, Cliente>{
	
	private UfService ufService;
	private UsuarioService usuarioService;
	private ClienteService clienteService;
	private TransactionTemplate transactionTemplate;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cliente form) throws Exception {

		if(form!=null && form.getId()!=null){
			form.setUf(ufService.getUfByMunicipio(form.getMunicipio()));
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Cliente form)throws CrudException {
		try{
			TrackerUtil.addLog("Novo Cliente Cadastrado no Sistema");
			
			if(form!=null && form.getId()==null){
				clienteService.saveOrUpdate(form);
				final Cliente tmp = form;
				final Cliente referencia = new Cliente();
				referencia.setId(tmp.getId());
				transactionTemplate.execute(new TransactionCallback<Object>(){
					public Object doInTransaction(TransactionStatus arg0) {
						Usuario user = new Usuario();
						user.setAtivo(true);
						user.setCliente(referencia);
						user.setEmail(tmp.getEmail1());
						user.setReEmail(tmp.getEmail1());
						user.setLogin(tmp.getCpfCnpj());
						user.setNome(tmp.getRazaoSocial());
						user.setSenha(tmp.getCpfCnpj());
						user.setReSenha(tmp.getCpfCnpj());
						user.setSenhaProvisoria(true);
						
						Papel papel = new Papel();
						
						if(tmp.getTipoCliente().getId()==0)
							papel.setId(PapelUtil.PESSOA_FISICA);
						else
							papel.setId(PapelUtil.FROTISTA);
						
						List<Papel> papeis = new ArrayList<Papel>();
						papeis.add(papel);
						
						user.setPapeis(papeis);
						
						usuarioService.saveOrUpdate(user);
						getRequest().addMessage("Registro salvo com sucesso!");
						
						return null;
					}
					
				});
			}else{
				clienteService.saveOrUpdate(form);
			}
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_cpfcnpj")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Cliente com este CPF/CNPJ cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Cliente com este Email cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new ClienteFiltro());
	}
}
