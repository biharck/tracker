package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.bean.ModeloRastreador;
import br.com.easytracking.tracker.service.ModeloRastreadorService;
import br.com.easytracking.tracker.util.DatabaseError;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/ModeloRastreador", authorizationModule = CrudAuthorizationModule.class)
public class ModeloRastreadorCrud extends CrudControllerTracker<FiltroListagem, ModeloRastreador, ModeloRastreador>{

	private ModeloRastreadorService modeloRastreadorService;
	public void setModeloRastreadorService(ModeloRastreadorService modeloRastreadorService) {
		this.modeloRastreadorService = modeloRastreadorService;
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, ModeloRastreador form)throws CrudException {
		try{
			TrackerUtil.addLog("Novo Modelo de Rastreador Criado no Sistema");
			modeloRastreadorService.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_nome")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Modelo de Rastreador com este nome cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new FiltroListagem());
	}

}
