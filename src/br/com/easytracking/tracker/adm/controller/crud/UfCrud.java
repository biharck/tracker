package br.com.easytracking.tracker.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.bean.Uf;

@Controller(path="/adm/crud/Uf",authorizationModule=CrudAuthorizationModule.class)
public class UfCrud extends CrudControllerTracker<FiltroListagem,Uf,Uf> {
	
	@Override
	protected void salvar(WebRequestContext request, Uf bean)  {
		super.salvar(request, bean);
	}

}
