package br.com.easytracking.tracker.adm.controller.crud;

import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.UsuarioFiltro;
import br.com.easytracking.tracker.bean.NotificacaoEventoVeiculo;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.dao.NotificacaoEventoVeiculoDAO;
import br.com.easytracking.tracker.service.VeiculoService;
import br.com.easytracking.tracker.util.DatabaseError;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Notificacoes", authorizationModule = CrudAuthorizationModule.class)
public class NotificacaoEventoVeiculoCrud extends CrudControllerTracker<FiltroListagem,NotificacaoEventoVeiculo, NotificacaoEventoVeiculo>{

	private VeiculoService veiculoService;
	private NotificacaoEventoVeiculoDAO notificacaoEventoVeiculoDAO;
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setNotificacaoEventoVeiculoDAO(
			NotificacaoEventoVeiculoDAO notificacaoEventoVeiculoDAO) {
		this.notificacaoEventoVeiculoDAO = notificacaoEventoVeiculoDAO;
	}
	
	@Override
	protected void entrada(WebRequestContext request,NotificacaoEventoVeiculo form) throws Exception {
		List<Veiculo> veiculos = veiculoService.getVeiculosByCliente(TrackerUtil.getUsuarioLogado().getCliente());
		getRequest().setAttribute("veiculos", veiculos);
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request,NotificacaoEventoVeiculo form) throws CrudException {
		try{
			notificacaoEventoVeiculoDAO.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_veiculo")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Ve�culo no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new UsuarioFiltro());
	}

}
