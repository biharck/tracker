package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.bean.Instalacao;
import br.com.easytracking.tracker.service.InstalacaoService;
import br.com.easytracking.tracker.service.UsuarioService;
import br.com.easytracking.tracker.util.DatabaseError;

@Controller(path = "/adm/crud/Instalacao", authorizationModule = CrudAuthorizationModule.class)
public class InstalacaoCrud extends CrudControllerTracker<FiltroListagem, Instalacao, Instalacao>{

	private UsuarioService usuarioService;
	private InstalacaoService instalacaoService;
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setInstalacaoService(InstalacaoService instalacaoService) {
		this.instalacaoService = instalacaoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Instalacao form)throws Exception {
		if(form!=null && form.getId()!=null)
			request.setAttribute("inst", true);
		request.setAttribute("instaladores", usuarioService.getUsuariosInstaladores());
	}

	@Override
	public ModelAndView doSalvar(WebRequestContext request, Instalacao form)throws CrudException {
		try{
			if(form!=null && form.getId()!=null){
				form.setTesteCorte(form.isTeste_corte());
				form.setTesteLiberacao(form.isTeste_liberar());
			}
			instalacaoService.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_rastreador")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Instala��o com este Rastreador cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new FiltroListagem());
	}
	
}
