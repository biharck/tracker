package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.VeiculoFiltro;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.service.VeiculoService;
import br.com.easytracking.tracker.util.DatabaseError;

@Controller(path = "/adm/crud/Veiculo", authorizationModule = CrudAuthorizationModule.class)
public class VeiculoCrud extends CrudControllerTracker<VeiculoFiltro, Veiculo,Veiculo>{
	
	private VeiculoService veiculoService;
	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Veiculo form)throws CrudException {
		try{
			veiculoService.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_chassi")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de ve�culo com este chassi cadastrado no sistma.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_placa")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de ve�culo com esta placa cadastrado no sistma.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_serial")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de ve�culo com este Serial cadastrado no sistma.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new VeiculoFiltro());
	}

	
	
	@Override
	protected void entrada(WebRequestContext request, Veiculo form)	throws Exception {
		if(form!=null && form.getId()!=null){
			form = veiculoService.getVeiculo(form);
			if(form!=null && form.getRastreador().getModeloRastreador()!=null){
				form.setModeloRastreador(form.getRastreador().getModeloRastreador());
				form.setFabricante(form.getRastreador().getModeloRastreador().getFabricante());
			}
		}
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		if(getParameter("hidden_menu")!=null)
			request.setAttribute("hidden_menu", getParameter("hidden_menu"));
		return super.doListagem(request, filtro);
	}

}
