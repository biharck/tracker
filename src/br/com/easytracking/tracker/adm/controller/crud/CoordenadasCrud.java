package br.com.easytracking.tracker.adm.controller.crud;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.CoordenadasFiltro;
import br.com.easytracking.tracker.bean.Coordenadas;
import br.com.easytracking.tracker.service.CoordenadasService;
import br.com.easytracking.tracker.util.TrackerUtil;

import com.google.gson.Gson;

@Controller(path="/adm/crud/Coordenadas", authorizationModule=ProcessAuthorizationModule.class)
public class CoordenadasCrud extends CrudControllerTracker<CoordenadasFiltro, Coordenadas, Coordenadas>{

	private CoordenadasService coordenadasService;
	public void setCoordenadasService(CoordenadasService coordenadasService) {
		this.coordenadasService = coordenadasService;
	}
	
	public void ajaxCoordenadasByVeiculo(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			List<Coordenadas> coordenadas = null;
			
			if(request.getParameter("data_inicio")!=null && !request.getParameter("data_inicio").isEmpty()){
				coordenadas = coordenadasService.getCoordenadasByPlaca(request.getParameter("placa"), TrackerUtil.QTD_COORDENADAS, 
						TrackerUtil.getDataHora("yyyy-MM-dd "+request.getParameter("hora_inicio")+":"+getParameter("minuto_inicio")+":00", TrackerUtil.stringToDate(request.getParameter("data_inicio"))), 
						TrackerUtil.getDataHora("yyyy-MM-dd "+getParameter("hora_termino")+":"+getParameter("minuto_termino")+":00", TrackerUtil.stringToDate(request.getParameter("data_fim"))));
			}else if(request.getParameter("serial")!=null){
				coordenadas = coordenadasService.getCoordenadasBySerial(request.getParameter("serial"), TrackerUtil.QTD_COORDENADAS);
			}
			else{
				coordenadas = coordenadasService.getCoordenadasByPlaca(request.getParameter("placa"), TrackerUtil.QTD_COORDENADAS, null, null);
			}
			String objJSONString = gson.toJson(coordenadas); //transforma obj para formato Json
			jsonObj.put("coordenada", objJSONString);
			jsonObj.put("erro", false);
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
	
}
