package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.LogFiltro;
import br.com.easytracking.tracker.bean.Log;

@Controller(path = "/adm/crud/Log", authorizationModule = CrudAuthorizationModule.class)
public class LogCrud extends CrudControllerTracker<LogFiltro, Log, Log>{

}
