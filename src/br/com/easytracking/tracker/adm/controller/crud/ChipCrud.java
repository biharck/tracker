package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.ChipFiltro;
import br.com.easytracking.tracker.bean.Chip;
import br.com.easytracking.tracker.service.ChipService;
import br.com.easytracking.tracker.util.DatabaseError;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Chip", authorizationModule = CrudAuthorizationModule.class)
public class ChipCrud extends CrudControllerTracker<ChipFiltro, Chip, Chip>{
	
	private ChipService chipService;
	
	@Override
	protected void salvar(WebRequestContext request, Chip bean) {
		TrackerUtil.addLog("Novo Chip Criado no Sistema");
		super.salvar(request, bean);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Chip form)throws CrudException {
		try{
			chipService.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_numero")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Chip com este N�mero cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new ChipFiltro());
	}

}
