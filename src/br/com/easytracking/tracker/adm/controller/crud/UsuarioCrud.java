package br.com.easytracking.tracker.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.controller.CrudControllerTracker;
import br.com.easytracking.tracker.adm.filtro.UsuarioFiltro;
import br.com.easytracking.tracker.bean.EnumAtivo;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.dao.UsuarioDAO;
import br.com.easytracking.tracker.service.PapelService;
import br.com.easytracking.tracker.util.DatabaseError;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path = "/adm/crud/Usuario", authorizationModule = CrudAuthorizationModule.class)
public class UsuarioCrud extends CrudControllerTracker<UsuarioFiltro, Usuario, Usuario> {

	private PapelService papelService;
	private UsuarioDAO usuarioDAO;

	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		UsuarioFiltro _filtro = (UsuarioFiltro) filtro;
		//adiciona filtro padr�o status ativado
		if(_filtro.getAtivo()==null){
			_filtro.setAtivo(EnumAtivo.ATIVO);
		}
		
		return super.doListagem(request, filtro);
	}
	
	

	@Override
	protected void entrada(WebRequestContext request, Usuario form) throws Exception {
		if(TrackerUtil.isPessoaLogadaAdministrador())
			request.setAttribute("listaPapel", papelService.findAll());
		else
			request.setAttribute("listaPapel", papelService.findByUsuario(TrackerUtil.getUsuarioLogado()));
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if("salvar".equals(acao)){
			Usuario bean = (Usuario) obj;
			if(bean.getId() == null){
				if(!bean.getSenha().equals(bean.getReSenha()))
					errors.reject("","A senha e a confirma��o devem ser iguais.");
				if(bean.getPapeis()==null || bean.getPapeis().isEmpty())
					errors.reject("","O usu�rio deve ter pelo menos um papel no sistema.");
			}
		}
		super.validate(obj, errors, acao);
	}
	
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Usuario form)throws CrudException {
		try{
			usuarioDAO.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_login")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Usu�rio com este Login cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new UsuarioFiltro());
	}

}
