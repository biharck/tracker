package br.com.easytracking.tracker.adm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nextframework.controller.NoActionHandlerException;
import org.nextframework.controller.crud.CrudController;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.util.SCRException;

public class CrudControllerTracker<FILTRO extends FiltroListagem, FORMBEAN, BEAN> extends CrudController<FiltroListagem, FORMBEAN, BEAN>{

	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,FORMBEAN form) {
		return new ModelAndView("crud/"+getBeanName()+"Entrada");	
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, BEAN bean) {
		try{
			super.salvar(request, bean);
			request.addMessage("Registro salvo com sucesso!");
		}catch (Exception e) {
			throw new SCRException(e.getMessage());
		}
	}
	
	protected void salvarSemMsg(WebRequestContext request, BEAN bean) {
		try{
			super.salvar(request, bean);
		}catch (Exception e) {
			throw new SCRException(e.getMessage());
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, BEAN bean)throws Exception {
		try{
			super.excluir(request, bean);
			request.addMessage("Registro exclu�do com sucesso!");
		}catch (Exception e) {
			e.printStackTrace();
			request.addError("Este registro n�o pode ser exclu�do pois possui refer�ncias com outros registros no sistema!");
		}
	}
	
	@Override
	protected ModelAndView noActionHandler(HttpServletRequest request, HttpServletResponse response, NoActionHandlerException e)throws NoActionHandlerException {
		return sendRedirectToAction(null);
	}
	
}
