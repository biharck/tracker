package br.com.easytracking.tracker.adm.controller.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;

import br.com.easytracking.tracker.bean.Coordenadas;
import br.com.easytracking.tracker.bean.Grupo;
import br.com.easytracking.tracker.bean.GrupoVeiculo;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.service.CoordenadasService;
import br.com.easytracking.tracker.service.GrupoService;
import br.com.easytracking.tracker.util.TrackerUtil;

import com.google.gson.Gson;

@Controller(path="/adm/process/grupoveiculos", authorizationModule=ProcessAuthorizationModule.class)
public class GrupoProcess extends MultiActionController {
		
	private GrupoService grupoService;
	private CoordenadasService coordenadasService;
	
	public void setGrupoService(GrupoService grupoService) {
		this.grupoService = grupoService;
	}
	public void setCoordenadasService(CoordenadasService coordenadasService) {
		this.coordenadasService = coordenadasService;
	}

	@DefaultAction
	public String index(){
		List<Grupo> grupos = grupoService.getGrupoCompletoByCliente(TrackerUtil.getUsuarioLogado().getCliente());
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		for (Grupo grupo : grupos) {
			for (GrupoVeiculo gv : grupo.getGrupoVeiculos()) {
				gv.getVeiculo().setId(grupo.getId());
				veiculos.add(gv.getVeiculo());
			}
		}
		
		getRequest().setAttribute("lista_grupos", grupos);
		getRequest().setAttribute("veiculos_grupos", veiculos);
		return "process/grupo";
	}
	
	public void getCoordenadasByGrupo(WebRequestContext request) throws JSONException, IOException{
		Grupo g = grupoService.getGrupoCompletoById(Integer.parseInt(getParameter("id")));
		List<Coordenadas> c = new ArrayList<Coordenadas>();
		
		for (GrupoVeiculo gv : g.getGrupoVeiculos()) {
			c.addAll(coordenadasService.getCoordenadasByPlaca(gv.getVeiculo().getPlaca(), TrackerUtil.QTD_COORDENADAS_GRUPO, null, null));
		}
		
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			String objJSONString = gson.toJson(c); //transforma obj para formato Json
			jsonObj.put("coordenada", objJSONString);
			jsonObj.put("erro", false);
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}

}
