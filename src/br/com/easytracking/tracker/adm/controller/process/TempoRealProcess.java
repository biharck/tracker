package br.com.easytracking.tracker.adm.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;

import br.com.easytracking.tracker.bean.Instalacao;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.service.InstalacaoService;
import br.com.easytracking.tracker.service.VeiculoService;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path="/adm/process/TempoReal", authorizationModule=ProcessAuthorizationModule.class)
public class TempoRealProcess extends MultiActionController {
	
	private VeiculoService veiculoService;
	private InstalacaoService instalacaoService;
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setInstalacaoService(InstalacaoService instalacaoService) {
		this.instalacaoService = instalacaoService;
	}
	
	@DefaultAction
	public String index(){
		TrackerUtil.addLog("Visualiza��o do tempo Real");
		getRequest().setAttribute("isTempoReal", true);
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		
		if(TrackerUtil.isPessoaLogadaInstalador()){
			List<Instalacao> instalacao = instalacaoService.getByInstalador(TrackerUtil.getUsuarioLogado());
			for (Instalacao i : instalacao) {
				Veiculo v = new Veiculo();
				v.setPlaca(i.getRastreador().getIdRastreador());
				v.setRastreador(i.getRastreador());
				v.setInstalador(true);
				veiculos.add(v);
			}
		}
		else if(TrackerUtil.isPessoaLogadaAdministrador() || TrackerUtil.isPessoaLogadaAdministrativo()){
			//buscando veiculos j� cadastrados
			veiculos = veiculoService.getVeiculosAdministrador();
			for (Veiculo v : veiculos) {
				if(v.getRastreador().getModeloRastreador().getFabricante().getId().equals(1))
					v.setTk(true);
				else
					v.setTk(false);
			}
			
			//agora, buscando veiculos que ainda n�o foram cadastrados e tem somente o imei
			List<Instalacao> instalacao = instalacaoService.getNaoInstalados();
			for (Instalacao i : instalacao) {
				Veiculo v = new Veiculo();
				v.setPlaca(i.getRastreador().getIdRastreador());
				v.setRastreador(i.getRastreador());
				v.setInstalador(true);
				veiculos.add(v);
			}
			
		}else{
			veiculos = veiculoService.getVeiculosByCliente(TrackerUtil.getUsuarioLogado().getCliente());
		
			for (Veiculo v : veiculos) {
				if(v.getRastreador().getModeloRastreador().getFabricante().getId().equals(1))
					v.setTk(true);
				else
					v.setTk(false);
			}
		}
		getRequest().setAttribute("veiculos", veiculos);
		return "process/tempoReal";
	}

}
