package br.com.easytracking.tracker.adm.controller.process;

import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;

import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.service.VeiculoService;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path="/adm/process/Historico", authorizationModule=ProcessAuthorizationModule.class)
public class HistoricoProcess extends MultiActionController {
	
	private VeiculoService veiculoService;
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	@DefaultAction
	public String index(){
		TrackerUtil.addLog("Visualização do Histórico");
		getRequest().setAttribute("isHistorico", true);
		getRequest().setAttribute("isTempoReal", true);
		List<Veiculo> veiculos = veiculoService.getVeiculosByCliente(TrackerUtil.getUsuarioLogado().getCliente());
		for (Veiculo v : veiculos) {
			if(v.getRastreador().getModeloRastreador().getFabricante().getId().equals(2))
				v.setTk(true);
			else
				v.setTk(false);
		}
		getRequest().setAttribute("veiculos", veiculos);
		return "process/historico";
	}

}
