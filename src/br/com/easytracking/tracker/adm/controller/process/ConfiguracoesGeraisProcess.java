package br.com.easytracking.tracker.adm.controller.process;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.bean.ConfiguracoesGerais;
import br.com.easytracking.tracker.dao.ConfiguracoesGeraisDAO;

@Controller(path="/adm/process/ConfiguracoesGerais", authorizationModule=ProcessAuthorizationModule.class)
public class ConfiguracoesGeraisProcess extends MultiActionController {

	ConfiguracoesGeraisDAO configuracoesGeraisDAO;
	public void setConfiguracoesGeraisDAO(ConfiguracoesGeraisDAO configuracoesGeraisDAO) {
		this.configuracoesGeraisDAO = configuracoesGeraisDAO;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request,ConfiguracoesGerais configuracoesGerais){
		configuracoesGerais = configuracoesGeraisDAO.getConfiguracoesGerais();
		
		if(configuracoesGerais==null){
			configuracoesGerais = new ConfiguracoesGerais();
		}
		return new ModelAndView("process/configuracoesGerais","configuracoesGerais",configuracoesGerais);
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, ConfiguracoesGerais bean){
		
		try {
			configuracoesGeraisDAO.saveOrUpdate(bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Falha ao salvar o registro, entre em contato com o administrador do sistema");
		}
		request.addMessage("Configurações atualizadas com sucesso!");
		return index(request, bean);
	}
}
