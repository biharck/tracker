package br.com.easytracking.tracker.adm.controller.process;

import java.io.IOException;
import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;

import br.com.easytracking.services.ReceiveCommandsStub;
import br.com.easytracking.services.ReceiveCommandsStub.Commands;
import br.com.easytracking.services.ReceiveCommandsStub.CommandsResponse;
import br.com.easytracking.tracker.service.VeiculoService;
import br.com.easytracking.tracker.util.TokenUtil;
import br.com.easytracking.tracker.util.TrackerUtil;

/**
 	 * 	    1 - corte
	 * 		2 - liberar
	 * 		3 - ativar limite velocidade
	 * 		4 - desativar limite velocidade
	 * 		5 - desativar p�nico
	 * 		6 - solicitar coordenada
	 * 		7 - sair do modo sleep
 */

@Controller(path="/adm/process/Comandos", authorizationModule=ProcessAuthorizationModule.class)
public class ComandosProcess extends MultiActionController{

	private VeiculoService veiculoService;
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	
	public void evento(String id,String velocidade, int commandType)throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			ReceiveCommandsStub stub = new ReceiveCommandsStub();
			Commands commands = new Commands();
			commands.setCommandType(commandType);
			commands.setId(id);
			commands.setLimiteVelocidade(velocidade);
			commands.setToken(TokenUtil.getToken());
			
			CommandsResponse response = new CommandsResponse();
			
			response = stub.commands(commands);
			System.out.println(response.get_return());

			jsonObj.put("erro", false);
			jsonObj.put("msg", "Comando para "+TrackerUtil.getCommandType(commandType)+" do Ve�culo Enviado com Sucesso!");
		}catch (AxisFault e) {
			e.printStackTrace();
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");

		}catch (RemoteException e) {
			e.printStackTrace();
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");

		} 
		catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		getRequest().getServletResponse().setContentType("application/json");
		getRequest().getServletResponse().setCharacterEncoding("ISO-8859-1");
		getRequest().getServletResponse().getWriter().println(jsonObj);
	}
	
	@Action("cortarCombustivel")
	public void cortarCombustivel(WebRequestContext request) throws JSONException, IOException {
		TrackerUtil.addLog("Solicita��o de Corte de Combust�vel para Ve�culo Placa "+request.getParameter("id"));
		evento(veiculoService.getVeiculoByPlaca(request.getParameter("id")).getRastreador().getIdRastreador(), null, 1);
	}

	public void liberarCombustivel(WebRequestContext request) throws JSONException, IOException{
		TrackerUtil.addLog("Solicita��o de Libera��o de Combust�vel para Ve�culo Placa "+request.getParameter("id"));
		evento(veiculoService.getVeiculoByPlaca(request.getParameter("id")).getRastreador().getIdRastreador(), null, 2);
	}

	public void ativarLimiteVelocidade(WebRequestContext request) throws JSONException, IOException{
		TrackerUtil.addLog("Solicita��o de Limite de Velocidade para Ve�culo Placa "+request.getParameter("id"));
		evento(veiculoService.getVeiculoByPlaca(request.getParameter("id")).getRastreador().getIdRastreador(), request.getParameter("velocidade"), 3);
	}
	public void desativarLimiteVelocidade(WebRequestContext request) throws JSONException, IOException{
		TrackerUtil.addLog("Solicita��o de Desativar Limite de Velocidade para Ve�culo Placa "+request.getParameter("id"));
		evento(veiculoService.getVeiculoByPlaca(request.getParameter("id")).getRastreador().getIdRastreador(), null, 4);		
	}

	public void desativarPanico(WebRequestContext request) throws JSONException, IOException{
		TrackerUtil.addLog("Solicita��o de Desativar P�nico para Ve�culo Placa "+request.getParameter("id"));
		evento(veiculoService.getVeiculoByPlaca(request.getParameter("id")).getRastreador().getIdRastreador(), null, 5);		
	}

	public void solicitarCoordenada(WebRequestContext request) throws JSONException, IOException{
		TrackerUtil.addLog("Solicita��o de Coordenada para Ve�culo Placa "+request.getParameter("id"));
		evento(veiculoService.getVeiculoByPlaca(request.getParameter("id")).getRastreador().getIdRastreador(), null, 6);
	}

	
	public void cortarCombustivelBySerial(WebRequestContext request) throws JSONException, IOException {
		TrackerUtil.addLog("Solicita��o de Corte de Combust�vel para Ve�culo Placa "+request.getParameter("id"));
		evento(request.getParameter("id"), null, 1);
	}

	public void liberarCombustivelBySerial(WebRequestContext request) throws JSONException, IOException{
		TrackerUtil.addLog("Solicita��o de Libera��o de Combust�vel para Ve�culo Placa "+request.getParameter("id"));
		evento(request.getParameter("id"), null, 2);
	}
	
	public void desativarSleep(WebRequestContext request) throws JSONException, IOException{
		
	}

	
}
