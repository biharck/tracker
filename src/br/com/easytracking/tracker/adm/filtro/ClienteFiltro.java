package br.com.easytracking.tracker.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.types.Telefone;

import br.com.easytracking.tracker.bean.EnumTipoCliente;
import br.com.easytracking.tracker.bean.Municipio;
import br.com.easytracking.tracker.bean.Uf;

public class ClienteFiltro extends FiltroListagem {

	private EnumTipoCliente tipoCliente;
	private String razaoSocial;
	private String cpfCnpj;
	private Uf uf;
	private Municipio municipio;
	private Telefone telefone1;
	private String email1;
	
	public EnumTipoCliente getTipoCliente() {
		return tipoCliente;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public Uf getUf() {
		return uf;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public Telefone getTelefone1() {
		return telefone1;
	}
	public String getEmail1() {
		return email1;
	}
	public void setTipoCliente(EnumTipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	
}
