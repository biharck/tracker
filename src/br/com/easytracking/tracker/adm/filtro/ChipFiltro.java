package br.com.easytracking.tracker.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.types.Telefone;

import br.com.easytracking.tracker.bean.Operadora;

public class ChipFiltro extends FiltroListagem {

	private Operadora operadora;
	private Telefone numero;
	
	public Operadora getOperadora() {
		return operadora;
	}
	public Telefone getNumero() {
		return numero;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	public void setNumero(Telefone numero) {
		this.numero = numero;
	}
}
