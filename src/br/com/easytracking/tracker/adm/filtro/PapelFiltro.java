package br.com.easytracking.tracker.adm.filtro;


import org.nextframework.controller.crud.FiltroListagem;


public class PapelFiltro extends FiltroListagem {

	private String nome;
	
	public String getNome() {
		return nome;
	}
	
	
	public void setNome(String nome) {
		this.nome = nome;
	}
  
}
