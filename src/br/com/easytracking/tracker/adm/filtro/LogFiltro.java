package br.com.easytracking.tracker.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.easytracking.tracker.bean.Usuario;


public class LogFiltro extends FiltroListagem {
	
	private String evento;
	private Usuario usuario;
	
	public String getEvento() {
		return evento;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
