package br.com.easytracking.tracker.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

public class CoordenadasFiltro extends FiltroListagem {
	
	private String serial;
	private String date;
	private String moving;
	private String input1;
	private String panic;
	private String output1;

	public String getSerial() {
		return serial;
	}
	public String getDate() {
		return date;
	}
	public String getMoving() {
		return moving;
	}
	public String getInput1() {
		return input1;
	}
	public String getPanic() {
		return panic;
	}
	public String getOutput1() {
		return output1;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setMoving(String moving) {
		this.moving = moving;
	}
	public void setInput1(String input1) {
		this.input1 = input1;
	}
	public void setPanic(String panic) {
		this.panic = panic;
	}
	public void setOutput1(String output1) {
		this.output1 = output1;
	}
}
