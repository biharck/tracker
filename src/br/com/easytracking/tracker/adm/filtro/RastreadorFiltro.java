package br.com.easytracking.tracker.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.easytracking.tracker.bean.Chip;
import br.com.easytracking.tracker.bean.SituacaoRastreador;

public class RastreadorFiltro extends FiltroListagem {

	private String imei;
	private Chip chip;
	private SituacaoRastreador situacaoRastreador;
	private String idRastreador;
	
	public String getImei() {
		return imei;
	}
	public Chip getChip() {
		return chip;
	}
	public String getIdRastreador() {
		return idRastreador;
	}
	public SituacaoRastreador getSituacaoRastreador() {
		return situacaoRastreador;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public void setChip(Chip chip) {
		this.chip = chip;
	}
	public void setIdRastreador(String idRastreador) {
		this.idRastreador = idRastreador;
	}
	public void setSituacaoRastreador(SituacaoRastreador situacaoRastreador) {
		this.situacaoRastreador = situacaoRastreador;
	}
}
