package br.com.easytracking.tracker.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Fabricante;
import br.com.easytracking.tracker.bean.ModeloRastreador;
import br.com.easytracking.tracker.bean.Rastreador;

public class VeiculoFiltro extends FiltroListagem{

	private String placa;
	private Fabricante fabricante;
	private ModeloRastreador modeloRastreador;
	private Cliente cliente;
	private Rastreador rastreador;
	private String chassi;
	
	public String getPlaca() {
		return placa;
	}
	public Fabricante getFabricante() {
		return fabricante;
	}
	public ModeloRastreador getModeloRastreador() {
		return modeloRastreador;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Rastreador getRastreador() {
		return rastreador;
	}
	public String getChassi() {
		return chassi;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public void setModeloRastreador(ModeloRastreador modeloRastreador) {
		this.modeloRastreador = modeloRastreador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setRastreador(Rastreador rastreador) {
		this.rastreador = rastreador;
	}
	public void setChassi(String chassi) {
		this.chassi = chassi;
	}
}
