package br.com.easytracking.tracker.adm.relatorio.filtro;


import java.util.List;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.easytracking.tracker.bean.EnumAtivo;
import br.com.easytracking.tracker.bean.Papel;

public class UsuarioReportFiltro extends FiltroListagem {

	private EnumAtivo ativo;
	private List<Papel> papeis;

	@DisplayName("Status")
	public EnumAtivo getAtivo() {
		return ativo;
	}
	public List<Papel> getPapeis() {
		return papeis;
	}
	
    public void setAtivo(EnumAtivo ativo) {
		this.ativo = ativo;
	}
    public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
}
