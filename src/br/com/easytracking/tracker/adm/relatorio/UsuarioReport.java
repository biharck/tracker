package br.com.easytracking.tracker.adm.relatorio;


import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.ResourceGenerationException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.springframework.web.servlet.ModelAndView;

import br.com.easytracking.tracker.adm.relatorio.filtro.UsuarioReportFiltro;
import br.com.easytracking.tracker.bean.Papel;
import br.com.easytracking.tracker.bean.PapelUsuario;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.dao.UsuarioDAO;
import br.com.easytracking.tracker.service.PapelService;
import br.com.easytracking.tracker.util.TrackerUtil;

@Controller(path="/adm/relatorio/Usuario",authorizationModule=ReportAuthorizationModule.class)
public class UsuarioReport extends ReportController<UsuarioReportFiltro> {
	
	private UsuarioDAO usuarioDAO;
	private PapelService papelService;
	
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, UsuarioReportFiltro filtro)throws Exception {
		
		Report report = new Report("usuario");
		
		TrackerUtil.addCabecarioReport(report);
		
		String status = filtro.getAtivo() != null ? filtro.getAtivo().getNome() : "Ativo/Inativo";
		String	msg = "Status: "+  status;
		if(filtro.getPapeis() !=null && filtro.getPapeis().size()>0){
			msg += " Papel: ";
			for (Papel p : filtro.getPapeis()) 
				msg += papelService.findNomeById(p).getNome() +" ";
		}
		report.addParameter("msg", msg);
		
		List<Usuario> listaUsuario = usuarioDAO.findByFiltro(filtro);
		
		for (Usuario usuario : listaUsuario) {
			for (PapelUsuario papelUsuario  : usuario.getPapeisUsuario()) 
				usuario.addPapeis(papelUsuario.getPapel());
		}
		
		report.setDataSource(listaUsuario);
        report.addSubReport("SUB_PAPEL", new Report("sub_papel"));
        
		return report;
	}
	@Action("filtroUsuario")
	public ModelAndView doFiltro(WebRequestContext request, UsuarioReportFiltro filtro) throws ResourceGenerationException {
		setAttribute("usuarios", usuarioDAO.findByFiltro(filtro));
		request.setAttribute("listaPapel", papelService.findAll());
		return super.doFiltro(request, filtro);
	}
}
