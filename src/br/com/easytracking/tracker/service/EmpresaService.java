package br.com.easytracking.tracker.service;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Empresa;
import br.com.easytracking.tracker.dao.EmpresaDAO;

public class EmpresaService extends GenericService<Empresa>{

	private EmpresaDAO empresaDAO;
	
	public void setEmpresaDAO(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}
	
	public Empresa getEmpresa(){
		return empresaDAO.getEmpresa();
	}
	
	private static EmpresaService instance;
	public static EmpresaService getInstance() {
		if(instance == null){
			instance = Next.getObject(EmpresaService.class);
		}
		return instance;
	}
}
