package br.com.easytracking.tracker.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Grupo;
import br.com.easytracking.tracker.dao.GrupoDAO;

public class GrupoService extends GenericService<Grupo>{

	private GrupoDAO grupoDAO;
	public void setGrupoDAO(GrupoDAO grupoDAO) {
		this.grupoDAO = grupoDAO;
	}
	
	public Grupo getGrupoCompletoById(Integer id){
		return grupoDAO.getGrupoCompleto(id);
	}

	public List<Grupo> getGrupoCompletoByCliente(Cliente c){
		return grupoDAO.getGrupoCompletoByCliente(c);
	}
}
