package br.com.easytracking.tracker.service;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Log;

public class LogService extends GenericService<Log>{
	
	private static LogService instance;
	public static LogService getInstance() {
		if(instance == null){
			instance = Next.getObject(LogService.class);
		}
		return instance;
	}
}
