package br.com.easytracking.tracker.service;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Fabricante;
import br.com.easytracking.tracker.bean.ModeloRastreador;
import br.com.easytracking.tracker.dao.FabricanteDAO;

public class FabricanteService extends GenericService<Fabricante>{

	private FabricanteDAO fabricanteDAO;
	public void setFabricanteDAO(FabricanteDAO fabricanteDAO) {
		this.fabricanteDAO = fabricanteDAO;
	}
	
	public Fabricante getFabricanteByModelo(ModeloRastreador modeloRastreador){
		return fabricanteDAO.getFabricanteByModelo(modeloRastreador);
	}
}
