package br.com.easytracking.tracker.service;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Municipio;
import br.com.easytracking.tracker.bean.Uf;
import br.com.easytracking.tracker.dao.UfDAO;

public class UfService extends GenericService<Uf> {
	
	private UfDAO ufDAO;
	public void setUfDAO(UfDAO ufDAO) {
		this.ufDAO = ufDAO;
	}
	
	public Uf getUfByMunicipio(Municipio m){
		return ufDAO.getUfByMunicipio(m);
	}	
}
