package br.com.easytracking.tracker.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Cliente;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.dao.VeiculoDAO;

public class VeiculoService extends GenericService<Veiculo>{

	private VeiculoDAO veiculoDAO;
	public void setVeiculoDAO(VeiculoDAO veiculoDAO) {
		this.veiculoDAO = veiculoDAO;
	}
	
	public List<Veiculo> getVeiculosByCliente(Cliente cliente){
		return veiculoDAO.getVeiculosByCliente(cliente);
	}
	public List<Veiculo> getVeiculosAdministrador(){
		return veiculoDAO.getVeiculosAdministrador();
	}
	
	
	public Veiculo getVeiculoByPlaca(String placa){
		return veiculoDAO.getVeiculoByPlaca(placa);
	}
	
	public Veiculo getVeiculo(Veiculo veiculo){
		return veiculoDAO.getVeiculo(veiculo);
	}
	
	@Override
	public void saveOrUpdate(Veiculo bean) {
		if(bean!=null && bean.getPlaca()!=null)
			bean.setPlaca(bean.getPlaca().toUpperCase());
		super.saveOrUpdate(bean);
	}
}
