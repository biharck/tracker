package br.com.easytracking.tracker.service;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Municipio;
import br.com.easytracking.tracker.dao.MunicipioDAO;



public class MunicipioService extends GenericService<Municipio> {
	
	private MunicipioDAO municipioDAO;
	public void setMunicipioDAO(MunicipioDAO municipioDAO) {
		this.municipioDAO = municipioDAO;
	}
	
	public Municipio loadByNome(String  nome){
		return municipioDAO.loadByNome(nome);
	}
	
}
