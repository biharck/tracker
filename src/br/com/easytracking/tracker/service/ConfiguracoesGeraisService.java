package br.com.easytracking.tracker.service;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.ConfiguracoesGerais;
import br.com.easytracking.tracker.dao.ConfiguracoesGeraisDAO;

public class ConfiguracoesGeraisService extends GenericService<ConfiguracoesGerais>{

	private ConfiguracoesGeraisDAO configuracoesGeraisDAO;
	public void setConfiguracoesGeraisDAO(ConfiguracoesGeraisDAO configuracoesGeraisDAO) {
		this.configuracoesGeraisDAO = configuracoesGeraisDAO;
	}
	
	public ConfiguracoesGerais getConfiguracoesGerais(){
		return configuracoesGeraisDAO.getConfiguracoesGerais();
	}
	
	
	private static ConfiguracoesGeraisService instance;
	public static ConfiguracoesGeraisService getInstance() {
		if(instance == null){
			instance = Next.getObject(ConfiguracoesGeraisService.class);
		}
		return instance;
	}
}
