package br.com.easytracking.tracker.service;

import java.sql.Timestamp;
import java.util.List;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.dao.UsuarioDAO;
import br.com.easytracking.tracker.util.SCRException;

public class UsuarioService extends GenericService<Usuario> {
		
	private Usuario userEncrypt;
	private UsuarioDAO usuarioDAO;
	
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	private static UsuarioService instance;
	public static UsuarioService getInstance() {
		if(instance == null){
			instance = Next.getObject(UsuarioService.class);
		}
		return instance;
	}
	
	public String encryptPassword(Usuario bean){
		userEncrypt = bean;
		if(userEncrypt.getSenha().equals(userEncrypt.getReSenha())){
			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
			userEncrypt.setSenha(encryptor.encryptPassword(bean.getSenha()));
		}else
			throw new SCRException("A senha e a confirma��o devem ser iguais.");
		return userEncrypt.getSenha();
	}
	
	public boolean isCheckPassword(String senha,String senhaEncrytor ){
		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
		return encryptor.checkPassword(senha, senhaEncrytor);
	}
	
	public boolean isSenhaAnterior(Usuario bean, String newSenha){
		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
		if(encryptor.checkPassword(newSenha,bean.getSenha()) || 
				encryptor.checkPassword(newSenha,bean.getSenhaAnterior()) ||
				encryptor.checkPassword(newSenha,bean.getSenhaAnterior2()))
			return true;
		else
			return false;
	}
	
	public void alteraSenha(Usuario bean){
		usuarioDAO.alteraSenha(bean);
	}
	
	public void alteraSenhaAndDataRequisicao(Usuario bean) {
		usuarioDAO.alteraSenhaAndDataRequisicao(bean);
	}
	
	public void alteraDtUltimoLogin(Usuario bean){
		bean.setDtUltimoLogin(new Timestamp(System.currentTimeMillis()));
		usuarioDAO.alteraDtUltimoLogin(bean);
	}
	
	public Usuario findByLogin(String login) {
		return usuarioDAO.findByLogin(login);
	}
	
	/**
	 * Encontra uma pessoa que possua o respectivo email
	 * @param
	 * @see  br.com.vitapres.geral.dao.PessoaDAO#findUserEmail 
	 * @return Um usuario com base no email
	 */
	public Usuario findUserEmail(String email) {
		return usuarioDAO.findUserEmail(email);
	}
	/**
	 * M�todo respons�vem em criptografar algum valor manualmente
	 * @param bean
	 */
	public String encrypta(String valor){
		String valor2 = "";
		char cletra; 
		int iletra;
		for (int i = 0; i < valor.length(); i++) {
			cletra =  valor.charAt(i);
			iletra =  cletra;
			iletra = iletra + 5;
			valor2 = valor2 + (char) iletra;
		}	
		return  valor2;
	}
	
	/**
	 * <p> M�todo respons�vem em descriptografar algum valor manualmente
	 * @param valor
	 * @return {@link String}
	 */
	public String dsencrypta(String valor){
		String valor2 = "";
		char cletra; 
		int iletra;
		for (int i = 0; i < valor.length(); i++) {
			cletra =  valor.charAt(i);
			iletra =  cletra;
			iletra = iletra - 5;
			valor2 = valor2 + (char) iletra;
		}	
		return  valor2;
	}

	public List<Usuario> getUsuariosInstaladores(){
		return usuarioDAO.getUsuariosInstaladores();
	}
	
}