package br.com.easytracking.tracker.service;

import java.util.List;

import org.nextframework.service.GenericService;



import br.com.easytracking.tracker.bean.Papel;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.dao.PapelDAO;

public class PapelService extends GenericService<Papel> {

	private PapelDAO papelDAO;
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	public List<Papel> findByUsuario(Usuario usuario) {
		return papelDAO.findByUsuario(usuario);
	}
	
	public Papel findNomeById(Papel bean){
		return papelDAO.findNomeById(bean);
	}
	
}
