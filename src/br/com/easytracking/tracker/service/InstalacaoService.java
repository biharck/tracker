package br.com.easytracking.tracker.service;

import java.sql.Timestamp;
import java.util.List;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Instalacao;
import br.com.easytracking.tracker.bean.Rastreador;
import br.com.easytracking.tracker.bean.SituacaoRastreador;
import br.com.easytracking.tracker.bean.Usuario;
import br.com.easytracking.tracker.bean.Veiculo;
import br.com.easytracking.tracker.dao.InstalacaoDAO;

public class InstalacaoService extends GenericService<Instalacao>{

	private VeiculoService veiculoService;
	private InstalacaoDAO instalacaoDAO;
	private RastreadorService rastreadorService;
	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setInstalacaoDAO(InstalacaoDAO instalacaoDAO) {
		this.instalacaoDAO = instalacaoDAO;
	}
	public void setRastreadorService(RastreadorService rastreadorService) {
		this.rastreadorService = rastreadorService;
	}
	
	@Override
	public void saveOrUpdate(Instalacao bean) {
		if(bean!=null && bean.getId()==null){
			SituacaoRastreador situacaoRastreador = new SituacaoRastreador();
			situacaoRastreador.setId(5);
			bean.setSituacaoRastreador(situacaoRastreador);
		}else{
			SituacaoRastreador situacaoRastreador = new SituacaoRastreador();
			situacaoRastreador.setId(6);
			
			Instalacao temp = load(bean);
			
			bean.setDataEntregueInstalacao(temp.getDataEntregueInstalacao());
			bean.setInstalador(temp.getInstalador());
			bean.setRastreador(temp.getRastreador());
			bean.setTimeInc(temp.getTimeInc());
			bean.setUserInc(temp.getUserInc());
			bean.setSituacaoRastreador(situacaoRastreador);
			bean.setTesteCorte(bean.getTesteCorte());
			bean.setTesteLiberacao(bean.getTesteLiberacao());
			bean.setDataInstalacao(new Timestamp(System.currentTimeMillis()));
			
			Veiculo v = new Veiculo();
			v = bean.getVeiculo();
			v.setRastreador(bean.getRastreador());
			
			veiculoService.saveOrUpdate(v);
			
			rastreadorService.updateSituacaoRastreador(bean.getRastreador(), situacaoRastreador);
			
		}
		super.saveOrUpdate(bean);
	}
	
	public void updateSituacao(Instalacao instalacao){
		instalacaoDAO.saveOrUpdate(instalacao);
	}
	
	public List<Instalacao> getByInstalador(Usuario user){
		return instalacaoDAO.getByInstalador(user);
	}
	
	public List<Instalacao> getNaoInstalados(){
		return instalacaoDAO.getNaoInstalados();
	}
	
	public Instalacao getInstalacaoByRastreador(Rastreador rastreador){
		return instalacaoDAO.getInstalacaoByRastreador(rastreador);
	}
}
