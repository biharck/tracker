package br.com.easytracking.tracker.service;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Instalacao;
import br.com.easytracking.tracker.bean.Rastreador;
import br.com.easytracking.tracker.bean.SituacaoRastreador;
import br.com.easytracking.tracker.dao.RastreadorDAO;

public class RastreadorService extends GenericService<Rastreador>{

	private RastreadorDAO rastreadorDAO;
	private InstalacaoService instalacaoService;
	
	public void setRastreadorDAO(RastreadorDAO rastreadorDAO) {
		this.rastreadorDAO = rastreadorDAO;
	}
	public void setInstalacaoService(InstalacaoService instalacaoService) {
		this.instalacaoService = instalacaoService;
	}
	
	public Rastreador getRastreador(Rastreador rastreador){
		return rastreadorDAO.getRastreador(rastreador);
	}
	
	public void updateSituacaoRastreador(Rastreador rastreador, SituacaoRastreador situacaoRastreador){
		rastreadorDAO.updateSituacaoRastreador(rastreador, situacaoRastreador);
	}
	
	@Override
	public void saveOrUpdate(Rastreador bean) {
		if(bean!=null && bean.getId()!=null){
			Instalacao instalacao = instalacaoService.getInstalacaoByRastreador(bean);
			if(instalacao!=null){
				instalacao.setSituacaoRastreador(bean.getSituacaoRastreador());
				instalacaoService.updateSituacao(instalacao);
			}
		}
		super.saveOrUpdate(bean);
	}
	
}
