package br.com.easytracking.tracker.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.easytracking.tracker.bean.Coordenadas;
import br.com.easytracking.tracker.dao.CoordenadasDAO;

public class CoordenadasService extends GenericService<Coordenadas>{

	private CoordenadasDAO coordenadasDAO;
	private VeiculoService veiculoService;
	public void setCoordenadasDAO(CoordenadasDAO coordenadasDAO) {
		this.coordenadasDAO = coordenadasDAO;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	public List<Coordenadas> getCoordenadasByPlaca(String placa, int qtd, String dtInicio, String dtFim){
		return coordenadasDAO.getCoordenadasBySerial(
				veiculoService.getVeiculoByPlaca(placa).getRastreador().getIdRastreador(),qtd, dtInicio, dtFim);
	}

	public List<Coordenadas> getCoordenadasBySerial(String serial,int qtd){
		return coordenadasDAO.getCoordenadasBySerial(serial,qtd,null,null);
	}
}
