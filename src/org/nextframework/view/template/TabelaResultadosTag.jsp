<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<div class="widget">
	<div class="head dark">
	    <div class="icon"><span class="icos-paragraph-justify"></span></div>
	    <h2>Listagem de ${listagemTag.titulo}</h2>                    
	</div>
	<div class="block-fluid">
		<n:dataGrid itens="${TtabelaResultados.itens}" var="${TtabelaResultados.name}" style="widht:100%;" styleClass="tabela-resultados, table-hover" cellspacing="0" groupProperty="${TtabelaResultados.dynamicAttributesMap['groupproperty']}" 
				rowondblclick="javascript:dialogAguarde();$csu.editarRegistro(this);"		 
				
				rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
				rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
				id="tabelaResultados" varIndex="index">
			<n:bean name="${TtabelaResultados.name}" valueType="${TtabelaResultados.valueType}">
				<n:getContent tagName="acaoTag" vars="acoes">
					<t:propertyConfig mode="output" renderAs="column">
						<n:doBody />
					</t:propertyConfig>
					<c:if test="${(!empty acoes) || (TtabelaResultados.showEditarLink) || (TtabelaResultados.showExcluirLink) || (TtabelaResultados.showConsultarLink)}">
						<n:column header="A��o" style="width: 1px; white-space: nowrap; padding-right: 3px;"> <%-- width: 1%;  --%>
							${acoes}
								<script language="javascript">
								<c:catch var="exSelecionar">
									imprimirSelecionar(new Array(${n:hierarchy(TtabelaResultados.valueType)}), 
											"<a href=\"javascript:selecionar('${n:escape(n:valueToString(n:reevaluate(TtabelaResultados.name, pageContext)))}','${n:escape(n:descriptionToString(n:reevaluate(TtabelaResultados.name, pageContext)))}')\">selecionar</a>&nbsp;");
								</c:catch>
								</script>
								<c:if test="${!empty exSelecionar}">
									<span style="font-size: 10px; color: red; white-space: pre; display:block;">Erro ao imprimir bot�o selecionar: ${exSelecionar.message} <c:catch>${exSelecionar.rootCause.message}</c:catch></span>
								</c:if>
							<c:if test="${TtabelaResultados.showConsultarLink}">
								<n:link action="consultar" class="consult tipl" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" onclick="dialogAguarde();" description="Visualizar um registro" ><span class="icon-search"></span></n:link>
							</c:if>						
							<c:if test="${TtabelaResultados.showEditarLink}">
								<n:link action="editar" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" class="tipl" description='Editar um registro'  onclick="dialogAguarde();"><span class="icon-pencil"></span></n:link>
							</c:if>
							<c:if test="${TtabelaResultados.showExcluirLink}">
								<n:link class="tipl" description="Apagar um Registro" confirmationMessage="Deseja realmente excluir esse registro?" action="excluir" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}"><span class="icon-trash"></span></n:link>				
							</c:if>		
						</n:column>
					</c:if>
				</n:getContent>
			</n:bean>
		</n:dataGrid>
		<div class="dr"><span></span></div>
		<div class="toolbar bottom TAL">
			<div class="pagging" >
				P&aacute;gina <n:pagging currentPage="${currentPage}" totalNumberOfPages="${numberOfPages}" selectedClass="pageSelected" unselectedClass="pageUnselected" /> de ${numberOfPages}
			</div>
		</div>
	</div>
</div>
<style type="text/css">
#tabelaResultados{
	width: 100%;
}
</style>