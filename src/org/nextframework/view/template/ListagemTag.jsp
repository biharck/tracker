<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<div class="breadCrumb clearfix">    
    <ul id="breadcrumbs">
        <li><a href="/Tracker/adm">Home</a></li>
        <li>${listagemTag.titulo}</li>
    </ul>        
</div>


<t:tela  validateForm="false">
		<input type="hidden" name="notFirstTime" value="true"/>
		<c:if test="${listagemTag.showNewLink || !empty listagemTag.linkArea}">

			<div class="linkBar">
				${listagemTag.invokeLinkArea}
				<c:if test="${listagemTag.showNewLink}">	
					<c:if test="${empty hideNovo}">					
						<n:link  class="btn btn-primary tipr" description="Criar um Novo Registro" action="criar" onclick="dialogAguarde();">${n:default('Novo', listagemTag.dynamicAttributesMap['novolabel'])}</n:link>
					</c:if>
				</c:if>						
			</div>
		</c:if>	
	
		<div>
			<n:doBody />
		</div>

</t:tela>

