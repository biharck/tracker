<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<div class="widget">
	<div class="head dark">
	    <div class="icon"><i class="icos-pencil2"></i></div>
	    <h2>Cadastro de ${entradaTag.titulo}</h2>
	</div>
	<div class="block-fluid">
			<n:panelGrid columns="${TtabelaEntrada.columns}" style="${TtabelaEntrada.style}" styleClass="${TtabelaEntrada.styleClass}" rowStyleClasses="${TtabelaEntrada.rowStyleClasses}" rowStyles="${TtabelaEntrada.rowStyles}"
				columnStyleClasses="${TtabelaEntrada.columnStyleClasses}" columnStyles="${TtabelaEntrada.columnStyles}" colspan="${TtabelaEntrada.colspan}" propertyRenderAsDouble="${TtabelaEntrada.propertyRenderAsDouble}"
				dynamicAttributesMap="${TtabelaEntrada.dynamicAttributesMap}" cellpadding="1" cellspacing="0">
		
				<t:propertyConfig showLabel="${tag.propertyShowLabel}">
					<n:doBody />
				</t:propertyConfig>
		
			</n:panelGrid>
	</div>
</div>
<style style="text/css">
.inputTable{
	width:100%;
	border: none;
}

.inputTable td{
	border: none;
}
</style>                        
