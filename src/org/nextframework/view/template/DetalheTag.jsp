<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<div class="widget">
	<div class="head dark">
	    <div class="icon"><span class="icos-paragraph-justify"></span></div>
	    <h2>Ve�culos</h2>                    
	</div>
	<div class="block-fluid">

	<n:dataGrid itens="${Tdetalhe.itens}" cellspacing="0" dynaLine="true" id="${Tdetalhe.tableId}"   var="${Tdetalhe.detailVar}" 
			styleClass="tabela-resultados, table-hover"
			>
		<n:bean name="${Tdetalhe.detailVar}" valueType="${Tdetalhe.detailClass}" propertyPrefix="${Tdetalhe.fullNestedName}" propertyIndex="${index}">
			<n:getContent tagName="acaoTag" vars="acoes">
				<t:propertyConfig renderAs="column">
					<n:doBody />
				</t:propertyConfig>
				<c:if test="${Tdetalhe.showColunaAcao && !consultar}">
				<n:column header="${Tdetalhe.nomeColunaAcao}" style="width: 1%; white-space: nowrap; padding-right: 3px;">
					${acoes}
					<c:if test="${Tdetalhe.showBotaoRemover}">
						<c:if test="${!propertyConfigDisabled || dataGridDynaline}">
							<button type="button" class="btn btn-primary tipt" description="Remover Ve�culo" onclick="if(function(button){${Tdetalhe.onRemove}}(this)){excluirLinhaPorNome(this.id);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true)}" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]">
								remover
							</button>
						</c:if>
						<c:if test="${propertyConfigDisabled && !dataGridDynaline}">	
							<button type="button" disabled="disabled" class="btn btn-primary tipt" description="Remover Ve�culo" onclick="if(function(button){${Tdetalhe.onRemove}}(this)){excluirLinhaPorNome(this.id);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true)}" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]">
								remover
							</button>						
						</c:if>					
					</c:if>
				</n:column>
				</c:if>
			</n:getContent>
		</n:bean>
	</n:dataGrid>
	<c:if test="${Tdetalhe.showBotaoNovaLinha && !consultar}">
		<c:if test="${empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
			<c:set value="Adicionar Registro"  scope="page" var="labelnovalinha"/>
		</c:if>
		<c:if test="${!empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
			<c:set value="${Tdetalhe.dynamicAttributesMap['labelnovalinha']}" scope="page" var="labelnovalinha"/>
		</c:if>
		
		<c:if test="${!propertyConfigDisabled}">
			<center>
				<button type="button" class="btn btn-primary tipt" description="Adicionar Ve�culo" onclick="newLine${Tdetalhe.tableId}();${Tdetalhe.onNewLine}">
					${labelnovalinha}
				</button>
			</center>
		</c:if>
		<c:if test="${propertyConfigDisabled}">
			<center>
				<button type="button" class="btn btn-primary tipt" description="Adicionar Ve�culo" disabled="disabled" onclick="newLine${Tdetalhe.tableId}();${Tdetalhe.onNewLine}">
					${labelnovalinha}
				</button>
			</center>
		</c:if>
	</c:if>
	</div>
</div>
