

/**
 * ReceiveCommandsTest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package br.com.easytracking.services;

    /*
     *  ReceiveCommandsTest Junit test case
    */

    public class ReceiveCommandsTest extends junit.framework.TestCase{

     
        /**
         * Auto generated test method
         */
        public  void testcommands() throws java.lang.Exception{

        br.com.easytracking.services.ReceiveCommandsStub stub =
                    new br.com.easytracking.services.ReceiveCommandsStub();//the default implementation should point to the right endpoint

           br.com.easytracking.services.ReceiveCommandsStub.Commands commands4=
                                                        (br.com.easytracking.services.ReceiveCommandsStub.Commands)getTestObject(br.com.easytracking.services.ReceiveCommandsStub.Commands.class);
                    // TODO : Fill in the commands4 here
                
                        assertNotNull(stub.commands(
                        commands4));
                  



        }
        
         /**
         * Auto generated test method
         */
        public  void testStartcommands() throws java.lang.Exception{
            br.com.easytracking.services.ReceiveCommandsStub stub = new br.com.easytracking.services.ReceiveCommandsStub();
             br.com.easytracking.services.ReceiveCommandsStub.Commands commands4=
                                                        (br.com.easytracking.services.ReceiveCommandsStub.Commands)getTestObject(br.com.easytracking.services.ReceiveCommandsStub.Commands.class);
                    // TODO : Fill in the commands4 here
                

                stub.startcommands(
                         commands4,
                    new tempCallbackN65548()
                );
              


        }

        private class tempCallbackN65548  extends br.com.easytracking.services.ReceiveCommandsCallbackHandler{
            public tempCallbackN65548(){ super(null);}

            public void receiveResultcommands(
                         br.com.easytracking.services.ReceiveCommandsStub.CommandsResponse result
                            ) {
                
            }

            public void receiveErrorcommands(java.lang.Exception e) {
                fail();
            }

        }
      
        //Create an ADBBean and provide it as the test object
        public org.apache.axis2.databinding.ADBBean getTestObject(java.lang.Class type) throws java.lang.Exception{
           return (org.apache.axis2.databinding.ADBBean) type.newInstance();
        }

        
        

    }
    