<span style="font-size:20px; color: #2281CF;">Prezado(a) {nome},</span>
<hr style="border: 1px #CDCDCD solid;">
<span style="font-size:12px; color:#555;">
	Uma nova senha foi solicitada no sistema <font style="color:#E77272">Tracker, da empresa EasyTracking.</font> <br />
	
	Para continuar utilizando o sistema,  <a href="https://www.trackerhost.srv.br/Tracker">Clique Aqui</a> e informe a senha tempor&aacute;ria <br />
	fornecida abaixo:<br><br />

	<br />
	<font style="color:#1F75CC;"> 
		Senha Provis&oacute;ria: {senha}
	</font><br />

<br/ >
<i>
Cordialmente,<br/>

<div>
	<font color="#000066" size="1">
		<b><br></b>
	</font>
	<div>
		<div>
			<span style="font-size:x-small">
				<font color="#336666"><img src="http://www.trackhost.srv.br/Tracker/img/topo_login.png" width="96" height="49"><br></font>
			</span>
			<font color="#003333">
				<span style="font-size:x-small">&nbsp;&nbsp;<a href="http://www.easytracking.com.br" target="_blank">http://www.easytracking.com.br</a></span>
			</font><br>
		</div>
	</div>
</div>
</i>
<br />
<br />
<font style="font-size:10px;">
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
</font>

</span>

