<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv='cache-control' content='no-cache' />
<meta http-equiv='expires' content='0' />
<meta http-equiv='pragma' content='no-cache' />
<meta name="ROBOTS" content="NOINDEX,NOFOLLOW" />

<title>OPS ...</title>
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
</head>
<body>
<style>
/* Estilos para a p�gina inicial do painel */
*{
	font-family: 		Droid Sans, sans-serif, tahoma, "Trebuchet MS";
}
body{
	padding:			0px;
	margin:				0px;
	font-size:			16px;
	color:				#666;
}
h1 {
	font-size:			30px;
	margin: 			0 0 4px 0;
	color: 				#096982;
	font-weight:		bold;
}

h2{
	padding: 			0px;
	margin: 			5px 0px 0px;
	text-indent: 		0px;
	color: 				#666;
	float: 				left;
	clear: 				right;
	width: 				100%;
}


#content  {
	padding-left:		386px; 
	margin: 			100px auto;
	width: 				600px;
}
#content_error  {
	padding: 			0;
	width: 				400px;
	margin: 			50px auto;
}

	#content.browser_error{
		width: 					960px;
		margin: 				0px auto;
		padding-left: 			0px; 
	}

	
	.desculpas_wrapper {
		background: 			#fbeff0;
		border-bottom:			1px solid #f2d8d9;
		margin-bottom: 			30px;
		width: 					100%;
	}
	.desculpas {
		background: 			transparent url('/Tracker/img/forbidden.png') no-repeat 150px center;
		color: 					#c25154;
		padding: 				45px 0px 50px 0px;
		width: 					960px;
		margin: 				0px auto;
	}
	.desculpas p {
		margin-left: 			270px;
	}
	.desculpas p b {
		font-size: 				24px;
	}
	
	
	
	.compativeis	{
		background-color: 		#fff7d9;
		border: 				1px solid #ffeda7;
		padding: 				25px;
		width: 					100%;
		float: 					right;
		text-align: 			center;
		border-radius: 			3px;
		-moz-border-radius: 	3px;
		-webkit-border-radius: 	3px;
	}
	.compativeis #logo	{
		margin-top: 			-30px;
		margin-left: 			-350px;
	}
	.compativeis h1	{
		font-size: 				24px;
		padding-bottom: 		10px;
		color: 					#e2730e;
	}
	.compativeis a	{
		background-color: 		#FFF; 
		background-position:	center 30px;
		background-repeat: 		no-repeat;
		border: 				1px solid #ddd;
		border-radius: 			3px;
		-moz-border-radius: 	3px;
		-webkit-border-radius: 	3px;
		cursor: 				pointer;
		display: 				block;
		float:					left;
		font-size: 				14px;
		font-weight: 			normal;
		margin: 				7px 5px 7px;
	    outline: 				0;
		padding: 				80px 60px 25px;
		color: 					#888;
		text-decoration: 		none;
	}
	.compativeis a:hover {
		color: 					#666;
		border: 				1px solid #faa252;
		box-shadow: 			0px 1px 3px #d7d7d7;
		-moz-box-shadow:		0px 1px 3px #d7d7d7;
		-webkit-box-shadow: 	0px 1px 3px #d7d7d7;
	}
	
	.compativeis a.chrome{
		margin-left: 			9%;
		background-image: 		url('/Tracker/img/user_agent_chrome.png');
	}
	.compativeis a.safari{
		background-image: 		url('/Tracker/img/user_agent_safari.jpg');
	}
	.compativeis a.firefox{
		background-image: 		url('/Tracker/img/user_agent_firefox.png');
	}
	
	
	
	
	

</style>
	<div class='desculpas_wrapper'>
		<div class='desculpas'>
			<p><p>O navegador <b>Safari</b> n�o suporta corretamente as fun��es do sistema.</p><p>Por favor instale um dos navegadores compat�veis para continuar.</p></p>		</div>
	</div>
	<div id="content" class='browser_error'>
		<div class='compativeis'>
			<h1>Navegadores Compat�veis</h1>
			<a href="http://www.google.com/chrome" target="_blank" class="chrome">Fazer Download</a>
			<a href="http://mozilla.org/firefox" target="_blank" class="firefox">Fazer Download</a>
			<a href="http://www.apple.com/safari" target="_blank" class="safari">Fazer Download</a>
		</div>
		
	</div>
</body>
</html>