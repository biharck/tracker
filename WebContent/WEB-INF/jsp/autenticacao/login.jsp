<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->        
    
    <title>EasyTracking - Tracker 2.0</title>
    
    <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />      
    <!--[if lt IE 10]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
    <![endif]-->        
    <link rel="icon" type="image/ico" href="/Tracker/img/favicon.ico"/>
      
    <script type='text/javascript' src='js/plugins/jquery/jquery-1.8.3.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/jquery-ui-1.9.1.custom.min.js'></script>
    <script type='text/javascript' src='js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='js/plugins/other/excanvas.js'></script>
    
    <script type='text/javascript' src='js/plugins/other/jquery.mousewheel.min.js'></script>
        
    <script type='text/javascript' src='js/plugins/bootstrap/bootstrap.min.js'></script>            
    
    <script type='text/javascript' src='js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    
    <script type='text/javascript' src='js/plugins/fancybox/jquery.fancybox.pack.js'></script>
    
    <script type='text/javascript' src='js/plugins/jflot/jquery.flot.js'></script>    
    <script type='text/javascript' src='js/plugins/jflot/jquery.flot.stack.js'></script>    
    <script type='text/javascript' src='js/plugins/jflot/jquery.flot.pie.js'></script>
    <script type='text/javascript' src='js/plugins/jflot/jquery.flot.resize.js'></script>
    
    <script type='text/javascript' src='js/plugins/epiechart/jquery.easy-pie-chart.js'></script>
    
    <script type='text/javascript' src='js/plugins/sparklines/jquery.sparkline.min.js'></script>    
    
    <script type='text/javascript' src='js/plugins/pnotify/jquery.pnotify.min.js'></script>
    
    <script type='text/javascript' src='js/plugins/fullcalendar/fullcalendar.min.js'></script>        
    
    <script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>    
    
    <script type='text/javascript' src='js/plugins/wookmark/jquery.wookmark.js'></script>        
    
    <script type='text/javascript' src='js/plugins/jbreadcrumb/jquery.jBreadCrumb.1.1.js'></script>
    
    <script type='text/javascript' src='js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    
    <script type='text/javascript' src="js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='js/plugins/tagsinput/jquery.tagsinput.min.js'></script>
    <script type='text/javascript' src='js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
    <script type='text/javascript' src='js/plugins/multiselect/jquery.multi-select.min.js'></script>    
    
    <script type='text/javascript' src='js/plugins/validationEngine/languages/jquery.validationEngine-en.js'></script>
    <script type='text/javascript' src='js/plugins/validationEngine/jquery.validationEngine.js'></script>        
    <script type='text/javascript' src='js/plugins/stepywizard/jquery.stepy.js'></script>
        
    <script type='text/javascript' src='js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    <script type='text/javascript' src='js/plugins/hoverintent/jquery.hoverIntent.minified.js'></script>
    
    <script type='text/javascript' src='js/plugins/media/mediaelement-and-player.min.js'></script>    
    
    <script type='text/javascript' src='js/plugins/cleditor/jquery.cleditor.js'></script>

    <script type='text/javascript' src='js/plugins/shbrush/shCore.js'></script>
    <script type='text/javascript' src='js/plugins/shbrush/shBrushXml.js'></script>
    <script type='text/javascript' src='js/plugins/shbrush/shBrushJScript.js'></script>
    <script type='text/javascript' src='js/plugins/shbrush/shBrushCss.js'></script>    

    <script type='text/javascript' src='js/plugins/slidernav/slidernav-min.js'></script>    
    <script type='text/javascript' src='js/plugins/isotope/jquery.isotope.min.js'></script>    
    <script type='text/javascript' src='js/plugins/jnotes/jquery-notes_1.0.8_min.js'></script>
    <script type='text/javascript' src='js/plugins/jcrop/jquery.Jcrop.min.js'></script>
    <script type='text/javascript' src='js/plugins/ibutton/jquery.ibutton.min.js'></script>
        
    <script type='text/javascript' src='js/plugins.js'></script>
    <script type='text/javascript' src='js/charts.js'></script>
    <script type='text/javascript' src='js/actions.js'></script>
    
</head>
<body>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

	<style type="text/css">
	input[type="text"], input[type="password"], textarea, select {
		width: 91%;
	}
	</style>
	<script type="text/javascript">
		function enviaSenha(){
			email = $('#email').val();
			if(email==''){
				$('#email').focus();
				dialog('Aten��o','Por favor, informe um e-mail para podermos enviar a nova senha pra voc�!');
				return false;
			}
			
			if(checkMail(email)){
				document.enviaSenhaForm.submit();
			}else{
				$('#email').focus();
				dialog('Aten��o','Por favor, digite um e-mail v�lido!');
				return false;
			}
			return false;
		}
		function checkMail(mail){	
		    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
		    if(typeof(mail) == "string"){
		    	if(mail=="") return true;
		        if(er.test(mail)){ return true; }
		    } else if(typeof(mail) == "object"){
		    	if(mail.value=="") return true;
		        if(er.test(mail.value)){ return true; }
			}	    
			return false;
		}
		
	function identifyBrowser() {
		var ua = navigator.userAgent.toLowerCase();
		if (ua.indexOf("msie") != -1) {
			browserName = "msie";
			location.href = '/Tracker/autenticacao/errobroser'
		} 
	}
	</script>
    
    <div align="center">
    	<img alt="logo, easytracking" src="/Tracker/img/topo_login.png">
    </div>
    
    <div class="login" id="login">
        <div class="wrap">
            <h1>Bom vindo, efetue seu login</h1>
			<n:form validateFunction="validaCamposLogin">
				<n:bean name="usuario">
					<div class="row-fluid">
						<div class="input-prepend">
		                    <span class="add-on"><i class="icon-user"></i></span>
		                    <t:property name="login" id="login" placeholder="Login"/>
		                </div>
						<div class="input-prepend">
		                    <span class="add-on"><i class="icon-exclamation-sign"></i></span>
		                    <t:property name="senha" type="password" label="Senha" placeholder="Senha"/>
		                </div>          
		                <div class="dr"><span></span></div>
					</div>
					<div class="row-fluid">
		                <div class="span4    TAR">
		                    <n:submit type="submit" action="doLogin" style="margin-left: 94px;" class="btn btn-block btn-primary" panelColspan="2" panelAlign="right">Logar</n:submit>
		                </div>
		            </div>
				</n:bean>
			</n:form>
            <div class="dr"><span></span></div>
            <div class="row-fluid">
                <div class="span8">                    
                    <button class="btn btn-block" style="margin-left: 49px;" onClick="loginBlock('#forgot');">Esquec� minha senha!</button>
                </div>
            </div>            
        </div>
    </div>
    
    <div class="login" id="forgot">
        <div class="wrap">
            <form name="enviaSenhaForm" method="POST" action="/Tracker/publico/process/EnviaSenha?ACAO=pesquisarDados">
	            <h1>Esqueceu sua senha?</h1>
	            <div class="row-fluid">
	                <p>Pro favor, informe seu email para recuperar sua senha!</p>
	                <div class="input-prepend">
	                    <span class="add-on"><i class="icon-envelope"></i></span>
	                    <input type="text" name="email" id="email" placeholder="E-mail"/>
	                </div>                                                           
	                <div class="dr"><span></span></div>                               
	            </div>                   
         </form>                                  
	            <div class="row-fluid">
	                <div class="span4">                    
	                    <button class="btn btn-block" onClick="loginBlock('#login');">Voltar</button>
	                </div>                                
	                <div class="span4"></div>
	                <div class="span4 TAR">
	                    <button class="btn btn-block btn-primary" onclick="enviaSenha()">Recuperar</button>
	                </div>
	            </div>
        </div>
    </div>    
    <!-- ui-dialog para mensagens -->
<div id="dialog">
	<p></p>
</div>
</body>
</html>
