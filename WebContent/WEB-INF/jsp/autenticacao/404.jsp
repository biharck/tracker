<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<h3 class="tile-error"><i>Ups! Algo Inesperado aconteceu...</i></h3>
<br>
<div class="ultimaalteracao">
        <h6 class="last-login">
                Ol�:

                <font style="color:#E77272">
                        <c:out value="${nomeExibicao}"/>
                </font>

               	A P�gina que voc� est� tentando encontrar, n�o existe! :(
        </h6>
<br />
</div>
<br />
<h4>Mas tudo bem, clique <a href="javascript:history.back()"> aqui para Retornar para o sistema. :)</a></h4>
<style type="text/css">
.tile-error{
        font-family: 'ProximaNova-LightIt',arial,sans-serif;
        font-weight: normal;
        color: #5D5D5D;
        font-size: 30px;
        letter-spacing: 1px;
        line-height: 30px;
}
</style>


