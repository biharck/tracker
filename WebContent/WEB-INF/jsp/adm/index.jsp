<%@page import="br.com.easytracking.tracker.util.TrackerUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<br>
<div class="ultimaalteracao">
	<h6 class="last-login">
		
		<font style="color:#E77272">
			<c:out value="${nomeExibicao}"/>
		</font> 
		
		seu �ltimo login foi dia 
		
		<font style="color:#E77272">
			<c:out value="${dtUltimoLogin}"/>
		</font>
	</h6>
</div>
<style type="text/css">
	.info p{
		font-size: 12px;
	}
</style>

<div class="content">
    <c:if test="<%= !br.com.easytracking.tracker.util.TrackerUtil.isPessoaLogadaInstalador()%>">
        <div class="row-fluid">
            
            <div class="span8">
                
                <div class="middle">

                    <div class="button">
                        <a href="#">
                            <span class="icomg-earth"></span>
                            <span class="text">Rastreamento</span>
                        </a>
                        <ul class="sub">
                            
                            <li><a href="/Tracker/adm/process/Historico" class="tip" title="Hist�rico"><span class="icosg-history"></span></a></li>
                            <li><a href="/Tracker/adm/process/grupoveiculos" class="tip" title="Grupos"><span class="icon-tag"></span></a></li>
							<li><a href="/Tracker/adm/process/TempoReal" class="tip" title="Tempo Real"><span class="icon-map-marker"></span></a></li>                            
							<li><a href="widgets.html" class="tip" title="Alertas"><span class="icon-warning-sign"></span></a></li>
                        </ul>
                    </div>

                    <div class="button">
                        <a href="#">
                            <span class="icomg-user2"></span>
                            <span class="text">Meus Dados</span>
                        </a>
                        <ul class="sub">
                            <li><a href="/Tracker/adm/crud/Usuario" class="tip" title="Meus Dados"><span class="icosg-user"></span></a></li>
                            <li><a href="/Tracker/adm/process/AlteraSenha" class="tip" title="Alterar Senha"><span class="icosg-key"></span></a></li>                        
                            <li><a href="/Tracker/adm/crud/Usuario" class="tip" title="Meus Usu�rios"><span class="icosg-users"></span></a></li>
                        </ul>
                    </div>    
                    
                    <c:if test="<%= br.com.easytracking.tracker.util.TrackerUtil.isPessoaLogadaAdministrador()%>">
	                    <div class="button">
	                        <a href="#">
	                            <span class="icomg-user2"></span>
	                            <span class="text">Administrador</span>
	                        </a>
	                        <ul class="sub">
	                            <li><a href="/Tracker/adm/crud/Coordenadas" class="tip" title="Coordenadas"><span class="icon-map-marker"></span></a></li>
	                        </ul>
	                    </div>
                    </c:if>

                    <c:if test="<%= br.com.easytracking.tracker.util.TrackerUtil.isPessoaLogadaGerente()%>">
	                    <div class="button">
	                        <a href="#">
	                            <span class="icomg-user2"></span>
	                            <span class="text">Gerente</span>
	                        </a>
	                        <ul class="sub">
	                            <li><a href="/Tracker/adm/crud/Coordenadas" class="tip" title="Coordenadas"><span class="icon-map-marker"></span></a></li>
	                        </ul>
	                    </div>
                    </c:if>

                </div>      
                <div class"middle">
	                <div class="body">
						
					</div>
				</div>
                          
                
            </div>
            
        </div>
     </c:if>   
    </div>
    <style type="text/css">
    .row-fluid .span8 {
		width: 98%;
	}
    </style>