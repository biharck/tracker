<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page import="br.com.easytracking.tracker.util.PathUtil"%>
<%@page import="br.com.easytracking.tracker.util.TrackerUtil"%>
<HTML>
	<HEAD>
		<n:head/>
	</HEAD>
	<BODY leftmargin="0" topmargin="0">
		<div class="applicationTitle">
			<a href="${application}">Logo</a>
		</div>
		
		<c:if test="${empty param.showMenu && (empty isSenhaProvisoria || !isSenhaProvisoria)}">
			<c:if test="${empty showMenu}">
				<div class="menubar">
					<center>
                         <n:menu menupath="/WEB-INF/menu/menu.xml"/>
                    </center>
				</div>
			</c:if>
			<div align="right" style="margin-top:5px;color:#555;">
				<span > Voc� est� logado como: <font style="color: #2281CF;"><%=TrackerUtil.getUsuarioLogado().getNome()%></font>&nbsp;&nbsp;</span>
			</div>
		</c:if>
		<c:if test="${!empty param.showMenu}">
			<style type="text/css">
				/*empurra a div principal para a esquerda logo abaixo do nome da tela*/
				.pageBody {
					margin-left: -61px;
				}			
			</style>
		</c:if>			
		<div class="messageOuterDiv">
		
			<div class="ajax-combo">
				Carregando...
			</div>
		
			<!-- ui-dialog para mensagens -->
			<div id="dialog">
				<p></p>
			</div>
			<div id="dialog-confirm" style="display:none;" title="Ol� <%=TrackerUtil.getLoginUsuarioLogado()%>">
				<p>Voc� est� tentando voltar sem salvar um registro, deixe-me ajudar.</p><br>
				
				
				<div class="p_dialog p_dialog_success" onclick="$('#dialog-confirm').dialog('close');$('.salvar-registro').trigger('click');">
					<p>Salvar</p>
					<p class="p_dialog_sub p_dialog_sub_success">Salva o registro que voc� estava criando ou editando neste momento.</p>
				</div><br>
				<div class="p_dialog p_dialog_warning" onclick="location.href = window.location.href.substr(0,window.location.href.indexOf('?'))">
					<p>Continuar</p>
					<p class="p_dialog_sub p_dialog_sub_warning">Cancela o que estava fazendo e retorna para a listagem dos dados sem salvar.</p>
				</div><br>	
				<div class="p_dialog p_dialog_error" onclick="$('#dialog-confirm').dialog('close');">
					<p>Cancelar</p>
					<p class="p_dialog_sub p_dialog_sub_error">Retorna para a tela de cria��o/edi��o de dados antes de voc� ter clicado em voltar.</p>
				</div><br>
			</div>
			<n:messages/>&nbsp;
		</div>
		<div class="body">
			<jsp:include page="${bodyPage}" />
		</div>
		<!-- div-auxiliar para armazenar valores tempor�rios -->
			<div id="div-auxiliar" style="display:none;">
			</div>
		<script type="text/javascript">
			/*
				Para iniciar antes de carregar a p�gina
			*/
			$(document).ready(function(){
			   $('.tootip').qtip({
			      content: {
			         text: false // Use each elements title attribute
			      },
               	  position: {
	                  corner: {
	                     tooltip: 'rightBottom', // Use the corner...
	                     target: 'leftTop' // ...and opposite corner
	                  }
               	  },
			      style: {
	                  border: {
	                     width: 5,
	                     radius: 10
	                  },
	                  padding: 10, 
	                  textAlign: 'center',
	                  tip: true, // Give it a speech bubble tip with automatic corner detection
	                  name: 'cream' // Style it according to the preset 'cream' style
	               },
	              adjust: {
		               resize: true,
		               scroll: true
		            }
			   });
			});
			$(document).ready(function(){
			   $('.tootip-center').qtip({
			      content: {
			         text: false // Use each elements title attribute
			      },
               	  position: {
	                  corner: {
	                     tooltip: 'bottomLeft', // Use the corner...
	                     target: 'topRight' // ...and opposite corner
	                  }
               	  },
			      style: {
	                  border: {
	                     width: 5,
	                     radius: 10
	                  },
	                  padding: 10, 
	                  textAlign: 'center',
	                  tip: true, // Give it a speech bubble tip with automatic corner detection
	                  name: 'cream' // Style it according to the preset 'cream' style
	               },
	              adjust: {
		               resize: true,
		               scroll: true
		            }
			   });
			});
			$(document).ready(function(){
				   $('.tootip-alert').qtip({
				      content: {
				         text: false // Use each elements title attribute
				      },
	               	  position: {
		                  corner: {
		                     tooltip: 'rightBottom', // Use the corner...
		                     target: 'leftTop' // ...and opposite corner
		                  }
	               	  },
				      style: {
		                  border: {
		                     width: 5,
		                     radius: 10
		                  },
		                  padding: 10, 
		                  textAlign: 'center',
		                  tip: true, // Give it a speech bubble tip with automatic corner detection
		                  name: 'red' // Style it according to the preset 'cream' style
		               },
		              adjust: {
			               resize: true,
			               scroll: true
			            }
				   });
				});
			$(document).ready(function(){
				   $('.tootip-warnig').qtip({
				      content: {
				         text: false // Use each elements title attribute
				      },
	               	  position: {
		                  corner: {
		                     tooltip: 'rightBottom', // Use the corner...
		                     target: 'leftTop' // ...and opposite corner
		                  }
	               	  },
				      style: {
		                  border: {
		                     width: 5,
		                     radius: 10
		                  },
		                  padding: 10, 
		                  textAlign: 'center',
		                  tip: true, // Give it a speech bubble tip with automatic corner detection
		                  name: 'green' // Style it according to the preset 'cream' style
		               },
		              adjust: {
			               resize: true,
			               scroll: true
			            }
				   });
				});
            /*
              Verifica se usuario deve alterar senha, senhaProvis�ria = true
             */
            $(document).ready(function(){
               isSenhaProvisoria = ${isSenhaProvisoria};
               if(isSenhaProvisoria){
                     url = window.location.href;
                     path = url.split('SCR')[1];
                     if(path != '<%=PathUtil.AFTER_ALTERAR_SENHA_GO_TO%>'){
                         dialog('Aten��o','Voc� deve deve alterar sua senha antes de utilizar o sistema.');
                         location.href = '/Tracker'+'<%=PathUtil.AFTER_ALTERAR_SENHA_GO_TO%>';
                     }
               }       
		    });
            
		</script>
	</BODY>
</HTML>