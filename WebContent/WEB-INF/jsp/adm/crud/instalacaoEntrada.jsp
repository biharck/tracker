<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<c:if test="${empty inst}">
						<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
						<t:property name="instalador" renderAs="doubleline" itens="${instaladores}"/>
						<n:comboReloadGroup>
							<t:property name="rastreador.modeloRastreador.fabricante"  renderAs="doubleline" id="nome"/>
							<t:property name="rastreador.modeloRastreador"  renderAs="doubleline" id="nome"/>
							<t:property name="rastreador" renderAs="doubleline"/>
						</n:comboReloadGroup>
						<t:property name="dataEntregueInstalacao" renderAs="doubleline"/>
					</c:if>
					<c:if test="${!empty inst}">
						<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
						<t:property name="instalador" colspan="2" renderAs="doubleline" mode="output"/>
						<t:property name="rastreador"  renderAs="doubleline" disabled="disabled" id="serial"/>
						<t:property name="dataEntregueInstalacao" renderAs="doubleline" mode="output"/>
						<t:property name="veiculo.placa" id="placa"/>
						<t:property name="veiculo.chassi"/>
						<t:property name="veiculo.ano"/>
						<t:property name="veiculo.cor"/>
						<t:property name="observacoes" rows="3" cols="10"/>
						<t:property name="teste_corte" id="teste_corte" type="hidden" showLabel="false" label=" "/>
						<t:property name="teste_liberar" id="teste_liberar" type="hidden" showLabel="false" label=" "/>
						
							<div class="block-fluid">
				                <div class="row-form">
				                    <div class="span8">
				                        <button class="btn btn-danger" onclick="javascript:$('#teste_corte').val(1);ajaxComandoBySerial('cortarCombustivelBySerial',$('select option:selected').html())" type="button">Cortar Combustível</button>
				                        Clique aqui para efetuar o teste de corte do combustível. 
				                    </div>
				                </div>
				                <div class="row-form">
				                    <div class="span8">
				                        <button class="btn btn-success" onclick="javascript:$('#teste_liberar').val(1);ajaxComandoBySerial('liberarCombustivelBySerial',$('select option:selected').html())" type="button">Liberar Combustível</button>
				                        Clique aqui reativar o combustível.
				                    </div>
				                </div>
				            </div>
					</c:if>
					
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#placa').focus();
	 });
	 
	 $("#placa").mask("aaa-9999");	 
	 function ajaxComandoBySerial(comando,serial){
			$.getJSON('/Tracker/adm/process/Comandos?ACAO='+comando,{'id':serial},
			  	function(data){
					$('#close_btn').click();
					if(data.erro){	
			  			dialog('Falha de Processamento',data.msg);
			  		}else{
			  			dialog('Atenção',data.msg)
					}
				});
		}


</script> 
