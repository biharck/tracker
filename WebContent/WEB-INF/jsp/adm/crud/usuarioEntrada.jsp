<%@page import="br.com.easytracking.tracker.util.TrackerUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada >
	<t:janelaEntrada submitConfirmationScript="validaForm">
		<t:tabelaEntrada >
			<n:panel>
				<n:panelGrid columns="4" >
					<c:if test="${param.ACAO != 'consultar'}">
						<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					</c:if>
					<t:property name="nome" id="nome" colspan="2" style="width:400px" renderAs="doubleline"/>
					<t:property name="login" colspan="2" style="width:200px" renderAs="doubleline"/>
					
					<t:property name="email" colspan="2" style="width:400px" renderAs="doubleline" id="email"/>
					<c:if test="${empty usuario.id}">
						<t:property name="reEmail" colspan="2" style="width:400px" renderAs="doubleline" id="reemail" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
					</c:if>
					
					
					<c:if test="${empty usuario.id}">
						<t:property name="senha"   renderAs="doubleline" id="senha" onchange="validaTamMinimoCampo($('#senha'),'senha',6)"/>
						<t:property name="reSenha" renderAs="doubleline" id="resenha" onchange="validaIgualdadeCampo($('#senha'),this,'senha')"/>
						<n:panel colspan="2"></n:panel>
					</c:if>
					<c:if test="${!empty usuario.id}">
						<t:property name="ativo" renderAs="doubleline"/>
					</c:if>
					
					<t:property name="senhaProvisoria" renderAs="doubleline" class="tootip" title="No pr�ximo acesso dever� ocorrer altera��o de senha"/>
					
					<n:group legend="Papeis">
					<n:dataGrid itemType="br.com.easytracking.tracker.bean.Papel" itens="${listaPapel}" bodyStyleClasses="," styleClass=",">
						<n:column width="20">
		                    <c:if test="${param.ACAO != 'consultar'}">
		                    	<n:input name="papeis"  value="${row}" type="checklist" itens="${usuario.papeis}"/>
		                    </c:if>
		                    <c:if test="${param.ACAO == 'consultar'}">
		                    	<n:input name="papeis"  value="${row}" type="checklist" itens="${usuario.papeis}" disabled="disabled"/>
		                    </c:if>
		                </n:column>
		                <n:column>
			                <t:property name="nome" mode="output"/>
		                </n:column>
					</n:dataGrid>
					</n:group>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>


<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });

	 function validaForm(){
	        edicao = ${empty usuario.id};
		 	if(edicao){
		 		return(validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail') &&
	                   validaIgualdadeCampo($('#senha'),$('#resenha'),'senha') &&
	                   validaTamMinimoCampo($('#senha'),'senha',6) &&
	                   validaSenhaForte($('#senha'))
	                  );
		 	}else return true;
		 }
  
 
  
</script>