<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="operadora" renderAs="doubleline"/>
					<t:property name="numero" style="width:400px" renderAs="doubleline" id="nome"/>
					<t:property name="pin" renderAs="doubleline"/>
					<t:property name="pin2" renderAs="doubleline"/>
					<t:property name="puk" renderAs="doubleline"/>
					<t:property name="puk2" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#operadora').focus();
	 });
</script> 