<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="tipoCliente"/>
					<t:property name="razaoSocial"/>
					<t:property name="cpfCnpj"/>
					<n:comboReloadGroup>
						<t:property name="uf"/>
						<t:property name="municipio"/>
					</n:comboReloadGroup>
					<t:property name="telefone1"/>
					<t:property name="email1"/>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados >
			<t:property name="tipoCliente"/>
			<t:property name="razaoSocial"/>
			<t:property name="nomeFantasia"/>
			<t:property name="cpfCnpj"/>
			<t:property name="bairro"/>
			<t:property name="municipio"/>
			<t:property name="telefone1"/>
			<t:property name="celular1"/>
			<t:property name="email1"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
