<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaResultados>
		<t:tabelaResultados showConsultarLink="false" showEditarLink="false" showExcluirLink="false" >
			<t:property name="instalador" />
			<t:property name="rastreador"/>
			<t:property name="observacoes" />
			<t:property name="testeCorte" trueFalseNullLabels="Realizado, N�o Realizado,N�o Realizado"/>
			<t:property name="testeLiberacao" trueFalseNullLabels="Realizado, N�o Realizado,N�o Realizado"/>
			<t:property name="situacaoRastreador" />
			<t:property name="dataEntregueInstalacao" />
			<t:property name="dataInstalacao" />
			<t:acao>
				<c:if test="${empty instalacao.dataInstalacao}">
					<a href="/Tracker/adm/crud/Instalacao?ACAO=editar&amp;id=${instalacao.id}" onclick="dialogAguarde();" class="tipl" data-original-title="Instalar um Rastreador"><span class="icosg-wrench"></span></a>
				</c:if>
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
