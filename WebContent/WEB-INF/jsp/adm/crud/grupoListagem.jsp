<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaResultados>
		<t:tabelaResultados itens="${listaGrupos }"  >
			<t:property name="id" />
			<t:property name="nome"/>
			<t:property name="veiculos"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
