<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="veiculo" renderAs="doubleline" selectOnePath="/adm/crud/Veiculo?hidden_menu=true"/>
					<t:property name="emailVelocidade" renderAs="doubleline"/>
					<t:property name="emailCorte" renderAs="doubleline"/>
					<t:property name="emailAlertaMovimento" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
