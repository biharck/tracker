<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem showNewLink="false">
	<t:janelaFiltro >
		<t:tabelaFiltro >
			<n:panel>
				<n:panelGrid columns="4">
					<t:property name="serial"/>
					<t:property name="date"/>
					<t:property name="moving"/>
					<t:property name="input1"/>
					<t:property name="panic"/>
					<t:property name="output1"/>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false" showEditarLink="false" showConsultarLink="false">
			<t:property name="serial"/>
			<t:property name="transmissionReason"/>
			<t:property name="date"/>
			<t:property name="latitude"/>
			<t:property name="longitude"/>
			<t:property name="course"/>
			<t:property name="speed"/>
			<t:property name="gprsConnection"/>
			<t:property name="gpsSignal"/>
			<t:property name="gpsAntennaFailure"/>
			<t:property name="gpsAntennaDisconnected"/>
			<t:property name="excessSpeed"/>
			<t:property name="gsmJamming"/>
			<t:property name="input1"/>
			<t:property name="panic"/>
			<t:property name="output1"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
