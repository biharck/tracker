<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="imei"/>
					<t:property name="idRastreador"/>
					<t:property name="chip"/>
					<t:property name="situacaoRastreador"/>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados >
			<t:property name="imei"/>
			<t:property name="idRastreador"/>
			<t:property name="chip"/>
			<t:property name="modeloRastreador"/>
			<t:property name="situacaoRastreador"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
