<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="tipoCliente" renderAs="doubleline"/>
					<t:property name="razaoSocial" renderAs="doubleline"/>
					<t:property name="nomeFantasia" renderAs="doubleline"/>
					<t:property name="cpfCnpj" class="tootip" title="Somente n�meros" maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline"/>
					<t:property name="inscricaoMunicipal" renderAs="doubleline"/>
					<t:property name="inscricaoEstadual" renderAs="doubleline"/>
					<t:property name="cep" id="cep" maxlength="9" onkeypress="formatar_mascara(this,  \"#####-###\");" renderAs="doubleline" class="tootip" title="Somente n�meros"/>
					<t:property name="endereco" renderAs="doubleline"/>
					<t:property name="numero" renderAs="doubleline"/>
					<t:property name="complemento" renderAs="doubleline"/>
					<t:property name="bairro" renderAs="doubleline"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="uf" renderAs="doubleline"/>
						<t:property name="municipio" renderAs="doubleline"/>
					</n:comboReloadGroup>
					<t:property name="telefone1" renderAs="doubleline"/>
					<t:property name="telefone2" renderAs="doubleline"/>
					<t:property name="celular1" renderAs="doubleline"/>
					<t:property name="celular2" renderAs="doubleline"/>
					<t:property name="email1" renderAs="doubleline"/>
					<t:property name="email2" renderAs="doubleline"/>
					<t:property name="website" renderAs="doubleline"/>
					<t:property name="facebook" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#tipoCliente').focus();
	 });
</script> 