<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="rastreador.modeloRastreador.fabricante" renderAs="doubleline" label="Fabricante"/>
						<t:property name="rastreador.modeloRastreador" renderAs="doubleline" label="Modelo do Rastreador"/>
						<t:property name="rastreador" renderAs="doubleline"/>
					</n:comboReloadGroup>
					<t:property name="cliente" renderAs="doubleline"/>
					<t:property name="placa" id="placa" renderAs="doubleline"/>
					<t:property name="ano" renderAs="doubleline"/>
					<t:property name="cor" renderAs="doubleline"/>
					<t:property name="chassi" renderAs="doubleline"/>
					<t:property name="observacoes" renderAs="doubleline" rows="3" cols="10"/>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#fabricante').focus();
	 });
	 
	 $("#placa").mask("aaa-9999");	 
</script> 