<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="nome"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false">
			<t:property name="nome"/>
			<t:property name="descricao"/>
			<t:acao>
				<c:if test="${(papel.id != 1) && (papel.id != 2)&& (papel.id != 3)&& (papel.id != 4)&& (papel.id != 5)&& (papel.id != 6)}">
					<n:link confirmationMessage="Deseja realmente excluir esse registro?" action="excluir" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}">excluir</n:link>
				</c:if>
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>