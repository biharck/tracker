<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4">
					<t:property name="placa" id="placa"/>
					<n:comboReloadGroup>				
						<t:property name="fabricante"/>				
						<t:property name="modeloRastreador"/>				
						<t:property name="rastreador"/>
					</n:comboReloadGroup>				
					<t:property name="cliente"/>				
					<t:property name="chassi"/>				
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados >
			<t:property name="rastreador.modeloRastreador.fabricante"/>
			<t:property name="rastreador.modeloRastreador"/>
			<t:property name="rastreador"/>
			<t:property name="cliente"/>
			<t:property name="placa"/>
			<n:column header="Ano">
				${veiculo.ano}
			</n:column>
			<t:property name="chassi"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
	 $("#placa").mask("aaa-9999");	 
</script> 
