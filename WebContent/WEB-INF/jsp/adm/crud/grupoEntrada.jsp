<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="nome" style="width:400px" renderAs="doubleline" id="nome"/>
				</n:panelGrid>
				<t:detalhe name="grupoVeiculos" labelnovalinha="Adicionar Ve�culo" class="tabela-resultados, table-hover">
					<t:property name="veiculo" selectOnePath="/adm/crud/Veiculo?hidden_menu=true"/>
				</t:detalhe>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script> 