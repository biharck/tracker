<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem >
	<t:janelaFiltro >
		<t:tabelaFiltro >
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="evento"/>
					<t:property name="usuario"/>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados  >
		<t:tabelaResultados showConsultarLink="false" >
			<t:property name="userInc" />
			<t:property name="evento"/>
			<t:property name="timeInc" />
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
