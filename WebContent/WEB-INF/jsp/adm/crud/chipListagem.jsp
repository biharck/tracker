<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="operadora"/>
					<t:property name="numero"/>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados >
			<t:property name="operadora" />
			<t:property name="numero"/>
			<t:property name="pin" />
			<t:property name="puk" />
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
