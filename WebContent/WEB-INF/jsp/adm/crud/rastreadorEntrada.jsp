<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="idRastreador" style="width:400px" colspan="2" renderAs="doubleline" id="nome"/>
					<t:property name="imei" style="width:400px" colspan="2" renderAs="doubleline" id="nome"/>
					<t:property name="chip" renderAs="doubleline"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="fabricante" renderAs="doubleline"/>
						<t:property name="modeloRastreador" renderAs="doubleline"/>
					</n:comboReloadGroup>
					<t:property name="situacaoRastreador" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#imei').focus();
	 });
</script> 