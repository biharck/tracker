<%@page import="br.com.easytracking.tracker.util.PathUtil"%>
<%@page import="br.com.easytracking.tracker.util.TrackerUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
	
	
	<!DOCTYPE html>
	<html lang="en">
	<head>  
		<n:head/>      
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
			    
	    <!--[if gt IE 8]>
	        <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
	    <![endif]-->        
	    
	    <title>EasyTracking - Tracker 2.0</title>
	    
	    <link href="css/stylesheets.css" rel="stylesheet" type="text/css" />
	    <!--[if lt IE 10]>
	        <link href="css/ie.css" rel="stylesheet" type="text/css" />
	    <![endif]-->       
	    <link rel="icon" type="image/ico" href="/Tracker/img/favicon.ico"/>
	    
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/jquery/jquery-1.8.3.min.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/jquery/jquery-ui-1.9.1.custom.min.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/jquery/globalize.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/other/excanvas.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/other/jquery.mousewheel.min.js'></script>
	        
	    <script type='text/javascript' src='/Tracker/js/plugins/bootstrap/bootstrap.min.js'></script>            
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/fancybox/jquery.fancybox.pack.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/jflot/jquery.flot.js'></script>    
	    <script type='text/javascript' src='/Tracker/js/plugins/jflot/jquery.flot.stack.js'></script>    
	    <script type='text/javascript' src='/Tracker/js/plugins/jflot/jquery.flot.pie.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/jflot/jquery.flot.resize.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/epiechart/jquery.easy-pie-chart.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/sparklines/jquery.sparkline.min.js'></script>    
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/pnotify/jquery.pnotify.min.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/fullcalendar/fullcalendar.min.js'></script>        
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/datatables/jquery.dataTables.min.js'></script>    
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/wookmark/jquery.wookmark.js'></script>        
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/jbreadcrumb/jquery.jBreadCrumb.1.1.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
	    
	    
	    <script type='text/javascript' src="/Tracker/js/plugins/uniform/jquery.uniform.min.js"></script>
	    <script type='text/javascript' src="/Tracker/js/plugins/select/select2.min.js"></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/tagsinput/jquery.tagsinput.min.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/multiselect/jquery.multi-select.min.js'></script>    
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/validationEngine/languages/jquery.validationEngine-en.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/validationEngine/jquery.validationEngine.js'></script>        
	    <script type='text/javascript' src='/Tracker/js/plugins/stepywizard/jquery.stepy.js'></script>
	        
	    <script type='text/javascript' src='/Tracker/js/plugins/animatedprogressbar/animated_progressbar.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/hoverintent/jquery.hoverIntent.minified.js'></script>
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/media/mediaelement-and-player.min.js'></script>    
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/cleditor/jquery.cleditor.js'></script>
	
	    <script type='text/javascript' src='/Tracker/js/plugins/shbrush/shCore.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/shbrush/shBrushXml.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/shbrush/shBrushJScript.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/shbrush/shBrushCss.js'></script>    
	        
	    <!-- <script type='text/javascript' src='/Tracker/js/plugins/filetree/jqueryFileTree.js'></script> -->
	    
	    <script type='text/javascript' src='/Tracker/js/plugins/slidernav/slidernav-min.js'></script>    
	    <script type='text/javascript' src='/Tracker/js/plugins/isotope/jquery.isotope.min.js'></script>    
	    <script type='text/javascript' src='/Tracker/js/plugins/jnotes/jquery-notes_1.0.8_min.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/jcrop/jquery.Jcrop.min.js'></script>
	    <script type='text/javascript' src='/Tracker/js/plugins/ibutton/jquery.ibutton.min.js'></script>
	        
	    <script type='text/javascript' src='/Tracker/js/plugins.js'></script>
	    <script type='text/javascript' src='/Tracker/js/charts.js'></script>
	    <script type='text/javascript' src='/Tracker/js/actions.js'></script>
	    
	    
	</head>
	<body>
		<c:if test="${empty hidden_menu }">
		    <div class="header">
	        	&nbsp;&nbsp;&nbsp;&nbsp;<span style="padding: 0px 0px 0px 19px;"><font color="#fff">Sua sess�o expira em: <span id="cronometro_tempo"></span></font></span>
		        <a href="/Tracker/adm" class="logo"></a>
		
		        <div class="buttons">
		            <div class="popup" id="subNavControll">
		                <div class="label"><span class="icos-list"></span></div>
		            </div>
		            <div class="dropdown">
		                <div class="label"><span class="icos-user2"></span></div>
		                <div class="body" style="width: 160px;">
		                    <div class="itemLink">
		                        <a href="/Tracker/adm/process/AlteraSenha"><span class="icon-cog icon-white"></span> Alterar Senha</a>
		                    </div>
		                    <div class="itemLink">
		                        <a href="/Tracker/autenticacao/Logout"><span class="icon-off icon-white"></span> Sair</a>
		                    </div>                                        
		                </div>                
		            </div>            
		            
		            <div class="popup">
		                <div class="label"><span class="icos-cog"></span></div>
		                <div class="body">
		                    <div class="arrow"></div>
		                    <div class="row-fluid">
		                        <div class="row-form">
		                            <div class="span12">
		                                <span class="top">Temas:</span>
		                                <div class="themes">
		                                    <a href="#" data-theme="" class="tip" title="Default"><img src="/Tracker/img/themes/default.jpg"/></a>                                    
		                                    <a href="#" data-theme="ssDaB" class="tip" title="DaB"><img src="/Tracker/img/themes/dab.jpg"/></a>
		                                    <a href="#" data-theme="ssTq" class="tip" title="Tq"><img src="/Tracker/img/themes/tq.jpg"/></a>
		                                    <a href="#" data-theme="ssGy" class="tip" title="Gy"><img src="/Tracker/img/themes/gy.jpg"/></a>
		                                    <a href="#" data-theme="ssLight" class="tip" title="Light"><img src="/Tracker/img/themes/light.jpg"/></a>
		                                    <a href="#" data-theme="ssDark" class="tip" title="Dark"><img src="/Tracker/img/themes/dark.jpg"/></a>
		                                    <a href="#" data-theme="ssGreen" class="tip" title="Green"><img src="/Tracker/img/themes/green.jpg"/></a>
		                                    <a href="#" data-theme="ssRed" class="tip" title="Red"><img src="/Tracker/img/themes/red.jpg"/></a>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="row-form">
		                            <div class="span12">
		                                <span class="top">Cor de Fundo:</span>
		                                <div class="backgrounds">
		                                    <a href="#" data-background="bg_default" class="bg_default"></a>
		                                    <a href="#" data-background="bg_mgrid" class="bg_mgrid"></a>
		                                    <a href="#" data-background="bg_crosshatch" class="bg_crosshatch"></a>
		                                    <a href="#" data-background="bg_hatch" class="bg_hatch"></a>                                    
		                                    <a href="#" data-background="bg_light_gray" class="bg_light_gray"></a>
		                                    <a href="#" data-background="bg_dark_gray" class="bg_dark_gray"></a>
		                                    <a href="#" data-background="bg_texture" class="bg_texture"></a>
		                                    <a href="#" data-background="bg_light_orange" class="bg_light_orange"></a>
		                                    <a href="#" data-background="bg_yellow_hatch" class="bg_yellow_hatch"></a>                        
		                                    <a href="#" data-background="bg_green_hatch" class="bg_green_hatch"></a>                        
		                                </div>
		                            </div>          
		                        </div>
		                        
		                    </div>
		                </div>
		            </div>
		            
		        </div>
		        
		    </div>
	    
	    
		<div class="ajax-combo">
			Carregando...
		</div>	
		
		<div class="navigation">
			<div class="control"></div>
	     
		     <ul class="main">
		         <li><a href="/Tracker/adm" class="active"><span class="icom-screen"></span><span class="text">Home</span></a></li>
		         <li><a href="#ui"><span class="icom-bookmark"></span><span class="text">Menu</span></a></li>            
		         <c:if test="${isTempoReal}">
		         	<li><a href="#cars"><span class="icom-cars"></span><span class="text">Ve�culos</span></a></li>
		         </c:if>
		     </ul>
		     
		     <div class="submain">
		        
		         <div id="ui">                
		             <ul class="fmenu">
		                    <c:if test="${!isSenhaProvisoria}">
		                    	<n:menu menupath="/WEB-INF/menu/menu.xml"/>
		                    </c:if>
		                </ul>                           
		             <div class="dr"><span></span></div>                
		         </div>
		        <c:if test="${isTempoReal}">             
					<div id="cars">
						<div class="widget">
						    <div class="head">
						        <div class="icon"><i class="icosg-menu"></i></div>
						        <h2>Meus Ve�culos</h2>
						    </div>
						    <div class="block typography">
						    	<div style="overflow: auto;height: 500px;" >                        
								    <ul class="list tickets">
								    	<c:forEach var="v" items="${veiculos}">
									        <li class="accept clearfix" id="T213">
									            <div class="title">
									            	${v.placa}
									            	<c:if test="${!v.instalador}">
										            	<c:if test="${empty isHistorico }">
											            	<c:if test="${v.tk}">
											                	<a href="#fModal" onclick="javascript:$('#placa_span').html('${v.placa }');$('#placa_veiculo').val('${v.placa}');" role="button" data-toggle="modal">Comandos</a>
											                </c:if>
											            	<c:if test="${!v.tk}">
											                	<a href="#fModal_tk" onclick="javascript:$('#placa_span_tk').html('${v.placa }');$('#placa_veiculo').val('${v.placa}');" role="button" data-toggle="modal">Comandos</a>
											                </c:if>
										                </c:if>
										                <c:if test="${isHistorico }">
										                	<a href="#fModal_hist" onclick="javascript:$('#placa_span_tk').html('${v.placa }');$('#placa_veiculo').val('${v.placa}');" role="button" data-toggle="modal">Per�odo</a>
										                </c:if>
										            </c:if>
									            </div>
									            <c:if test="${empty isHistorico }">
									            	<c:if test="${!v.instalador}">
											            <div class="actions">
											                <a href="javascript:limparMapa(),$('.control').click();ajaxBuscaCoordenadasAutomatico('${v.placa}')" class="tipb" title="Ver no Mapa"><span class="icosg-location"></span></a> 
											            </div>
											        </c:if>
									            	<c:if test="${v.instalador}">
											            <div class="actions">
											                <a href="javascript:limparMapa(),$('.control').click();ajaxBuscaCoordenadasAutomatico(null,'${v.placa}')" class="tipb" title="Ver no Mapa"><span class="icosg-location"></span></a> 
											            </div>
											        </c:if>
										        </c:if>
									        </li>
					                 	</c:forEach>
								    </ul>
								  </div>    
						    </div>
						</div>
			     	</div>
			     </c:if>
		     </div>
	 	</div>    
	    </c:if>
		<jsp:include page="${bodyPage}" />
		
			<div class="messageOuterDiv">
				
			
				<!-- ui-dialog para mensagens -->
				<div id="dialog">
					<p></p>
				</div>
				<div id="dialog-confirm" style="display:none;" title="Ol� <%=TrackerUtil.getLoginUsuarioLogado()%>">
					<p>Voc� est� tentando voltar sem salvar um registro, deixe-me ajudar.</p><br>
					
					
					<div class="p_dialog p_dialog_success" onclick="$('#dialog-confirm').dialog('close');$('.salvar-registro').trigger('click');">
						<p>Salvar</p>
						<p class="p_dialog_sub p_dialog_sub_success">Salva o registro que voc� estava criando ou editando neste momento.</p>
					</div><br>
					<div class="p_dialog p_dialog_warning" onclick="location.href = window.location.href.substr(0,window.location.href.indexOf('?'))">
						<p>Continuar</p>
						<p class="p_dialog_sub p_dialog_sub_warning">Cancela o que estava fazendo e retorna para a listagem dos dados sem salvar.</p>
					</div><br>	
					<div class="p_dialog p_dialog_error" onclick="$('#dialog-confirm').dialog('close');">
						<p>Cancelar</p>
						<p class="p_dialog_sub p_dialog_sub_error">Retorna para a tela de cria��o/edi��o de dados antes de voc� ter clicado em voltar.</p>
					</div><br>
				</div>
				<div id="dialog-sessao" style="display:none;">
					<p>Sua sess�o ir� expirar em menos de 2 minutos. Deixe-me lhe ajudar:</p><br/>
					<div class="p_dialog p_dialog_success" onclick="renovaSessao()">
						<p style="background-color: #DFD">Renovar Sess�o</p>
						<p class="p_dialog_sub p_dialog_sub_success">Renova sua sess�o por mais 30 minutos.</p>
					</div><br>
					<div class="p_dialog p_dialog_warning" onclick="closeDialogSessao();">
						<p style="background-color: #FFC;">Continuar</p>
						<p class="p_dialog_sub p_dialog_sub_warning">Cancela o processo de renovar sua sess�o e ela ir� expirar em menos de 2 minutos.</p>
					</div><br>		
				</div>
				
				<div id="dialog-sessao-expirada" style="display:none;">
					<p>Sua sess�o expirou.<br>Deixe-me lhe ajudar:</p><br/>
					<div class="p_dialog p_dialog_success" onclick="location.href='/Tracker/adm'">
						<p style="background-color: #DFD">Logar</p>
						<p class="p_dialog_sub p_dialog_sub_success">Se sua sess�o expirou, voc� precisa logar novamente no ClinicalSys.</p>
					</div><br>
				</div>
				
			</div>
		<script type="text/javascript">
			/*
		    Verifica se usuario deve alterar senha, senhaProvis�ria = true
		   */
		  $(document).ready(function(){
			startCountdown();
		     isSenhaProvisoria = ${isSenhaProvisoria};
		     if(isSenhaProvisoria){
		           url = window.location.href;
		           path = url.split('Tracker')[1];
		           if(path != '<%=PathUtil.AFTER_ALTERAR_SENHA_GO_TO%>'){
		               location.href = '/Tracker'+'<%=PathUtil.AFTER_ALTERAR_SENHA_GO_TO%>';
		           }
		     }       
		  });
	
	</script>	      
	</body>
	</html>
