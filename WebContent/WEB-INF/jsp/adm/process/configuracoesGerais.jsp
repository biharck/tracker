<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<div class="breadCrumb clearfix">    
    <ul id="breadcrumbs">
        <li><a href="/Tracker/adm">Home</a></li>
        <li>Configurações Gerais</li>
    </ul>        
</div>
<t:tela >
    <n:bean name="configuracoesGerais">
        <div class="inputWindow">
        	<t:property name="id" renderAs="doubleline" type="hidden" showLabel="false" write="false" label=" "/>
        	<t:tabelaEntrada  colspan="2">
        		<n:panelGrid columns="6">
        			<n:group legend="Autentcação para envio de Email" colspan="6">
        				<t:property name="usuarioEmail"  renderAs="doubleline" id="usuarioEmail" style="width:300px;"/>
        				<t:property name="senhaEmail"  renderAs="doubleline"  style="width:300px"/>
        				<t:property name="smtp"  renderAs="doubleline"  style="width:300px"/>
        				<t:property name="porta"  renderAs="doubleline"  style="width:300px"/>
        			</n:group>
        		</n:panelGrid>
        	</t:tabelaEntrada>
            <div class="actionBar">
                <n:submit action="salvar" validate="true" class="btn btn-primary" confirmationScript="${janelaEntradaTag.submitConfirmationScript}" onclick="dialogAguarde();">salvar</n:submit>
            </div>
        </div>
        <br><br>
    </n:bean>
</t:tela>

<script type="text/javascript">
     $(document).ready(function() {
        $('#usuarioEmail').focus();
     });
</script>
