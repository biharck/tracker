<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript"> 
	
	$(document).ready(function(){
		$('#filterClose').click();	       
	});
	
	var origem = new google.maps.LatLng(-13.496473,-55.722656);
    var marker;
    var map;
    var geocoder;
    var markersArray = [];
    var flightPlanCoordinates = new Array();
    var polylinesArray = new Array();
    var pin;
    var image = {
   	    url: '/Tracker/img/map/rigth_arrow.png',
   	    // This marker is 20 pixels wide by 32 pixels tall.
   	    size: new google.maps.Size(32, 39),
   	    // The origin for this image is 0,0.
   	    origin: new google.maps.Point(0,0),
   	    // The anchor for this image is the base of the flagpole at 0,32.
   	    anchor: new google.maps.Point(30, 25)
   	  };
    var image_car = {
       	    url: '/Tracker/img/map/fourbyfour.png',
       	    // This marker is 20 pixels wide by 32 pixels tall.
       	    size: new google.maps.Size(32, 39),
       	    // The origin for this image is 0,0.
       	    origin: new google.maps.Point(0,0),
       	    // The anchor for this image is the base of the flagpole at 0,32.
       	    anchor: new google.maps.Point(0, 25)
       	  };
    var image_car_atual = {
       	    url: '/Tracker/img/map/pin-export.png',
       	    // This marker is 20 pixels wide by 32 pixels tall.
       	    size: new google.maps.Size(32, 39),
       	    // The origin for this image is 0,0.
       	    origin: new google.maps.Point(0,0),
       	    // The anchor for this image is the base of the flagpole at 0,32.
       	    anchor: new google.maps.Point(1, 25)
       	  };
    

    function initialize() {
      var mapOptions = {
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: origem
      };
      
      pin = new google.maps.MVCObject();

      map = new google.maps.Map(document.getElementById('map_canvas'),
              mapOptions);
      geocoder = new google.maps.Geocoder();
    }

    function toggleBounce() {

      if (marker.getAnimation() != null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }
    
    function verNoMapa(obj,lat, lon) {
    	var location = new google.maps.LatLng(lat, lon);
    	marker = new google.maps.Marker({
    	    position: location,
    	    map: map,
    	    icon: image
    	  });
    	markersArray.push(marker);
    	map.setZoom(16);
    	map.panTo(location);
    }
    
    function limparMapa() {
		if (markersArray) {
		   for (i in markersArray) {
		     markersArray[i].setMap(null);
		   }
		   markersArray.length = 0;
		 }
		
		if(polylinesArray){
			for (i in polylinesArray) {
				polylinesArray[i].setMap(null);
			}
			polylinesArray.length = 0;
		}
	}
    
    function enderecoAproximado(lat, lon){
		var latlng = new google.maps.LatLng(lat, lon);

		geocoder.geocode({'latLng': latlng}, function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	            if (results[1]) {
	              dialog('Aten��o, Este Endere�o � Aproximado.',results[1].formatted_address);
	            } else {
	            	dialog('Aten��o, Este Endere�o � Aproximado.','O endere�o exato n�o pode ser exibido.');
	            } 
	          } 
	        });
		verNoMapa(null, lat, lon);
	}
</script>
	
<style type="text/css">
	.linkStyle {
		text-decoration: none;
		border: none;
		font-size: 11px;
		color: #9E9E9E;
		font-family:Arial, Helvetica, sans-serif;
	}
	#map_canvas{
		width: 93.5%;
        height: 550px;
        margin-left:80px;
        outline: 1px solid gray;
	}
	#widget{
		margin-left: 6%;
		height: 0%;
		width: 94%;
		margin-bottom: 0px;
    }
	.sGallery .item {
		height: 40px;
	}
	.list li .title a {
		padding: 9px 0px 0px 0px;
	}
	.list li .actions {
		margin-top: 8px;
	}
</style>
	

<body onload="initialize();" >
	
	 <div class="widget" id="widget">                
	        <div class="toolbar">
	            <div class="btn-group" data-toggle="buttons-radio">
	            	<c:forEach items="${lista_grupos}" var="grp">
	                	<button type="button" class="btn btn-primary btn-small" id="filterBy_${grp.id}" grupo_id="${grp.id}">${grp.nome} </button>
	                	<script type="text/javascript">
		                	$("#filterBy_${grp.id}").click(function(){
		                		ajaxBuscaGrupos($(this).attr('grupo_id'))
		                        $('#isotope').isotope({ filter: '.itens_${grp.id}' });
		                    });
						</script>
	            	</c:forEach>
	                <button type="button" class="btn btn-primary btn-small active" id="filterClose">Fechar</button>
	            </div>                    
	        </div>
	        <div class="sGallery clearfix" id="isotope">
	        	<c:forEach items="${veiculos_grupos}" var="vg">
		            <div class="item itens_${vg.id}">
		            	<ul class="list tickets">
                            <li class="done clearfix" id="T212">
                                <div class="title">
                                    <p><a href="#">${vg.placa}</a><br/></p>
                                </div>
                            </li>
                        </ul>
		            </div>
	        	</c:forEach>
	        </div>                                    
	
	    </div>                
    <div id="map_canvas"></div>
</body>

</html>
