<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<style>
    /* Style inspired by http://osm.lonvia.de/world_hiking.html */
    html, body, .slippymap {
        height: 100%;
        width: 100%;
        margin: 0;
        
        padding: 0;
    }
    .slippymap {
        width: 93.5%;
        height: 99.5%;
        margin-left:80px;
        outline: 1px solid gray;
    }
    header, footer{
        position: fixed;
        left: 0;
        right: 0;
        width: 100%;
        margin: 0;
        padding: 0.21em;
        z-index: 2;
        background: #eed;
    }
    h1 {
        font-size: 1.5em;
        font-weight: bold;
        margin: 0;
    }
    #rodape{
    	height: 200px;
    	width: 100%;
    	background: rgb(247, 248, 250);
    	position: absolute;
		bottom: 0;
    }
    #rodape p{
    	color: #ff00ff;
    	font-size: 15px;
    	margin-left: 90px;
    }
</style> 
<div id="map" class="slippymap" style="float: left;"></div>

<div id="rodape">
	<p>�ltimas Coordenadas</p>	
	<p>
		<button onclick="verNoMapa(48.859012,2.29442)">ver</button>
	</p>
</div>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="http://www.openlayers.org/dev/OpenLayers.js"></script>
<script type="text/javascript">
    var element = document.getElementById("map");
    vectors = new OpenLayers.Layer.Vector("Vector Layer");
    var map = new google.maps.Map(element, {
        center: new google.maps.LatLng(-13.496473,-55.722656),
        zoom: 4,
        mapTypeId: "OSM",
        mapTypeControlOptions: {
            mapTypeIds: ["OSM"]
        }
    });

    map.mapTypes.set("OSM", new google.maps.ImageMapType({
        getTileUrl: function(coord, zoom) {
            return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        name: "OpenStreetMap",
        maxZoom: 18
    }));
    
    function verNoMapa(lat, lon) {
    	feature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Collection([new OpenLayers.Geometry.Point(lon, lat)]), {});
    	vectors.addFeatures([feature]);
    	map.addLayer(vectors);
    }

</script>
