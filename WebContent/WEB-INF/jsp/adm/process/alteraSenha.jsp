<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<div class="breadCrumb clearfix">    
    <ul id="breadcrumbs">
        <li><a href="/Tracker/adm">Home</a></li>
        <li>Alterar Senha</li>
    </ul>        
</div>

<div class="pageBody">
	<n:form  action="alteraSenha" method="post" validateFunction="validaForm">
		<t:janelaFiltro>
			<t:tabelaFiltro showSubmit="false">
	         <center>
	         	<c:if test="${isSenhaProvisoria}">
	         		<li class="warn">Este acesso ao sistema foi realizado com uma senha tempor�ria, sendo assim, voc� deve trocar sua senha para uma nova senha mais segura e de uso pessoal.<br /></li>
	         	</c:if>
	         	<br />
		         <n:panel>
	              <n:panelGrid columns="2" >			
	                <t:property name="senhaAtual" renderAs="doubleline" id="senhaAtual" colspan="2"/>
					<t:property name="senhaNova" renderAs="doubleline" id="senhaNova" onchange="validaTamMinimoCampo($('#senhaNova'),'nova senha',6)"/>
					<t:property name="reSenhaNova" renderAs="doubleline" id="reSenhaNova" onchange="validaIgualdadeCampo($('#senhaNova'),this,'nova senha')" />
				  </n:panelGrid>
	             </n:panel> 
	         </center>
            </t:tabelaFiltro>
		</t:janelaFiltro>
		<div style="float: right;">
			<n:submit type="submit" class="btn btn-primary" action="doSalvar">Salvar</n:submit>
		</div>
	</n:form>

</div>
<script type="text/javascript">
  $(document).ready(function() {
   $('#senhaAtual').focus();
  });
 
  
   function validaCampos(){
    dialogAguarde();
    senhaAtual = trim(document.getElementsByName('senhaAtual')[0].value);
    erros='';
    if(senhaAtual==null||senhaAtual==undefined||senhaAtual.length==0)
     erros=erros+'<br/>'+'O campo senha atual � obrigat�rio';
    if(erros.length!=0){
     dialog('Aten��o',erros);
     return false;
    }
    return true;
   } 

  function validaForm(){
    return(validaTamMinimoCampo($('#senhaNova'),'nova senha',6) &&
           validaIgualdadeCampo($('#senhaNova'),$('#reSenhaNova'),'nova senha') &&
           validaCampos() &&
           validaSenhaForte($('#senhaNova'))  
          );
  }
</script>