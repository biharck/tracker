<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript"> 
	
	var origem = new google.maps.LatLng(-13.496473,-55.722656);
    var marker;
    var map;
    var geocoder;
    var markersArray = [];
    var flightPlanCoordinates = new Array();
    var polylinesArray = new Array();
    var pin;
    var image = {
   	    url: '/Tracker/img/map/rigth_arrow.png',
   	    // This marker is 20 pixels wide by 32 pixels tall.
   	    size: new google.maps.Size(32, 39),
   	    // The origin for this image is 0,0.
   	    origin: new google.maps.Point(0,0),
   	    // The anchor for this image is the base of the flagpole at 0,32.
   	    anchor: new google.maps.Point(30, 25)
   	  };
    var image_car = {
       	    url: '/Tracker/img/map/fourbyfour.png',
       	    // This marker is 20 pixels wide by 32 pixels tall.
       	    size: new google.maps.Size(32, 39),
       	    // The origin for this image is 0,0.
       	    origin: new google.maps.Point(0,0),
       	    // The anchor for this image is the base of the flagpole at 0,32.
       	    anchor: new google.maps.Point(0, 25)
       	  };
    var image_car_atual = {
       	    url: '/Tracker/img/map/pin-export.png',
       	    // This marker is 20 pixels wide by 32 pixels tall.
       	    size: new google.maps.Size(32, 39),
       	    // The origin for this image is 0,0.
       	    origin: new google.maps.Point(0,0),
       	    // The anchor for this image is the base of the flagpole at 0,32.
       	    anchor: new google.maps.Point(1, 25)
       	  };
    

    function initialize() {
      var mapOptions = {
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: origem
      };
      
      pin = new google.maps.MVCObject();

      map = new google.maps.Map(document.getElementById('map_canvas'),
              mapOptions);
      geocoder = new google.maps.Geocoder();
    }

    function toggleBounce() {

      if (marker.getAnimation() != null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }
    
    function verNoMapa(obj,lat, lon) {
    	var location = new google.maps.LatLng(lat, lon);
    	marker = new google.maps.Marker({
    	    position: location,
    	    map: map,
    	    icon: image
    	  });
    	markersArray.push(marker);
    	map.setZoom(16);
    	map.panTo(location);
    }
    
    function limparMapa() {
		if (markersArray) {
		   for (i in markersArray) {
		     markersArray[i].setMap(null);
		   }
		   markersArray.length = 0;
		 }
		
		if(polylinesArray){
			for (i in polylinesArray) {
				polylinesArray[i].setMap(null);
			}
			polylinesArray.length = 0;
		}
	}
    
    function enderecoAproximado(lat, lon){
		var latlng = new google.maps.LatLng(lat, lon);

		geocoder.geocode({'latLng': latlng}, function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	            if (results[1]) {
	              dialog('Aten��o, Este Endere�o � Aproximado.',results[1].formatted_address);
	            } else {
	            	dialog('Aten��o, Este Endere�o � Aproximado.','O endere�o exato n�o pode ser exibido.');
	            } 
	          } 
	        });
		verNoMapa(null, lat, lon);
	}
</script>
	
<style type="text/css">
	.linkStyle {
		text-decoration: none;
		border: none;
		font-size: 11px;
		color: #9E9E9E;
		font-family:Arial, Helvetica, sans-serif;
	}
	#map_canvas{
		width: 93.5%;
        height: 500px;
        margin-left:80px;
        outline: 1px solid gray;
	}
	#rodape{
    	height: 200px;
    	width: 93.7%;
    	background: rgb(247, 248, 250);
    	position: absolute;
		bottom: 0;
		margin-left: 80px;
		overflow: auto;
    }
    #rodape p{
    	color: #ff00ff;
    	font-size: 15px;
    	margin-left: 90px;
    }
    .list li .title a {
		padding: 10px 0px 10px 10px;
	}
	.list li .actions {
		margin-top: 8px;
	}
	
	.block-fluid{
		width: 80%;
	}
	
</style>
	

<body onload="initialize();" >
	
    <div id="map_canvas"></div>
	<div id="rodape">
       <table cellpadding="0" cellspacing="0" width="100%" id="table_coord" class="table-hover">
           <thead>
               <tr>
                   <th width="10%">
                      Data
                   </th>
                   <th width="10%">
                      Hora
                   </th>
                   <th width="10%">
                      Latitude
                   </th>
                   <th width="10%">
                      Longitude
                   </th>
                   <th width="10%">
                      Velocidade
                   </th>
                   <th width="15%">
                      Evento
                   </th>
                   <th width="20%">
                      Ver
                   </th>
               </tr>
           </thead>
           <tbody>
               <tr>
               </tr>
           </tbody>
       </table>
   </div>
   <!-- Bootrstrap modal form -->
    <div id="fModal_hist" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Hist�rico do Ve�culo Placa <font color="#f89406"><span id="placa_span_tk"> </span></font></h3>
        </div>  
        <input type="hidden" name="placa_veiculo" id="placa_veiculo">      
        <div class="row-fluid">
            <div class="block-fluid">
                <div class="row-form">
                	<center>Informe o intervalo para exibi��o das coordenadas</center>
                	<br />
                	<center><font style="color:#FF0000">Aten��o! Dependendo do tempo selecionado, pode demorar um pouco! </font></center>
                </div>
                <div class="row-form">
                    <div class="span12">
                        <table>
                        	<tr>
                        		<td>
			                        <span class="left title">In�cio</span>
			                    </td>
			                    <td>
			                        <input type="text" style="width:200px" class="datepicker" name="dt_inicio" id="dt_inicio"/>
			                    </td>
			                    <td>
			                        <span class="left title">Hora de In�cio</span>
			                    </td>
			                    <td>
			                        <select name="hora_inicio" id="hora_inicio" style="width:50px">
			                        	<option value="00">00</option>
			                        	<option value="01">01</option>
			                        	<option value="02">02</option>
			                        	<option value="03">03</option>
			                        	<option value="04">04</option>
			                        	<option value="05">05</option>
			                        	<option value="06">06</option>
			                        	<option value="07">07</option>
			                        	<option value="08">08</option>
			                        	<option value="09">09</option>
			                        	<option value="10">10</option>
			                        	<option value="11">11</option>
			                        	<option value="12">12</option>
			                        	<option value="13">13</option>
			                        	<option value="14">14</option>
			                        	<option value="15">15</option>
			                        	<option value="16">16</option>
			                        	<option value="17">17</option>
			                        	<option value="18">18</option>
			                        	<option value="19">19</option>
			                        	<option value="20">20</option>
			                        	<option value="21">21</option>
			                        	<option value="22">22</option>
			                        	<option value="23">23</option>
			                        </select>
			                        :
			                        <select name="minuto_inicio" id="minuto_inicio" style="width:50px">
			                        	<option value="00">00</option>
			                        	<option value="15">15</option>
			                        	<option value="30">30</option>
			                        	<option value="45">45</option>
			                        </select>
			                     </td>
	                       </tr>
	                       <tr>
	                       		<td>
			                    	<span class="left title">T�rmino</span>
			                   	</td>
			                    <td>
			                        <input type="text" style="width:200px" class="datepicker" name="dt_fim" id="dt_fim"/>
			                    </td>
			                    <td>
			                        <span class="left title">Hora de T�rmino</span>
			                    </td>
			                    <td>
			                        <select name="hora_termino" id="hora_termino" style="width:50px">
			                        	<option value="00">00</option>
			                        	<option value="01">01</option>
			                        	<option value="02">02</option>
			                        	<option value="03">03</option>
			                        	<option value="04">04</option>
			                        	<option value="05">05</option>
			                        	<option value="06">06</option>
			                        	<option value="07">07</option>
			                        	<option value="08">08</option>
			                        	<option value="09">09</option>
			                        	<option value="10">10</option>
			                        	<option value="11">11</option>
			                        	<option value="12">12</option>
			                        	<option value="13">13</option>
			                        	<option value="14">14</option>
			                        	<option value="15">15</option>
			                        	<option value="16">16</option>
			                        	<option value="17">17</option>
			                        	<option value="18">18</option>
			                        	<option value="19">19</option>
			                        	<option value="20">20</option>
			                        	<option value="21">21</option>
			                        	<option value="22">22</option>
			                        	<option value="23">23</option>
			                        </select>
			                        :
			                        <select name="minuto_termino" id="minuto_termino" style="width:50px">
			                        	<option value="00">00</option>
			                        	<option value="15">15</option>
			                        	<option value="30">30</option>
			                        	<option value="45">45</option>
			                        </select>
			                      </td>
			                     </tr>
	                      </table>
                    </div>
                </div>
            </div>
        </div>                   
        <div class="modal-footer">
        	<button class="btn btn-success" onclick="javascript:limparMapa();$('.control').click();$('#close_btn').click();ajaxBuscaCoordenadas($('#placa_veiculo').val(),$('#dt_inicio').val(),$('#dt_fim').val(),$('#hora_inicio').val(),$('#minuto_inicio').val(),$('#hora_termino').val(),$('#minuto_termino').val())" type="button">Pesquisar</button></span>
            <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="close_btn">Fechar</button>            
        </div>
    </div>          
</body>

</html>
