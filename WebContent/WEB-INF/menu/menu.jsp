<div class="navigation">
	<div class="control"></div>
     <ul class="main">
         <li><a href="/Tracker/adm" class="active"><span class="icom-screen"></span><span class="text">DashBoard</span></a></li>
         <li><a href="#ui"><span class="icom-bookmark"></span><span class="text">UI elements</span></a></li>            
         <li><a href="#stats"><span class="icom-stats-up"></span><span class="text">Statistic</span></a></li>
     </ul>
     
     <div class="control"></div>        
     
     <div class="submain">
         
         <div id="default">
             
             <div class="widget-fluid userInfo clearfix">
                 <div class="image">
                     <img src="img/examples/users/dmitry.jpg" class="img-polaroid"/>
                 </div>              
                 <div class="name">Welcom, Dmitry</div>
                 <ul class="menuList">
                     <li><a href="#"><span class="icon-cog"></span> Settings</a></li>
                     <li><a href="#"><span class="icon-comment"></span> Messages</a></li>
                     <li><a href="#"><span class="icon-share-alt"></span> Logoff</a></li>                        
                 </ul>
                 <div class="text">
                     Welcom back! Your last visit: 24.10.2012 in 19:55
                 </div>
             </div>
             <div class="dr"><span></span></div>
             <ul class="fmenu">
                 <li>
                     <a href="#">Submenu level 2</a>                        
                 </li>
                 <li>
                     <a href="#">Submenu level 2</a>
                     <span class="caption blue">5</span>
                     <ul>
                         <li><a href="#">Submenu level 3</a><span class="caption">1</span></li>
                         <li><a href="#">Submenu level 3</a><span class="caption red">2</span></li>
                         <li><a href="#">Submenu level 3</a><span class="caption green">3</span></li>
                         <li><a href="#">Submenu level 3</a><span class="caption orange">4</span></li>
                         <li><a href="#">Submenu level 3</a><span class="caption purple">5</span></li>
                     </ul>
                 </li>
                 <li>
                     <a href="#">Submenu level 2</a>                        
                 </li>                    
             </ul>
             <div class="dr"><span></span></div>
             <div class="menu">
                 <a href="#">Simple Submenu level 1</a>
                 <a href="#">Simple Submenu level 1</a>
                 <a href="#">Simple Submenu level 1</a>
                 <a href="#">Simple Submenu level 1</a>
                 <a href="#">Simple Submenu level 1</a>
             </div>       
             <div class="dr"><span></span></div>
             <div class="widget-fluid TAC">
                 <div class="epc mini">
                     <div class="epcm-red" data-percent="56"><span>80</span>%</div>
                     <div class="label label-important">Storage</div>
                 </div>                    
                 <div class="epc mini">
                     <div class="epcm-green" data-percent="95"><span>1890</span>/2000</div>
                     <div class="label label-success">Inbox</div>
                 </div>                             
             </div>                
             <div class="dr"><span></span></div>
             <div class="widget">
                 <button class="btn btn-primary btn-block">Button block</button>
             </div>                
             <div class="widget">
                 <button class="btn btn-warning btn-block">Some another button</button>
             </div>
             <div class="dr"><span></span></div>
         </div>
         
         <div id="ui">                
             <div class="menu">
                 <a href="ui.html"><span class="icon-user"></span> Interface</a>
                 <a href="buttons.html"><span class="icon-chevron-right"></span> Buttons set</a>
                 <a href="widgets.html"><span class="icon-th-large"></span> Widgets</a>                    
                 <a href="icons.html"><span class="icon-fire"></span> Icons</a>
                 <a href="grid.html"><span class="icon-th"></span> Grid system</a>
                 <a href="dnd.html"><span class="icon-move"></span> Drug and drop</a>
             </div>                                
             <div class="dr"><span></span></div>                
             <div class="widget">
                 <button class="btn btn-primary btn-block">Button block</button>
             </div>                
             <div class="dr"><span></span></div>
         </div>            
         
		<div id="stats"></div>
     </div>

 </div>