var intervalo;

function ajaxBuscaCoordenadasAutomatico(placa,serial){
	stopEventCoordenadas();
	if(placa==null || placa == undefined){
		var nulo = '';
		ajaxBuscaCoordenadas(nulo,nulo,nulo,nulo,nulo,nulo,nulo,serial);
		intervalo = setInterval(function(){ajaxBuscaCoordenadas(nulo,nulo,nulo,nulo,nulo,nulo,nulo,serial)},30000);
	}else{
		console.log('normal')
		ajaxBuscaCoordenadas(placa);
		intervalo = setInterval(function(){ajaxBuscaCoordenadas(placa,nulo,nulo)},30000);
	}
}

function ajaxBuscaGrupos(id){
	$.getJSON('/Tracker/adm/process/grupoveiculos?ACAO=getCoordenadasByGrupo',{'id':id},
		  	function(data){
		  		endAjaxLoading();
		  		if(data.erro){	
		  			dialog('Falha de Processamento  ao Buscar Coordenadas',data.msg);
		  		}else{
					//recebe objeto grandeGerador
					var coordenadas = jQuery.parseJSON(data.coordenada);

					$("#table_coord > tbody").html("");
					
					limparMapa();
					if(coordenadas.length==0){
						var placaOuSerial = placa==null?serial:placa;
						notify('Aten��o','O Ve�culo Placa: '+placaOuSerial+' ainda n�o possui coordenadas.<br/>');
						return;
					}
					
					
					
					var location_zero = new google.maps.LatLng(coordenadas[0].latitude, coordenadas[0].longitude);
					
					var infowindow = new google.maps.InfoWindow({
						content: content
					});
					
					function openInfoWindow(marker) {
						title.innerHTML = marker.getTitle();
						pin.set("position", marker.getPosition());
						infowindow.open(map, marker);
					}
					
					// Handle the DOM ready event to create the StreetView panorama
				    // as it can only be created once the DIV inside the infowindow is loaded in the DOM.
				    var panorama = null;
				   
				 	google.maps.event.addListenerOnce(infowindow, "domready", function() {
				       panorama = new google.maps.StreetViewPanorama(streetview, {
				       		navigationControl: false,
				       		enableCloseButton: false,
				       		addressControl: false,
				       		linksControl: false,
				       		visible: true
				      });
				      panorama.bindTo("position", pin);
				    });
					
			    	function addMarker(lat,lng,name,image_ico) {
			   			var marker = new google.maps.Marker({
			  				position: new google.maps.LatLng(lat, lng),
			  				map: map,
			  				icon:image_ico,
			        		title: name
			   			});
			   			google.maps.event.addListener(marker, "click", function() {
			  				openInfoWindow(marker);
			   			});
			   			markersArray.push(marker);
			    	}

					
					for (var i=0;i<coordenadas.length;i++){
						//var location = new google.maps.LatLng(coordenadas[i].latitude, coordenadas[i].longitude);
						var dia = coordenadas[i].date.substring(8,10);
						var mes = coordenadas[i].date.substring(5,7);
						var ano = coordenadas[i].date.substring(0,4);
						
						var transReaon = motivoTransmissao(coordenadas[i].transmissionReason);
						
						$("#table_coord > tbody ").append("<tr><td>"+dia+"/"+mes+"/"+ano+
								"</td><td>"+coordenadas[i].date.substring(11,19)+
								"</td><td>"+coordenadas[i].latitude+"</td><td>"+coordenadas[i].longitude+
								"</td><td>"+coordenadas[i].speed+" Km/hora </td><td>"+transReaon+"</td><td><center> <span style='cursor:pointer' class='label label-important' onclick='verNoMapa(this,"+coordenadas[i].latitude+","+coordenadas[i].longitude+")'>Mapa</span>"+
								" <span style='cursor:pointer' class='label label-warning' onclick='enderecoAproximado("+coordenadas[i].latitude+","+coordenadas[i].longitude+")'>Endere�o</span></center></td></tr>");
						flightPlanCoordinates[i] = new google.maps.LatLng(coordenadas[i].latitude, coordenadas[i].longitude);
						
						addMarker(coordenadas[i].latitude,
								coordenadas[i].longitude,
								getBallon(coordenadas[i].serial,coordenadas[i].speed,coordenadas[i].latitude,coordenadas[i].longitude),
								image_car
						);
					}

					
					map.panTo(location_zero);
					map.setZoom(16);
					
					var flightPath = new google.maps.Polyline({
	                    path: flightPlanCoordinates,
	                    strokeColor: get_random_color(),
	                    strokeOpacity: 1.0,
	                    strokeWeight: 3
	                  });
					polylinesArray.push(flightPath);
	                flightPath.setMap(map);
				}
			});
}

function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

function stopEventCoordenadas(){
	clearTimeout(intervalo);
}

var content = document.createElement("DIV");
var title = document.createElement("DIV");

content.appendChild(title);

var streetview = document.createElement("DIV");

streetview.style.width = "400px";
streetview.style.height = "200px";

content.appendChild(streetview);


function ajaxBuscaCoordenadas(placa,data_inicio, data_fim, hora_inicio, minuto_inicio, hora_termino, minuto_termino,serial){
	
	//console.log('ajaxBuscaCoordenadas');
	notify('Coordenadas','Atualizando Coordenadas<br/>Placa: '+placa+'<br/>');
	
	$.getJSON('/Tracker/adm/crud/Coordenadas?ACAO=ajaxCoordenadasByVeiculo',{'placa':placa,'data_inicio':data_inicio,'data_fim':data_fim,'hora_inicio':hora_inicio, 'minuto_inicio': minuto_inicio, 'hora_termino':hora_termino, 'minuto_termino':minuto_termino,'serial':serial},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento  ao Buscar Coordenadas',data.msg);
	  		}else{
				//recebe objeto grandeGerador
				var coordenadas = jQuery.parseJSON(data.coordenada);

				$("#table_coord > tbody").html("");
				
				limparMapa();
				if(coordenadas.length==0){
					var placaOuSerial = placa==null?serial:placa;
					notify('Aten��o','O Ve�culo Placa: '+placaOuSerial+' ainda n�o possui coordenadas.<br/>');
					return;
				}
				
				
				
				var location_zero = new google.maps.LatLng(coordenadas[0].latitude, coordenadas[0].longitude);
				
				var infowindow = new google.maps.InfoWindow({
					content: content
				});
				
				function openInfoWindow(marker) {
					title.innerHTML = marker.getTitle();
					pin.set("position", marker.getPosition());
					infowindow.open(map, marker);
				}
				
				// Handle the DOM ready event to create the StreetView panorama
			    // as it can only be created once the DIV inside the infowindow is loaded in the DOM.
			    var panorama = null;
			   
			 	google.maps.event.addListenerOnce(infowindow, "domready", function() {
			       panorama = new google.maps.StreetViewPanorama(streetview, {
			       		navigationControl: false,
			       		enableCloseButton: false,
			       		addressControl: false,
			       		linksControl: false,
			       		visible: true
			      });
			      panorama.bindTo("position", pin);
			    });
				
		    	function addMarker(lat,lng,name,image_ico) {
		   			var marker = new google.maps.Marker({
		  				position: new google.maps.LatLng(lat, lng),
		  				map: map,
		  				icon:image_ico,
		        		title: name
		   			});
		   			google.maps.event.addListener(marker, "click", function() {
		  				openInfoWindow(marker);
		   			});
		   			markersArray.push(marker);
		    	}

				
				for (var i=0;i<coordenadas.length;i++){
					//var location = new google.maps.LatLng(coordenadas[i].latitude, coordenadas[i].longitude);
					var dia = coordenadas[i].date.substring(8,10);
					var mes = coordenadas[i].date.substring(5,7);
					var ano = coordenadas[i].date.substring(0,4);
					
					var transReaon = motivoTransmissao(coordenadas[i].transmissionReason);
					
					$("#table_coord > tbody ").append("<tr><td>"+dia+"/"+mes+"/"+ano+
							"</td><td>"+coordenadas[i].date.substring(11,19)+
							"</td><td>"+coordenadas[i].latitude+"</td><td>"+coordenadas[i].longitude+
							"</td><td>"+coordenadas[i].speed+" Km/hora </td><td>"+transReaon+"</td><td><center> <span style='cursor:pointer' class='label label-important' onclick='verNoMapa(this,"+coordenadas[i].latitude+","+coordenadas[i].longitude+")'>Mapa</span>"+
							" <span style='cursor:pointer' class='label label-warning' onclick='enderecoAproximado("+coordenadas[i].latitude+","+coordenadas[i].longitude+")'>Endere�o</span></center></td></tr>");
					flightPlanCoordinates[i] = new google.maps.LatLng(coordenadas[i].latitude, coordenadas[i].longitude);
					
					addMarker(coordenadas[i].latitude,
							coordenadas[i].longitude,
							getBallon(placa,coordenadas[i].speed,coordenadas[i].latitude,coordenadas[i].longitude),
							image_car
					);
				}

				//posicao final
				addMarker(coordenadas[0].latitude,
						coordenadas[0].longitude,
						getBallon(placa,coordenadas[0].speed,coordenadas[0].latitude,coordenadas[0].longitude),
						image_car_atual
				);
				
				map.panTo(location_zero);
				map.setZoom(16);
				
				var flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    strokeColor: "#67c547",
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                  });
				polylinesArray.push(flightPath);
                flightPath.setMap(map);
			}
		});
}


function motivoTransmissao(key){
	chave = parseInt(key,10);
	
	switch (chave) {
		case 3:
			return "Envio de Coordenadas Parado"
		case 4:
			return "Envio de Coordenada movendo";
		case 9:
			return "Igni��o Ligada";
		case 10:
			return "Igni��o Desligada";
		case 11:
			return "P�nico Ativado";
		case 12:
			return "P�nico Desativado";
		case 21:
			return "Alerta de Movimento";
		case 25:
			return "Falta de Energia Externa";
		case 26:
			return "Energia Externa OK";
		case 30:
			return "Entrou em Modo Sleep";
		case 31:
			return "Combust�vel Cortado";
		case 32:
			return "Combust�vel Liberado";
		case 37:
			return "Velocidade M�xima Atingida";
		case 38:
			return "Velocidade Normalizada";
		case 41:
			return "Falha de Bateria de BackUp";
		case 42:
			return "Bateria de BackUp OK";
		case 43:
			return "Posi��o Armazenada Enviada";
		case 53:
			return "Jamming Detectado";
		case 54:
			return "Jamming Sem Contato";
		default:
			return "Envio de Coordenada";
	}
}

function getBallon(placa,velocidade,lat,lon){
	var ballon = "<h4><center>"+placa+"</center></h3>"+
				"<p>Velocidade: "+velocidade+" Km/hora</p>"+
				"<p>Latitude: "+lat+" </p>"+
				"<p>Longitude: "+lon+" </p>";
	
	return ballon;
}

function ajaxBuscaCep(parametro1){
	
	dialog('Aten��o','Aguarde, estamos buscando o endere�o...');
	
	loadAjaxLoading();

	$.getJSON('/Tracker/publico/process/BuscaCep?ACAO=ajaxBuscaCep',{'cep':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento',data.msg);
	  		}else{
				//recebe objeto CredenciaisPEV
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos
				$("#endereco").val(obj.logradouro);
				$("#bairro").val(obj.bairro);
				$("#numero").focus();
				
				if(obj.idUf != null && obj.idMunicipio != null){
					$('#uf option[value="br.com.easytracking.tracker.bean.Uf[id='+obj.idUf+']"]').prop('selected', true);
					$('#uf option[value="br.com.easytracking.tracker.bean.Uf[id='+obj.idUf+']"]').change();
					//ao terminar ajax que carrega municipios atraves do uf 
					//metodo function selecionaComboUfManual chamado no final de addItensToCombo, arq ajax.js, seleciona o combo com o valor do campo valorIdMunAjax
					$("#div-auxiliar").html('<input type="hidden" name="valorIdMunAjax" id="valorIdMunAjax"/>');
					valorIdMunAjax=obj.idMunicipio;
					$("#div-auxiliar").html('<input type="hidden" name="valorIdMunAjax" id="valorIdMunAjax"/>');
					$("input[name='valorIdMunAjax']").val(valorIdMunAjax);
				}
				
				closeDialog();
			}
		});
}

function selecionaComboUfManual(){
	valorIdMunAjax = $("input[name='valorIdMunAjax']").val();
	if(!(valorIdMunAjax == "" || valorIdMunAjax == null || valorIdMunAjax == '<null>')){
  		$('#municipio option[value="br.com.easytracking.tracker.bean.Municipio[id='+valorIdMunAjax+']"]').prop('selected', true);
	}
}

function ajaxComando(comando,id,velocidade){
	$.getJSON('/Tracker/adm/process/Comandos?ACAO='+comando,{'id':id,'velocidade':velocidade},
	  	function(data){
			$('#close_btn').click();
			if(data.erro){	
	  			dialog('Falha de Processamento',data.msg);
	  		}else{
	  			dialog('Aten��o',data.msg)
			}
		});
}


